/*	
 *	JS Document Written by Front-End Developer's Team of StapleLogic (http://www.staplelogic.com)
 *	In this file you will find all the scripts and functions used in site with proper comments.
 *	This file is copyright to StapleLogic and no reproduction of this file is allowed without the prior written permission from StapleLogic
 *	Version of file : V1.0
 */

$(function () {
    $('input, textarea').placeholder();				//Placeholder for IE

    // $('nav').positionSticky();					// Sticky Header

    $('.bxslider').bxSlider({// Main Slider
        auto: true
    });
    $('.post-slider').bxSlider({// Main Slider
        auto: true
    });
    $('.detail-slider').bxSlider({// Main Slider
        auto: true
    });

    $('.slider1').bxSlider({
        slideWidth: 200,
        minSlides: 6,
        maxSlides: 7,
        slideMargin: 10
    });
	
	$("#login").click(function () {
        $(".user_register").hide();
		$(".sign-up").hide();
        $(".social_login").show();
        return false;
    });
	$("#sign-up").click(function () {
        $(".user_register").hide();
		$(".sign-up").show();
        $(".social_login").hide();
        return false;
    });
	$(".pop-login").click(function () {
        $(".user_register").hide();
		$(".sign-up").hide();
        $(".social_login").show();
        return false;
    });
	$(".pop-signup").click(function () {
        $(".user_register").hide();
		$(".sign-up").show();
        $(".social_login").hide();
        return false;
    });
	$(".pop-signup-email").click(function () {
        $(".user_register").show();
		$(".sign-up").hide();
        $(".social_login").hide();
        return false;
    });

	$(".pop-ups, .modal_close").click(function () {
        $("body").toggleClass("overflow");
    });
	

 $('.subitem').attr('title', $('.vika').remove().html());
    //$(document).tooltip();
});




var $document = $(document),
    $element = $('nav'),
    nav = 'hasScrolled';


$document.scroll(function() {
    //$element.toggleClass('test', $document.scrollTop() <= 0 && $document.scrollTop() <= 399 );
   //$element.toggleClass('test', $document.scrollTop() == 399);
   $element.toggleClass('test', $document.scrollTop() >= 400);
  //$element.toggleClass('navevent-detail', $document.scrollTop() <= 0);
}); 


$("#login").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});
$("#sign-up").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});



$(window).scroll(function () {
    if ($(this).scrollTop()) {
        $('.totop').fadeIn();
    } else {
        $('.totop').fadeOut();
    }
});
$(".totop").click(function () {
    $("html, body").animate({scrollTop: 0}, 1000);
});
