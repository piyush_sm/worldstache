/*	
 *	JS Document Written by Front-End Developer's Team of StapleLogic (http://www.staplelogic.com)
 *	In this file you will find all the scripts and functions used in site with proper comments.
 *	This file is copyright to StapleLogic and no reproduction of this file is allowed without the prior written permission from StapleLogic
 *	Version of file : V1.0
 */

$(function () {
    $('input, textarea').placeholder();				//Placeholder for IE

    // Masonry corner stamp modifications
	 /* $.Mason.prototype.resize = function() {
	    this._getColumns();
	    this._reLayout();
	  };
	  
	  $.Mason.prototype._reLayout = function( callback ) {
	    var freeCols = this.cols;
	    if ( this.options.cornerStampSelector ) {
	      var $cornerStamp = this.element.find( this.options.cornerStampSelector ),
		  cornerStampX = $cornerStamp.offset().left - 
		    ( this.element.offset().left + this.offset.x + parseInt($cornerStamp.css('marginLeft')) );
	      freeCols = Math.floor( cornerStampX / this.columnWidth );
	    }
	    // reset columns
	    var i = this.cols;
	    this.colYs = [];
	    while (i--) {
	      this.colYs.push( this.offset.y );
	    }

	    for ( i = freeCols; i < this.cols; i++ ) {
	      this.colYs[i] = this.offset.y + $cornerStamp.outerHeight(true);
	    }

	    // apply layout logic to all bricks
	    this.layout( this.$bricks, callback );
	  }; */
 
	    $('#masonry').masonry({// Initialize Masonry
        columnWidth: 20,
        itemSelector: '.item',
        cornerStampSelector: '.corner-stamp',
        isFitWidth: false,
        isAnimated: !Modernizr.csstransitions
    }).imagesLoaded(function () {
        $(this).masonry('reload');
    });

    //$('nav').positionSticky();					// Sticky Header

    $('.bxslider').bxSlider({// Main Slider
        auto: true
    });
    $('.post-slider').bxSlider({// Main Slider
        auto: true
    });
    $('.detail-slider').bxSlider({// Main Slider
        auto: true
    });

    $('.slider1').bxSlider({
        slideWidth: 200,
        minSlides: 6,
        maxSlides: 7,
        slideMargin: 10
    });
	
	$("#login").click(function () {
        $(".user_register").hide();
		$(".sign-up").hide();
        $(".social_login").show();
        return false;
    });
	$("#sign-up").click(function () {
        $(".user_register").hide();
		$(".sign-up").show();
        $(".social_login").hide();
        return false;
    });
	$(".pop-login").click(function () {
        $(".user_register").hide();
		$(".sign-up").hide();
        $(".social_login").show();
        return false;
    });
	$(".pop-signup").click(function () {
        $(".user_register").hide();
		$(".sign-up").show();
        $(".social_login").hide();
        return false;
    });
	$(".pop-signup-email").click(function () {
        $(".user_register").show();
		$(".sign-up").hide();
        $(".social_login").hide();
        return false;
    });

	$(".pop-ups, .modal_close").click(function () {
        $("body").toggleClass("overflow");
    });
	

 $('.subitem').attr('title', $('.vika').remove().html());
    //$(document).tooltip();
});




var $document = $(document),
    $element = $('nav'),
    nav = 'hasScrolled';


$document.scroll(function() {
    //$element.toggleClass('test', $document.scrollTop() <= 0 && $document.scrollTop() <= 399 );
   //$element.toggleClass('test', $document.scrollTop() == 399);
   $element.toggleClass('test', $document.scrollTop() >= 490);
  //$element.toggleClass('navevent-detail', $document.scrollTop() <= 0);
}); 


$("#login").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});
$("#sign-up").leanModal({top: 200, overlay: 0.6, closeButton: ".modal_close"});



$(window).scroll(function () {
    if ($(this).scrollTop()) {
        $('.totop').fadeIn();
    } else {
        $('.totop').fadeOut();
    }
});
$(".totop").click(function () {
    $("html, body").animate({scrollTop: 0}, 1000);
});
