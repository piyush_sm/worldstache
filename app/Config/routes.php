<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/dashboard', array('controller' => 'users', 'action' => 'dashboard'));
	Router::connect('/admin-dashboard', array('controller' => 'users', 'action' => 'admin_dashboard'));
	Router::connect('/edit-profile', array('controller' => 'users', 'action' => 'my_account'));
	Router::connect('/about-us', array('controller' => 'users', 'action' => 'about_us'));
	Router::connect('/contact-us', array('controller' => 'users', 'action' => 'contact_us'));
	Router::connect('/professional-request', array('controller' => 'users', 'action' => 'professional_request'));
	Router::connect('/admin-users-list', array('controller' => 'users', 'action' => 'users_list'));
	Router::connect('/admin-professional-request', array('controller' => 'users', 'action' => 'professional_requests'));
	Router::connect('/profile', array('controller' => 'users', 'action' => 'my_profile'));
	Router::connect('/privacy', array('controller' => 'users', 'action' => 'privacy'));
	Router::connect('/terms-of-use', array('controller' => 'users', 'action' => 'terms_of_use'));
	Router::connect('/team', array('controller' => 'users', 'action' => 'team'));
	Router::connect('/help', array('controller' => 'users', 'action' => 'help'));
	Router::connect('/change-password', array('controller' => 'users', 'action' => 'change_password'));
	Router::connect('/set-payment-account', array('controller' => 'users', 'action' => 'set_payment'));
	Router::connect('/guestlist-sold', array('controller' => 'users', 'action' => 'guestlist_sold'));
	Router::connect('/post-comment/*', array('controller' => 'users', 'action' => 'post_comment'));
	Router::connect('/deactivate-user/*', array('controller' => 'users', 'action' => 'deactivate_user'));
	Router::connect('/delete-user/*', array('controller' => 'users', 'action' => 'delete_user'));
	Router::connect('/edit-user/*', array('controller' => 'users', 'action' => 'edit_user'));
	Router::connect('/process-professional/*', array('controller' => 'users', 'action' => 'process_professional'));
	Router::connect('/cancel-professional/*', array('controller' => 'users', 'action' => 'cancel_professional'));
	Router::connect('/my-orders/*', array('controller' => 'users', 'action' => 'my_orders'));
	Router::connect('/admin-manage-orders', array('controller' => 'users', 'action' => 'manage_orders'));
	Router::connect('/add-professional/*', array('controller' => 'users', 'action' => 'add_professional'));
    
    
	Router::connect('/search', array('controller' => 'events', 'action' => 'search'));
	Router::connect('/events-list/*', array('controller' => 'events', 'action' => 'events_list'));
	Router::connect('/events-lists/*', array('controller' => 'events', 'action' => 'events_list_s'));
	Router::connect('/event-detail/*', array('controller' => 'events', 'action' => 'event_detail'));
	Router::connect('/view-event/*', array('controller' => 'events', 'action' => 'view_event'));
	Router::connect('/add-event', array('controller' => 'events', 'action' => 'add_event_p'));
	Router::connect('/edit-event/*', array('controller' => 'events', 'action' => 'edit_event_p'));
	Router::connect('/activate-event/*', array('controller' => 'events', 'action' => 'activate_event'));
	Router::connect('/deactivate-event/*', array('controller' => 'events', 'action' => 'deactivate_event'));
	Router::connect('/delete-event/*', array('controller' => 'events', 'action' => 'delete_event'));
	Router::connect('/admin-edit-event/*', array('controller' => 'events', 'action' => 'edit_event'));
	Router::connect('/admin-add-event', array('controller' => 'events', 'action' => 'add_event'));
	Router::connect('/admin-typehead', array('controller' => 'events', 'action' => 'typehead'));
	Router::connect('/pay-by-stripe/*', array('controller' => 'events', 'action' => 'paybystripecomp'));
	Router::connect('/cash-on-delievery/*', array('controller' => 'events', 'action' => 'cashondelievery'));
	
	
	Router::connect('/categories-list', array('controller' => 'categories', 'action' => 'categories_list'));
	Router::connect('/edit-category/*', array('controller' => 'categories', 'action' => 'edit_category'));
	Router::connect('/delete-category/*', array('controller' => 'categories', 'action' => 'delete_category'));
	Router::connect('/admin-add-category', array('controller' => 'categories', 'action' => 'add_category'));
	
	Router::connect('/blog-list', array('controller' => 'blogs', 'action' => 'blog_list'));
	Router::connect('/activate-blog/*', array('controller' => 'blogs', 'action' => 'activate_blog'));
	Router::connect('/deactivate-blog/*', array('controller' => 'blogs', 'action' => 'deactivate_blog'));
	Router::connect('/delete-blog/*', array('controller' => 'blogs', 'action' => 'delete_blog'));
	Router::connect('/edit-blog/*', array('controller' => 'blogs', 'action' => 'edit_blog'));
	Router::connect('/admin-add-blog', array('controller' => 'blogs', 'action' => 'add_blog'));
	Router::connect('/blog-detail/*', array('controller' => 'blogs', 'action' => 'blog_detail'));
	
	Router::connect('/subcategories-list/*', array('controller' => 'subcategories', 'action' => 'subcategories_list'));
	Router::connect('/edit-subcategory/*', array('controller' => 'subcategories', 'action' => 'edit_subcategory'));
	Router::connect('/delete-subcategory/*', array('controller' => 'subcategories', 'action' => 'delete_subcategory'));
	Router::connect('/admin-add-subcategory', array('controller' => 'subcategories', 'action' => 'add_subcategory'));
	
	Router::connect('/add-artist', array('controller' => 'artists', 'action' => 'add_artist_p'));
	Router::connect('/activate-artist/*', array('controller' => 'artists', 'action' => 'activate_artist'));
	Router::connect('/deactivate-artist/*', array('controller' => 'artists', 'action' => 'deactivate_artist'));
	Router::connect('/delete-artist/*', array('controller' => 'artists', 'action' => 'delete_artist'));
	Router::connect('/edit-artist/*', array('controller' => 'artists', 'action' => 'edit_artist_p'));
	Router::connect('/view-artist/*', array('controller' => 'artists', 'action' => 'view_artist'));
	Router::connect('/admin-add-artist', array('controller' => 'artists', 'action' => 'add_artist'));
	Router::connect('/admin-edit-artist/*', array('controller' => 'artists', 'action' => 'edit_artist'));

	Router::connect('/add-place', array('controller' => 'places', 'action' => 'add_place_p'));
	Router::connect('/view-place/*', array('controller' => 'places', 'action' => 'view_place'));
	Router::connect('/activate-place/*', array('controller' => 'places', 'action' => 'activate_place'));
	Router::connect('/deactivate-place/*', array('controller' => 'places', 'action' => 'deactivate_place'));
	Router::connect('/delete-place/*', array('controller' => 'places', 'action' => 'delete_place'));
	Router::connect('/edit-place/*', array('controller' => 'places', 'action' => 'edit_place_p'));
	Router::connect('/admin-add-place', array('controller' => 'places', 'action' => 'add_place'));
	Router::connect('/admin-edit-place/*', array('controller' => 'places', 'action' => 'edit_place'));

	Router::connect('/admin-manage-banner', array('controller' => 'settings', 'action' => 'manage_banner'));
	Router::connect('/admin-manage-slider', array('controller' => 'settings', 'action' => 'manage_slider'));
	Router::connect('/admin-manage-footer', array('controller' => 'settings', 'action' => 'manage_footer'));
	Router::connect('/admin-static-pages', array('controller' => 'settings', 'action' => 'static_pages'));
	Router::connect('/admin-social-media', array('controller' => 'settings', 'action' => 'social_media'));
	
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
	if (!defined('WEB_ROOT'))
{
define('WEB_ROOT', "http://".$_SERVER['HTTP_HOST']."/dev.staplelogic.in/worldstashdev/images/");
}
Router::connect(
   '/opauth-complete/*',
   array('controller' => 'users', 'action' => 'opauth_complete')
);
