<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Add Sub-Category</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">View SubCategories</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'add_subcategory', 'enctype'=>'multipart/form-data')) ?>
                            <div class="edit-field">
                <?php echo $this->Form->input('cat_id', array('options'=>$list_categories,'id'=>'cat_id','placeholder'=>'Category Name','class'=>'form-control', 'required'=>'required', 'div'=>false, 'label'=>'Category Name')) ?>
                                </div>
                            <div class="edit-field">
                <?php echo $this->Form->input('subcategory_name', array('id'=>'subcategory_name','placeholder'=>'Sub-Category Name','class'=>'form-control', 'required'=>'required', 'div'=>false, 'label'=>'Sub-Category Name')) ?>
                                </div>
                            <div class="add-new-event adddish">
                             <?php echo $this->Html->image("calendaradd.png", array("alt"=>"logo"));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('subcategory_image', array('id'=>'subcategory_image', 'type'=>'file', 'label'=>false ,'div'=>false, 'class'=>'fileup')) ?>
                                    </div>
                                </div>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning fileupload" value="Add Sub-Category">
                            </div>
                            </div>
                            </div>
                            <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
								<?php foreach($all_categories as $categories){ ?>
									<?php if(!empty($categories['Subcategory']) || $categories['Subcategory'] != null) { ?>
									<p><strong> Category Name: <?php echo $categories['Category']['category_name']; ?></strong></p>
									<?php } ?>
									<?php foreach($categories['Subcategory'] as $subcategories) { ?>
									<?php if(!empty($subcategories) || $subcategories != null) {?>
									<?php $id = base64_encode($subcategories['id']); ?>
									<div class="add-new-event adddish">
									<?php if(empty($subcategories['subcategory_image'])){ ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"subcategory_image"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($subcategories['subcategory_image'], array("width"=>"100px","height"=>"100px","alt"=>"subcategory_image"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $subcategories['subcategory_name']; ?></h4>
                                    </div>
                                    <div class="adddel">
                                       <a href='edit-subcategory?scid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Edit</button></a>
                                       <a href="delete-subcategory?scid=<?php echo $id; ?>"><button type="button" onclick="if (confirm('Are you sure you want to delete this sub-category ?')) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
									<?php }
									
								}
									 } ?>
                            </div> 
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
 jQuery(document).ready(function () {
 jQuery(".fileupload").click(function ()        {
            var isValid = true;             
            jQuery('.fileup').each(function() {  
                               
            if(jQuery(this).val() =="" )
             {              
                isValid = false;
                jQuery(this).css({"border": "1px solid red","background": "#FFCECE"});
            }    
           else {
                jQuery(this).css({"border": "","background": ""});  
                return true;
           }
           });
              return isValid;
         });
 });

</script>

