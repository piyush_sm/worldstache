<div class="content">
        <?php echo $this->Session->flash(); ?>
        <div class="header">            
            <h1 class="page-title">Add Options</h1>
        </div>    

        <div class="container-fluid">
            <div class="row-fluid">

	<?php echo $this->Form->Create('', array('id'=>'add_option')) ?>
  
   <div class="btn-toolbar">

	<?php $categories = array('1'=>'cat1','2'=>'cat2','3'=>'cat3','4'=>'cat4') ; ?>
	
	<?php echo $this->Form->input('cat_id', array('options'=> $list_categories, 'id'=>'cat_id', 'label'=> 'Select main category')) ?>

	<?php echo $this->Form->input('option_name', array('id'=>'option_name', 'required'=>'required', 'placeholder'=>'Add Option')) ?>

    	<button class="btn btn-primary"><i class="icon-plus"></i> Add </button>

	<?php echo $this->Form->end(); ?>

  <div class="btn-group">
  </div>
</div>
<div class="well add-cate">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Option Name</th>
	     <th>Categories Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>

	<?php foreach($all_options as $options) {
		
		$id = base64_encode($options['Option']['id']);
		$name = ClassRegistry::init('Category')->find('all', array('field'=>'Category.category_name','conditions'=>array('id'=> $options['Option']['cat_id'])

		));
	?>
        <tr>
          <td> <?php echo $options['Option']['id']; ?> </td>
          <td> <?php echo $options['Option']['option_name']; ?> </td>
	  <td> <?php echo $name[0]['Category']['category_name']; ?>  </td>
          <td>
              <?php echo $this->Html->link('',array('controller'=>'options','action'=>'edit_option', '?'  => array('oid' => $id)),array('class'=>'edit fa fa-edit','title'=>'Edit')); ?>
	      <?php echo $this->Html->link('',array('controller'=>'options','action'=>'delete_option', '?'  => array('oid' => $id)),array('confirm'=>'Are you sure you want to delete this option ?','class'=>'delet fa fa-remove','title'=>'Delete')); ?>
          </td>
        </tr>
<?php } ?>
      </tbody>
    </table>
</div>
