             <div class="event-main thumbeve" id="masonry">
                <h2>Event Categories</h2>
                <?php $id = base64_encode(0); ?>
                <div class="event itemeve">
                    <a href="events-list?eid=<?php echo $id; ?>">
                        <span>
                            <span> All </span>
                        </span>
                        <?php echo $this->Html->image("all.png", array("alt"=>"event_img"));?>
                    </a>
                </div>
                <?php if($all_categories == null || $all_categories == ""){ ?>
                <div class="event itemeve">
                    <p> No data available to Display </p>
                </div>
                <?php } else { ?>
                <?php foreach ($all_categories as $categories) { ?>
                <?php $id = base64_encode($categories['Category']['id']);?>
                <div class="event itemeve">
                    <a href="subcategories-list?scid=<?php echo $id; ?>">
                        <span>
                            <span><?php echo $categories['Category']['category_name'];?></span>
                        </span>
                        <?php if(empty($categories['Category']['category_image'])) { ?>
                        <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_img"));?>
                        <?php } else { ?>
                         <?php echo $this->Html->image($categories['Category']['category_image'], array("alt"=>"category_image"));?>
                        <?php } ?>
                    </a>
                </div>
                <?php } } ?>  
            </div>
        </div>
