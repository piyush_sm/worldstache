<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Add New Category</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">View Categories</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'edit_category', 'enctype'=>'multipart/form-data')) ?>
                            <div class="edit-field">
                <?php echo $this->Form->input('category_name', array('id'=>'category_name','placeholder'=>'Category Name','class'=>'form-control', 'required'=>'required', 'div'=>false, 'value'=>$category['Category']['category_name'])) ?>
                                </div>
                            <div class="add-new-event adddish">
                            <?php if(empty($category['Category']['category_image'])){?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"category_image"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($category['Category']['category_image'], array("width"=>"120px","height"=>"100px","alt" => "Logo"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('category_image', array('id'=>'category_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Update Category">
                            </div>
                            </div>
                            </div>
                            <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                
                                <?php foreach($all_categories as $categories) { 
                                $id = base64_encode($categories['Category']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($categories['Category']['category_image'], array("width"=>"120px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $categories['Category']['category_name']; ?></h4>
                                    </div>
                                    <div class="adddel">
                                       <a href='../categories/edit_category?cid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Edit</button></a>
                                       <a href="../categories/delete_category?cid=<?php echo $id; ?>"><button type="button" onclick="if (confirm("Are you sure you want to delete this event ?")) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div> 
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
