<div id="masonry" class="mas-center">
		 <!-- BEGIN: Twitter website widget (http://twitterforweb.com) -->

		<div class="corner-stamp">
				 
		 <a class="twitter-timeline"  href="https://twitter.com/piyush20rana" data-widget-id="588198144934051840">Tweets by @piyush20rana</a>
		 
		<div class="fb-page" data-href="https://www.facebook.com/Sevilla.WorldStache" data-width="330" data-height="300" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/Sevilla.WorldStache"><a href="https://www.facebook.com/Sevilla.WorldStache">WorldStache Sevilla</a></blockquote></div></div>
		</div>


		<!-- END: Twitter website widget (http://twitterforweb.com) -->
		 <div class="item featured">

		            <ul class="post-slider">
				<?php $count = count($slider_data['images']); ?>
				<?php for($i = 0; $i < $count; $i++) { ?>
				<li><?php echo $this->Html->link(
	    				$this->Html->image($slider_data['images'][$i]), $slider_data['urls'][$i], array('target'=>'_blank','escape' => false));
				    ?>
				</li>
				<?php } ?>

		            </ul>
		        </div>
				<?php foreach ($all_posts as $post) { ?>
				<div class="item boxes">
                    <a href="javascript:void(0)">
                        <?php echo $this->Html->image($post['Blog']['post_image'], array("width"=>"200","height"=>"200","alt" => "post_image")); ?>
                        <h1> <?php echo $post['Blog']['post_title'];?> </h1>
                    </a>
                    <span><a href="javascript:void(0)"><i class="fa fa-btc"></i></a></span><div class="date"><?php $date = new DateTime($post['Blog']['created_at']); echo $date->format('Y-m-d'); ?></div>
                    <p>
                    	<?php echo $post['Blog']['post_description']; ?>
                        <?php //echo substr($post['Blog']['post_description'], 0, 60).".........."; ?>
                    </p>
                </div>
			<?php } ?>
		    
            </div>
        </div>
        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
