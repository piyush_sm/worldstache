<div class="col-sm-9  col-md-9  main">

                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Setup Paypal Account</a></li>
                        <!--<li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">Setup Your Stripe Account</a></li>-->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php echo $this->Form->Create('',array('id'=>'set_payment','enctype'=>'multipart/form-data')) ?>
                                    <!--<h2 class="dash-header">Add Event </h2>-->
                                    <div class="edit-field">
                                        <label>Paypal account used for payouts</label>
                <?php echo $this->Form->input('paypal_f_email', array('id'=>'paypal_f_email','placeholder'=>'Paypal Facilitator Email','class'=>'form-control', 'required'=>'required', 'div'=>false, 'label'=>false, 'value'=>$user['User']['paypal_f_email'])) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Stripe Security Key</label>
                <?php echo $this->Form->input('stripe_s_key', array('id'=>'stripe_s_key','placeholder'=>'Stripe Security Key','class'=>'form-control', 'required'=>'required', 'div'=>false, 'label'=>false, 'value'=>$user['User']['stripe_s_key'])) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Stripe Publishable Key</label>
                <?php echo $this->Form->input('stripe_p_key', array('id'=>'stripe_p_key','placeholder'=>'Stripe Publishable Key','class'=>'form-control', 'required'=>'required', 'div'=>false, 'label'=>false, 'value'=>$user['User']['stripe_p_key'])) ?>
                                    </div>
                                <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Update Account">
                                </div>
                                <?php echo $this->Form->end(); ?>    
                            </div> 
                        </div>
                    </div>
                </div>
            </div>