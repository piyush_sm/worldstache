<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Paypal Orders</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">COD Orders</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php if(empty($paypal_orders)) { ?>
                                <p> No Orders  </p>
                                <?php } ?>
                            <?php foreach($paypal_orders as $paypal) { 
                                $id = base64_encode($paypal['Payment']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <div class="addnew-discription">
                                        <p><i class="fa fa-envelope"></i> Order Number: <?php echo $paypal['Payment']['order_number'];?></p>
                                        <p><i class="fa fa-user"></i> Customer Name: <?php echo $paypal['User']['first_name']; ?> <?php echo $paypal['User']['last_name']; ?></p>
                                        <p><i class="fa fa-envelope"></i> Email: <?php echo $paypal['User']['email'];?></p>
                                        <?php if(!empty($paypal['User']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $paypal['User']['phone_number'];?></p>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            </div>
                            <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($cashondeleivery)) { ?>
                                <p> No Orders </p>
                                <?php } ?>
                                <?php foreach($cashondeleivery as $cod) { 
                                $id = base64_encode($cod['Payment']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    
                                    <div class="addnew-discription">
                                        <p><i class="fa fa-envelope"></i> Order Number: <?php echo $cod['Payment']['order_number'];?></p>
                                        <p><i class="fa fa-user"></i> Customer Name: <?php echo $cod['User']['first_name']; ?> <?php echo $cod['User']['last_name']; ?></p>
                                        <p><i class="fa fa-envelope"></i>User Email: <?php echo $cod['User']['email'];?></p>
                                        <?php if(!empty($cod['User']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $cod['User']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($cod['Payment']['shipping_address'])) { ?>
                                        <!--<p><i class="fa fa-map-marker"></i> Address: <?php echo $cod['Payment']['shipping_address'];?></p>-->
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                    </div>
                </div>
          </div>
</div>