<div class="col-sm-9  col-md-9  main">
    <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Change Password</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php echo $this->Form->Create('',array('id'=>'create_password','enctype'=>'multipart/form-data')) ?>
                                  <div class="edit-field">
                                    <?php echo $this->Form->input('old_password', array('type'=>'password','id'=>'old_password','placeholder'=>'Old Password','class'=>'form-control', 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                    <?php echo $this->Form->input('password', array('id'=>'password','placeholder'=>'New Password','class'=>'form-control', 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                    <?php echo $this->Form->input('confirm_password', array('type'=>'password','id'=>'confirm_password','placeholder'=>'Confirm Password','class'=>'form-control', 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="btn-group event-sub">
                                        <input type="submit" class="btn btn-warning guest_form" value="Update Password">
                                    </div>
                                <?php echo $this->Form->end(); ?> 
                            </div>  
                        </div>
                    </div>
                </div>
              
</div>
