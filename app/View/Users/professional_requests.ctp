<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Professional Requests</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_users) || $all_users == "" || $all_users ==null) { ?>
                                <p>No request for professional</p>
                                <?php } else { ?>
                            <?php foreach($all_users as $user) { 
                                $id = base64_encode($user['User']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($user['User']['profile_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $user['User']['first_name']; ?> <?php echo $user['User']['last_name']; ?></h4>
                                        <p><i class="fa fa-envelope"></i> Email: <?php echo $user['User']['email'];?></p>
                                        <?php if(!empty($user['User']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $user['User']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($user['User']['address'])) { ?>
                                        <p><i class="fa fa-map-marker"></i> Address: <?php echo $user['User']['address'];?></p>
                                        <?php } ?>
                                        <p>Requested to become a professional</p>
                                    </div>
                                    <div class="adddel">
                                        <a href='edit-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <a href='process-professional?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Professional </button></a>
                                       <a href='cancel-professional?uid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Cancel</button></a>
                                    </div>
                                </div>
                                <?php } }?>
                            </div>
                            </div>
                           
                </div>
          </div>
</div>