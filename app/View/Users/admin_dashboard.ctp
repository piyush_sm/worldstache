<div class="col-sm-9  col-md-9  main">

        <div class="tab-content" id="myTabContent">
            <div aria-labelledby="home-tab" id="home" class="tab-pane fade in active" role="tabpanel">
                        <div class="content">
                            <div class=" tabclass"> 
                                <div class="add-new-event">
                                    <h5>Admin Dashboard</h5>    
                                </div>
								
								
							<div id="box_links">							
							<div class="wrap" id="my_events">
                                <a class="dashboard-images" href="admin-add-event">
								<img src="images/event/events.jpg" />
								<h3 class="desc">Events<span class="round"> <?php echo $all_events; ?></span></h3>
                                </a>
							</div>
							
							<div class="wrap">
                                 <a class="dashboard-images" href="admin-add-artist">
								<img src="images/artist/artists.jpg" />
								<h3 class="desc">Artists<span class="round"> <?php echo $all_artist; ?></span></h3>
							      </a>
                            </div>
							
							<div class="wrap">
                                 <a class="dashboard-images" href="admin-add-place">
								<img src="images/place/places.jpg" />
								<h3 class="desc">Places<span class="round"> <?php echo $all_place; ?></span></h3>
							      </a>
                            </div>

                            <div class="wrap">
                                 <a class="dashboard-images" href="admin-professional-request">
                                <img src="images/category/prequests.jpg" />
                                <h3 class="desc">P-Requests<span class="round"> <?php echo $all_users; ?></span></h3>
                                 </a>
                            </div>
							
							<div class="wrap">
                                 <a class="dashboard-images" href="admin-add-category">
								<img src="images/category/categories.jpg" />
								<h3 class="desc">Categories<span class="round"> <?php echo $all_category; ?></span></h3>
							 </a>
                            </div>
							
							<div class="wrap">
                                 <a class="dashboard-images" href="admin-add-subcategory">
								<img src="images/subcategory/subcategories.jpg" />
								<h3 class="desc">Subcategory<span class="round"> <?php echo $all_subcategory; ?></span></h3>
                                </a>
							</div>
                        </div>
                    </div>
                </div>
                
            </div>                    
        </div>
</div>
