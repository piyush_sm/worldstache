<div class="col-sm-9  col-md-9  main">

                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Tickets Sold</a></li>
                        <!--<li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">Additional Information</a></li>-->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event ticket-sold">
                                <?php if(empty($user_events)) { ?>
                                <p> No Active Event </p>
                                <?php } ?>
                                <?php // pr($user_events); die();?>
                                <?php foreach($user_events as $event) { 
                                $id = base64_encode($event['Event']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($event['Event']['event_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <?php $test = new DateTime($event['Event']['event_start_date']); ?>
                                        <h4><?php echo $event['Event']['event_title']; ?></h4>
                                        <p><i class="fa fa-calendar"></i> <?php echo date_format($test, 'l, F d, Y');?></p>
                                        <p><i class="fa fa-clock-o"></i> <?php echo $event['Event']['event_start_hour']." : ".$event['Event']['event_start_min']." ".$event['Event']['event_start_ap'] ?> </p>
                                    </div>
                                    <div class="adddel revenue">
                                        <p><i class="fa fa-ticket"></i> <strong> Tickets Sold:</strong> <?php echo $event['Event']['tickets_sold'];?></p>
                                        <p><i class="fa fa-money"></i> <strong> Total Revenue:</strong> $<?php echo $event['Event']['total_revenue'];?></p>
                                    </div>
                                    <input type="hidden" name="event_id" id="event_id" value="<?php echo $event['Event']['id'] ?>"/>
                                    <div class="guestlistresult"></div>
                                    <button type="button" class="btn btn-warning" id="showguestlist_<?php echo $event['Event']['id'];?>">Show Guestlist</button>
                                </div>
                                <?php } ?>  
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
            <script>
 jQuery(document).ready(function(){
             jQuery("#showguestlist_<?php echo $event['Event']['id'];?>").on('click',function(){  
                var id= jQuery('#event_id').val();
                //alert(id); return false;
 jQuery.ajax({
             url: "http://dev.staplelogic.in/worldstashdev/users/showguestlist",            
             type:"POST",
             data: ({id:id}),     
             success:function(data)
   {
            
            var innerdiv = jQuery.parseJSON(data);
            jQuery.each(innerdiv, function(i) {
                jQuery(".guestlistresult").append('<div class="add-new-event guestlist"><h5> User Name: '+innerdiv[i]['User'].first_name+'</h5><h4> Tickets taken: '+innerdiv[i]['Payment'].total_tickets+'</h4><p> Amount Spent: '+innerdiv[i]['Payment'].amount+'</p></div>');                                         
            });
            jQuery("#showguestlist_<?php echo $event['Event']['id'];?>").hide();
   }
 })
             })
});
</script>