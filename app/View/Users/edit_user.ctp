<div class="col-sm-9  col-md-9  main">

                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Edit User</a></li>
                        <!--<li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">Additional Information</a></li>-->
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php echo $this->Form->Create('',array('id'=>'edit_user','enctype'=>'multipart/form-data')) ?>
                                    <!--<h2 class="dash-header">Add Event </h2>-->
                                    <div class="edit-field">
                <?php echo $this->Form->input('first_name', array('id'=>'first_name','placeholder'=>'First Name','class'=>'form-control', 'required'=>'required', 'div'=>false,'value'=> $user['User']['first_name'])) ?>
                                </div>
                                    <div class="edit-field">
                <?php echo $this->Form->input('last_name', array('id'=>'last_name','placeholder'=>'Last Name','class'=>'form-control', 'required'=>'required', 'div'=>false,'value'=> $user['User']['last_name'])) ?>
                                </div>
                                    <!--<div class="edit-field">
                <?php //echo $this->Form->input('email', array('id'=>'email','placeholder'=>'Email','class'=>'form-control', 'required'=>'required', 'div'=>false,'value'=> $user['User']['email'])) ?>
                                </div> -->
                                <div class="edit-field">
                <?php echo $this->Form->input('phone_number', array('id'=>'phone_number','placeholder'=>'Phone Number','class'=>'form-control', 'required'=>'required', 'div'=>false,'value'=> $user['User']['phone_number'])) ?>
                                </div>
                                
                                 <div class="edit-field">
								 <label>Address</label>
                                <?php echo $this->Form->textarea('address', array('id'=>'address','placeholder'=>'Address','class'=>'form-control', 'value'=> $user['User']['address'],'required'=>'required')) ?>
                                </div>

                                <div class="add-new-event adddish">
                                    <?php if(empty($user['User']['profile_image'])) { ?>
                                    <?php echo $this->Html->image("Icon-user.png", array("alt"=>"event_img"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($user['User']['profile_image'], array("width"=>"120px","height"=>"100px","alt"=>"event_img"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label>Profile Image</label>
                                        <?php echo $this->Form->input('profile_image', array('id'=>'profile_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning guest_form" value="Update Profile">
                                </div>
                              <?php echo $this->Form->end(); ?>     
                            </div>  
                        </div>
                    </div>
                </div>

            </div>
