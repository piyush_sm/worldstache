<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Users</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">Professional Users</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php // pr($all_users);?>
                            <?php if(empty($all_users)) { ?>
                                <p> No Users Present </p>
                                <?php } ?>
                            <?php foreach($all_users as $user) { 
                                $id = base64_encode($user['User']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($user['User']['profile_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $user['User']['first_name']; ?> <?php echo $user['User']['last_name']; ?></h4>
                                        <p><i class="fa fa-envelope"></i> Email: <?php echo $user['User']['email'];?></p>
                                        <?php if(!empty($user['User']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $user['User']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($user['User']['address'])) { ?>
                                        <p><i class="fa fa-map-marker"></i> Address: <?php echo $user['User']['address'];?></p>
                                        <?php } ?>
                                        <?php if($user['User']['status'] == '1') { ?> 
                                        <p><i class="icon-ban-circle"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="icon-ban-circle"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                       <a href='edit-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                      <a href='deactivate-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Deactivate </button></a>
                                       <a href='delete-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            </div>
                            <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_professionals)) { ?>
                                <p> No Professional user </p>
                                <?php } ?>
                                <?php foreach($all_professionals as $professional) { 
                                $id = base64_encode($professional['User']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php if($professional['User']['profile_image'] == "") {
                                        $professional['User']['profile_image'] = "Icon-user.png";
                                    } ?>
                                    <?php echo $this->Html->image($professional['User']['profile_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $professional['User']['first_name']; ?> <?php echo $professional['User']['last_name']; ?></h4>
                                        <p><i class="fa fa-envelope"></i> Email: <?php echo $professional['User']['email'];?></p>
                                        <?php if(!empty($professional['User']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $professional['User']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($professional['User']['address'])) { ?>
                                        <p><i class="fa fa-map-marker"></i> Address: <?php echo $user['User']['address'];?></p>
                                        <?php } ?>
                                        <?php if($professional['User']['status'] == '1') { ?> 
                                        <p><i class="icon-ban-circle"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="icon-ban-circle"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                     <div class="adddel">
                                       <a href='edit-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                      <a href='deactivate-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Deactivate </button></a>
                                       <a href='delete-user?uid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                    </div>
                </div>
          </div>
</div>