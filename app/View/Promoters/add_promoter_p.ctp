<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#create">View Page Info</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">General Information</a></li>
                        <li role="presentation"><a aria-controls="info" data-toggle="tab" id="info-tab" role="tab" href="#info">View Graphics</a></li>
                    
                    <div class="tab-content" id="myTabContent">
						<div aria-labelledby="profile-tab" id="create" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_promoters) || $all_promoters == null) { ?>
                                <p>Page Info not Available</p>
                                <?php } else { ?>
                                <?php foreach($all_promoters as $promoter) { 
                                $id = base64_encode($promoter['Promoter']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($promoter['Promoter']['header_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $promoter['Promoter']['title']; ?></h4>
                                        <p><strong>Company Name :</strong> <?php echo $promoter['Promoter']['company_name']; ?></p>
                                        <p><strong>Description : </strong><?php echo $promoter['Promoter']['about']; ?></p>
                                        <?php if($promoter['Promoter']['page_status'] == '1') { ?> 
                                        <p><strong>Status:</strong> Active </p>
                                        <?php } else { ?>
                                        <p>Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($promoter['Promoter']['page_status'] == '0') { ?>
                                       <a href='../promoters/edit_promoter_p?eid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <?php } ?>
                                       <a href='../promoters/delete_promoter_p?eid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } }?>
                                
                               
                               </div> 
                        </div>
                        <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                    <?php echo $this->Form->Create('',array('id'=>'add_promoter_p','enctype'=>'multipart/form-data')) ?>
                
                      <div class="edit-field">
                <?php echo $this->Form->input('company_name', array('id'=>'company_name','placeholder'=>'Company Name','class'=>'form-control', 'required'=>'required', 'div'=>false )) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('title', array('id'=>'title','placeholder'=>'Title','class'=>'form-control', 'required'=>'required', 'div'=>false )) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('worldstash_url', array('id'=>'worldstash_url','placeholder'=>'Worldstash Url','class'=>'form-control', 'required'=>'required', 'div'=>false )) ?>
                                </div>
                      <div class="edit-field">
                                    <label> About </label>
                <?php echo $this->Form->textarea('about', array('id'=>'about','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'required')) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('website', array('id'=>'website','placeholder'=>'Website Url','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('youtube_channel', array('id'=>'youtube_channel','placeholder'=>'YouTube Channel','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('featured_youtube_video', array('id'=>'featured_youtube_video','placeholder'=>'Youtube Video Url','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('facebook_page', array('id'=>'facebook_page','placeholder'=>'Facebook Page Url','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('twitter_account', array('id'=>'twitter_account','placeholder'=>'Twitter Account','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                        <div class="edit-field">
                                    <label> Twitter Widget </label>
                <?php echo $this->Form->textarea('twitter_widget', array('id'=>'twitter_widget','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'', 'div'=>false)) ?>
                                 </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('instagram_account', array('id'=>'instagram_account','placeholder'=>'Instagram Account','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                 </div>

                </div> 
                        </div>
                        
                        <div aria-labelledby="info-tab" id="info" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"header_image"));?>
                                    <div class="addnew-discription">
                                        <label>Header Image</label>
                                        <?php echo $this->Form->input('header_image', array('id'=>'header_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"cover_image"));?>
                                    <div class="addnew-discription">
                                        <label>Cover Image</label>
                                        <?php echo $this->Form->input('cover_image', array('id'=>'cover_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"tile_image"));?>
                                    <div class="addnew-discription">
                                        <label>Tile Image</label>
                                        <?php echo $this->Form->input('tile_image', array('id'=>'tile_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                               <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save Page">
                          
                                </div>
                                <?php echo $this->Form->end(); ?>
                               </div>
                        </div>
                        
                    </div>
                </div>
            </div>
