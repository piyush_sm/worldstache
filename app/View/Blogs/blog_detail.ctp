<div class="event-main">
                <div class="sld-main sld-detail">
                    <ul class="detail-slider">
                        <li><?php echo $this->Html->image($blog_detail['Blog']['post_image']) ?> </li>
                    </ul>
                    
                    <h3>
                        <?php echo $blog_detail['Blog']['post_title'];?>
                    </h3>
                    
                    <p>
                        <?php echo $blog_detail['Blog']['post_description'];?>
                    </p>
                    <ul class="share-detail">
                        <li><div class="g-plus" data-action="share" data-annotation="none"></div></li>
                        <li onclick="share_blog();"><a href="javascript:void(0)"><img alt="" src="/worldstashdev/img/fbshare.png"></a></li>
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-pinterest-square"></i></a></li>-->
                        <li><a class="twitter-share-button" href="https://twitter.com/share" data-count="none"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-shape="round" data-pin-height="32"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /><i class="fa fa-pinterest-square"></i></a></li>
                        <li><div class="fb-like" data-href="https://www.facebook.com/Sevilla.WorldStache" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
                    </ul>
                </div>

                <!--<a class="twitter-timeline"  href="https://twitter.com/piyush20rana" data-widget-id="588198144934051840">Tweets by @piyush20rana</a>
         <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>-->
            </div>
        </div>
        <script>
        function share_blog(){
            var id = "<?php echo $id; ?>";
            var path = "<?php echo $blogs['Blog']['post_image']; ?>"
            var image = "http://staplelogic.com/worldstashdev"+path;
            FB.ui({
              method: 'feed',
              link: '<?php echo Router::url(null, true ); ?>?eid='+id,
              caption: "<?php echo $blogs['Blog']['post_title']; ?>",
              picture: image,
              description: "<?php echo $blog_detail['Blog']['post_description']; ?>",
            }, function(response){});
    } 
        </script>
        <script>
        window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
        </script>
        <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>