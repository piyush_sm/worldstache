<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Edit Post</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#images">View Posts</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'edit_blog', 'enctype'=>'multipart/form-data')) ?>
                            <div class="edit-field">
                <?php echo $this->Form->input('post_title', array('id'=>'post_title','placeholder'=>'Post Name','class'=>'form-control', 'required'=>'required', 'div'=>false, 'value'=>$post['Blog']['post_title'])) ?>
                                </div>
                             <div class="edit-field">
                             <label>Description
                                    <a class="addtooltip" data-original-title="Description about the post" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                             </label>
                <?php echo $this->Form->textarea('post_description', array('id'=>'post_description','rows'=>'5','cols'=>'8','class'=>'span6','required'=>'required', 'value'=>$post['Blog']['post_description'])) ?>
                             </div>
                            <div class="add-new-event adddish">
                            <?php if(empty($post['Blog']['post_image'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"logo"));?>
                                  <?php } else { ?>
                                   <?php echo $this->Html->image($post['Blog']['post_image'], array("width"=>"120px","height"=>"100px","alt" => "Logo"));?>
                                  <?php } ?>  
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('post_image', array('id'=>'post_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save Post">
                            </div>
                            </div>
                            </div>
                            <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                
                                <?php foreach($all_posts as $post) { 
                                $id = base64_encode($post['Blog']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($post['Blog']['post_image'], array("width"=>"120px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $post['Blog']['post_title']; ?></h4>
                                    </div>
                                    <div class="adddel">
                                       <a href='../blogs/edit_blog?bid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Edit</button></a>
                                       <a href="../blogs/delete_blog?bid=<?php echo $id; ?>"><button type="button" onclick="if (confirm("Are you sure you want to delete this event ?")) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div> 
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
