             <div class="event-main thumbeve" id="masonry">
                <?php foreach ($all_blogs as $blog) { ?>
                <?php $id = base64_encode($blog['Blog']['id']); ?>
                <div class="event itemeve">
                    <a href="blog-detail?bid=<?php echo $id; ?>">
                        <span>
                            <span><?php echo $blog['Blog']['post_title'];?></span>
                        </span>
                        <?php if(empty($blog['Blog']['post_image'])) { ?>
                        <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_img"));?>
                        <?php } else { ?>
                         <?php echo $this->Html->image($blog['Blog']['post_image'], array("alt"=>"event_img"));?>
                        <?php } ?>
                    </a>
                </div>
                <?php } ?>  
            </div>
        </div>
