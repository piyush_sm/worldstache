<style type="text/css">
  ul.countdown {
  list-style: none;
  margin: 75px 0;
  padding: 0;
  display: block;
  text-align: center;
  }
  ul.countdown li {
  display: inline-block;
  }
  ul.countdown li span {
  font-size: 40px;
  font-weight: 300;
  line-height: 80px;
  }
  ul.countdown li.seperator {
  font-size: 40px;
  line-height: 70px;
  vertical-align: top;
  }
  ul.countdown li p {
  color: #a7abb1;
  font-size: 14px;
  }
 #map-canvas {
        width: 365px;
        height: 365px;
      }
      

.container {
  margin: 0px auto;
  padding: 0px;
}

#main { 
  background: #3B3B3B;
  height: 430px;
}

.content {
  padding: 10px 44px;
}

.text {
  border-bottom: 1px solid #262626;
  margin-top: 40px;
  padding-bottom: 40px;
  text-align: center;
}

.text h2 {
  color: #E5E5E5;
  font-size: 30px;
  font-style: normal;
  font-variant: normal;
  font-weight: lighter;
  letter-spacing: 2px;
}

.counter {
  background: #2C2C2C;
  -moz-box-shadow:    inset 0 0 5px #000000;
  -webkit-box-shadow: inset 0 0 5px #000000;
  box-shadow:         inset 0 0 5px #000000;
  min-height: 150px;
  text-align: center;
}

.counter h3 {
  color: #E5E5E5;
  font-size: 14px;
  font-style: normal;
  font-variant: normal;
  font-weight: lighter;
  letter-spacing: 1px;
  padding-top: 20px;
  margin-bottom: 30px;
}

#countdown {
  color: #FFFFFF;
}
.like-detail li { 
    cursor: pointer; 
}

</style>    

    <script>
      function show_price(val){
      
            jQuery('#loading').css({'display':'block'}); 
            var id = "<?php echo $event_detail['Event']['id']; ?>";
            var type = jQuery("#tickets_type").val();
            var num = jQuery("#tickets").val();
            if(num == "" || num == null){
            jQuery('#detaildv').hide();
         jQuery('#loading').css({'display':'none'}); 
                return false
            } else if(type ==""){
            jQuery('#detaildv').hide();
        jQuery('#loading').css({'display':'none'}); 
         return false
        
        }else {
            
            jQuery.ajax({
                        type: "POST",
                        url: 'http://dev.staplelogic.in/worldstashdev/events/getpricedescription',
                        data: {id: id, type: type},
                        success: function(message){
              //console.log(message); return false;
                        jQuery('#loading').css({'display':'none'});    
                        var options = jQuery.parseJSON(message);                        
                        var price = options['price'];
                       // alert(price);
                        var new_description = options['description'];

                        var new_price = parseInt(num) * parseInt(price);
                        //alert(new_price);
                        if(new_price > '0'){
            jQuery('#detaildv').show();
                        jQuery("#new_price").html('');
                        jQuery("#new_description").html('');
                        jQuery("#new_description").append(new_description);                        
                        jQuery("#new_price").append('<span id="new_price">$'+new_price+'</span>');
              }else{
              jQuery('#detaildv').hide();
              }
                        
                        },
                        
                    });

            
            }
        }


        function like_tag(name,id){
    //jQuery('#loading').css({'display':'block'});
    var tag_name = name;    
       jQuery.ajax({
                        type: "POST",
                        url: 'http://dev.staplelogic.in/worldstashdev/events/liketag',
                        data: {tag_name: tag_name},
                        success: function(message){
            var options = jQuery.parseJSON(message);
                        jQuery('#loading').css({'display':'none'});                       
                        jQuery('#'+tag_name).html('');                        
                       jQuery('#'+ tag_name).append(options.count);                        
                         var img_id= 'img_' + id;  
                        // alert(options.Taglike.status);
                        // console.log(options.like); 
                         //return false ;                    
                       if(options.like ==1){                             
                       document.getElementById(img_id).src = 'http://png.findicons.com/files/icons/2015/24x24_free_application/24/heart.png';
               }
             else  if(options.like ==0){
            document.getElementById(img_id).src = 'http://iconbug.com/data/c9/24/9a43dd5429cfe72de7f840e304d679c0.png';            
             }
                        },
                        
                    });
    }








        function initialize() {
        var geocoder= new google.maps.Geocoder();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI:true
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var address = "<?php echo $event_detail['Event']['event_city'];?>";
        //var address = "Phase 3B2, Mohali, India";
      geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location

        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
      } 

    google.maps.event.addDomListener(window, 'load', initialize);
    
    
    $( document ).ready(function() { 
      jQuery('#detaildv').hide();
    <?php  $event_dt= new DateTime($event_detail['Event']['event_end_date']); 
    $dt= $event_dt->format('M ,d ,Y '); ?>
   var target_date = new Date('<?php echo $dt; ?>').getTime();
var days, hours, minutes, seconds;
 
var countdown = document.getElementById('countdown');

setInterval(function () {
    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;
 
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;
     
    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;
     
    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days &nbsp;</b></span> <span class="hours">' + hours + ' <b>&nbsp;:</b></span> <span class="minutes">'
    + minutes + ' <b> :</b></span> <span class="seconds">' + seconds + ' <b></b></span>';  
 
}, 1000);
    });
    
    </script>
    <?php  //print_r($likestatus); ?>
<div id="loading" style="position: absolute;background: black; width: 100%; height: 100%; opacity: 0.7; z-index: 100000;text-align: center;display:none;"><img src="http://jimpunk.net/Loading/wp-content/uploads/loading37.gif" style=" width: 240px;margin-top: 203px;"> <h3 style="color:red; margin:-35px 0 0;"> loading...</h></div>
<div class="event-main">
                <div class="sld-main sld-detail">
                    <ul class="detail-slider">
                        <li><?php echo $this->Html->image($event_detail['Event']['event_image']) ?> </li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_flyer']) ?></li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_cover_image']) ?></li>
                    </ul>
                    <div class="like-detail">
                        <ul id="menu">
							<?php $tags = explode(',',$event_detail['Event']['event_tags']);
                                $num = count($tags);
                                for($i = 0; $i <= $num-1; $i++){
                            ?>
                            <li id="<?php echo $i; ?>" onclick='like_tag("<?php echo $tags[$i]; ?>",<?php echo $i ?>)'>
										<?php
										if (in_array($tags[$i], $likestatus))
										{?>
									<img src="http://png.findicons.com/files/icons/2015/24x24_free_application/24/heart.png" id="img_<?php echo $i; ?>" />
									<span class="heart-counting" id="<?php echo $tags[$i]?>">
                             <?php echo $event_tags[$i]; ?></span><label class="heart-text"><?php echo $tags[$i]?></label>
									   <?php }
										else
										{?>
										<img src="http://iconbug.com/data/c9/24/9a43dd5429cfe72de7f840e304d679c0.png" id="img_<?php echo $i; ?>" />
										<span class="heart-counting" id="<?php echo $tags[$i]?>">
                              <?php echo $event_tags[$i]; ?></span> <label class="heart-text"><?php echo $tags[$i]?></label>
										<?php }
										?>
                        
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <h3>
                        <?php $id = base64_encode($event_detail['Event']['id']);?>
                        <?php echo $event_detail['Event']['event_title'];?>
                    </h3>
                    <ul class="venue">
                        <?php $venue = base64_encode($event_detail['Event']['event_venue']) ?>
                        <?php $test = new DateTime($event_detail['Event']['event_start_date']); ?>
                        <li><i class="fa fa-calendar"></i><?php echo date_format($test, 'l, F d, Y');?> </li>
                        <li><i class="fa fa-clock-o"></i><?php echo $event_detail['Event']['event_start_hour']?>:<?php echo $event_detail['Event']['event_start_min']?><?php echo $event_detail['Event']['event_start_ap']?></li>
                        <li><a href="view-place?pid=<?php echo $venue; ?>"  target="_blank"><i class="fa fa-map-marker"></i><?php echo $place['Place']['name']; ?></a>, <?php echo $event_detail['Event']['event_city']?>&nbsp;</li>
                    </ul>
                    <span class="desc">
                    <p>
                        <?php echo $event_detail['Event']['event_description'];?>
                    </p>
                    </span>
                    <ul class="share-detail">
                        <li><div class="g-plus" data-action="share" data-annotation="none"></div></li>
                        <li><a href="javascript:void(0)" class="share-fb"><img alt="" src="/worldstashdev/img/fbshare.png"></a></li>
                        <li><a class="twitter-share-button" href="https://twitter.com/share" data-count="none"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-shape="round" data-pin-height="32"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /><i class="fa fa-pinterest-square"></i></a></li>
                        <li><div class="fb-like" data-href="https://www.facebook.com/Sevilla.WorldStache" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
                    </ul>
                        <?php if($user_id != "" && $user_id != null) { ?>
                        <?php if($event_detail['Event']['allow_comments'] == 1 ) {?>
    <div class="put-cmt">
        <span><?php echo $this->Html->image($user_data,array('width'=>'100','height'=>'100'));?></span>
        <form action="post-comment?eid=<?php echo $id;?>" method="post">
            <textarea name="data[Comment][comment]" class="form-control" placeholder="Comment here"></textarea>
            <input type="submit" value="Comment">
        </form>
    </div>
    <?php } } ?>

        <?php if(!empty($all_comments) || $all_comments != null){ ?>
            <h4>Reviews</h4>
            <?php foreach($all_comments as $comment) { ?>
                <div class="review-main">
               <?php echo $this->Html->image($comment['User']['profile_image'],array('width'=>'50','height'=>'50'));?> <span><?php echo $comment['Comment']['comment'];?></span>
                </div>
               <?php } }?>

        </div>
            
                <div class="map-detail">
                    <div id="map-canvas"></div>
                    <div class="lmtd-time">
                        <h3>LIMITED TIME ONLY!</h3>
                        <?php
                                $date1 = new DateTime(date('m/d/Y h:i:s'));
                                $date2 = new DateTime($event_detail['Event']['event_end_date'].' '.$event_detail['Event']['event_end_hour'].':'.$event_detail['Event']['event_end_min'].':00');
                                $diff=date_diff($date1,$date2);
                                $result2 = $date2->format('M ,d ,Y ');
                                ?>
                        <span><i class="fa fa-clock-o"></i><label id="countdown"></label><?php //echo $diff->d;?>   <?php // echo $diff->format('%h:%i:%s');?></span>
                        
                    </div>
                
                    <?php if($user_id != "" && $user_id != null) { ?>
                    <form action="http://dev.staplelogic.in/worldstashdev/events/event_detail?eid=<?php echo $id;?>" method="post">
                    <div class="lmtd-time qunt slcttyp">
                                <?php $types = unserialize($event_detail['Event']['event_ticket']);?>
                                <select name = "total_type" id="tickets_type" empty="Select ticket type" onchange="show_price(this.value);">
                                    <option value="">Select Ticket Type</option>
                                   <?php foreach($types as $type) { ?>
                                    <option value="<?php echo $type;?>"><?php echo $type;?></option>
                                    <?php } ?>
                                </select>
                                <?php if(!empty($event_detail['Event']['plus_max'])) {
                                $ticket = $event_detail['Event']['plus_max'];
                                      $tickets = $ticket + 1;
                                  } else {
                                $tickets =  $event_detail['Event']['plus_max'];
                                  }
                                ?>
                                <select name = "total_tickets" id="tickets" empty="Select ticket" onchange="show_price(this.value);">
                                    <option value="0">Select Tickets</option>
                                    <?php for($i=1; $i <= $tickets; $i++) { ?>
                                    <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php } ?>
                                </select>
                                <div id="detaildv">
                                  <h5 id="ticket_description">Ticket Description:<span class="descp" id="new_description"></span></h5>
                                  <h5 id="ticket_price">Ticket price:<span id="new_price"></span></h5>
                                  
                                          <input type="hidden" name="event_id" value="<?php echo $event_detail['Event']['id'];?>">
                                          <input type="hidden" name="cmd" value="_xclick">
                                          <input type="hidden" name="amount" value="<?php echo $event_detail['Event']['event_price'];?>">
                                          <input type="hidden" name="item_name" value="<?php echo $event_detail['Event']['event_title'];?>">
                                          <input type="hidden" name="currency_code" value="USD">
                                          <input type="hidden" name="return" value="http://dev.staplelogic.in/worldstashdev/events/success">
                                          <input type="submit" name="Pay Now" value="Pay via Paypal" id="pay_now">
                                  
                           
                                <a href="#" id="pay_from_card" onclick="pay_from_card();">Pay From Card</a> 
                                <a href="#" id="cash_on_deleivery" onclick="cod();">Cash On delivery</a> 
                              </div>
                    </div>
                  </form>
                    <?php } else { ?>
                    <p> You need to sign in to buy tickets </p>
                    <?php } ?>
                </div>
                <div class="more-events">
                    <h3>More Upcoming Events...</h3>
                    <div class="slider1">
                        <?php foreach($upcoming_events as $events) { ?>
                        <?php $id = base64_encode($events['Event']['id'])?>
                        <div class="slide">
                        <?php echo $this->Html->link(
                            $this->Html->image($events['Event']['event_image'], array("alt" => "event_img")),"/event-detail?eid=".$id,array('escape' => false, 'target'=>'_blank'));?>
                        <?php echo $events['Event']['event_title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <script>
        $( document ).ready(function() {
            
          $(".share-fb").on("click", function(){
            var desc = $(".desc").html();
            var text = $(desc).text();
            alert(text);
            var id = "<?php echo $id; ?>";
            var path = "<?php echo $events['Event']['event_image']; ?>";
            var image = '<?php echo Router::url("/", true); ?>'+path;
            alert(image);
            FB.ui({
              method: 'feed',
              link: '<?php echo Router::url(null, true ); ?>?eid='+id,
              caption: "<?php echo $events['Event']['event_title']; ?>",
              picture: image,
              description: text
              }, function(response){});
            });

        });
        /*function share_event(){
            //var desc = '<?php echo $events["Event"]["description"]; ?>';
            //alert(desc);
            var id = "<?php echo $id; ?>";
            var path = "<?php echo $events['Event']['event_image']; ?>";
            var image = "http://staplelogic.com/worldstashdev"+path;
            FB.ui({
              method: 'feed',
              link: '<?php echo Router::url(null, true ); ?>?eid='+id,
              caption: "<?php echo $events['Event']['event_title']; ?>",
              picture: image,
              description: $(".desc").html()
            }, function(response){});
        } */
    
    
		
        
        
        function pay_from_card(){
            var id = "<?php echo $id; ?>";
            var type = jQuery("#tickets_type").val();
            var num = jQuery("#tickets").val();
            window.location.href = "http://dev.staplelogic.in/worldstashdev/pay-by-stripe?type="+type+"&num="+num+"&ids="+id;

        }
        function cod(){
		
            var id = "<?php echo $id; ?>";
            var type = jQuery("#tickets_type").val();
            var num = jQuery("#tickets").val();
            window.location.href = "http://dev.staplelogic.in/worldstashdev/cash-on-delievery?type="+type+"&num="+num+"&ids="+id;
        }
        </script>

        <script>
        window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
        </script>
        <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
