	<div class="content">
        <?php echo $this->Session->flash(); ?>
        <div class="header">
            
            <h1 class="page-title">List Event </h1>
        </div>
<div class="well add-cate">
    <table class="table">
      <thead>
        <tr>
          <th>#</th>
          <th>Event Name</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
<?php foreach($all_events as $events) { 

	$id = base64_encode($events['Event']['id']);
	?>
        <tr>
          <td> <?php echo $events['Event']['id']; ?></td>
          <td> <?php echo $events['Event']['event_title']; ?> </td>
          <td>
	   <?php echo $this->Html->link('',array('controller'=>'events','action'=>'edit_event', '?'  => array('eid' => $id)),array('class'=>'edit fa fa-edit','title'=>'Edit')); ?>
	   <?php echo $this->Html->link('',array('controller'=>'events','action'=>'delete_event', '?'  => array('eid' => $id)),array('confirm'=>'Are you sure you want to delete this Event ?','class'=>'delet fa fa-remove','title'=>'Delete')); ?>
          </td>
        </tr>
<?php } ?>
      </tbody>
    </table>
</div>
