<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=100" />
<link rel="stylesheet" type="text/css" href="/worldstashdev/css/style.css" />
<title>Cash on Delivery</title>
</head>
<body>
<section class="main-cont">
  <div class="left-side-add"> </div>
  <div class="center-cont">
    <h2>Cash On Delivery</h2>

    <div class="tkt-type"><?php echo "Ticket Type => ".$ticket_type; ?></div>

    <div class="tkt-type"><?php echo "No. of Tickets => ".$ticket_num; ?></div>
    <?php if(isset($status)) { ?>
    <?php if($status = 1) { ?>

            <div class="tkt-type">Your order is successfull</div>
    <?php } else { ?>
            <div class="tkt-type">Your order is not placed</div>
    <?php } } else { ?>

    <?php echo $this->Form->Create('',array('id'=>'stripe_payment','enctype'=>'multipart/form-data')) ?>
    <div class="cod-form">
    <?php echo $this->Form->input('email_address', array('id'=>'email_address','class'=>'form-control', 'required'=>'required', 'div'=>false)) ?>
    </div>
    <div class="cod-form">
    <?php echo $this->Form->input('address', array('id'=>'address','class'=>'form-control', 'required'=>'required', 'div'=>false)) ?>
    </div>
    <?php echo $this->Form->input('event_id', array('type'=>'hidden','id'=>'event_id','class'=>'form-control', 'value'=>$event_detail['Event']['id'] ,'label'=>false , 'required'=>'required', 'div'=>false)) ?>

    <?php echo $this->Form->input('event_owner_id', array('type'=>'hidden','id'=>'event_owner_id','class'=>'form-control', 'value'=>$event_detail['User']['id'] ,'label'=>false , 'required'=>'required', 'div'=>false)) ?>

    <?php echo $this->Form->input('total_type', array('type'=>'hidden','id'=>'total_type','class'=>'form-control', 'value'=>$ticket_type,'label'=>false , 'required'=>'required', 'div'=>false)) ?>

    <?php echo $this->Form->input('total_tickets', array('type'=>'hidden','id'=>'total_tickets','class'=>'form-control', 'value'=>$ticket_num ,'label'=>false , 'required'=>'required', 'div'=>false)) ?>
    
    <input type="submit" name="" class="btn-card-info" value="Confirm" id="">
    <?php echo $this->Form->end(); ?>
    <?php } ?>
  </div>
</section>
</body>
</html>