<?php      
    $locations=array();
    if(!empty($all_events) && isset($all_events)){      
        foreach($all_events as $location)
        {
            $locations[] = array('city'=> $location['Event']['event_city'],'lag'=>    $location['Event']['event_city_lat'], 'long'=>$location['Event']['event_city_long']);   
        }       
    }
?>
<div id="loading" style="position: absolute;background: black; width: 100%; height: 100%; opacity: 0.7; z-index: 100000;text-align: center;display:none;"><img src="http://jimpunk.net/Loading/wp-content/uploads/loading37.gif" style=" width: 240px;margin-top: 203px;"> <h3 style="color:red; margin:-35px 0 0;"> loading...</h></div>
<div class="event-main">
    <div class="left-bar">
        <div class="left-map">
                <div id="map_2385853" style="width:200px; height:200px;"></div>
        </div>
        <div class="filter">
           
            <input type="hidden" class="min-range" value="0" />
            <input type="hidden" class="max-range" value="1000" /> 
            <div class="cat-filter">
            <h3>Search By Category</h3>
            <?php $startdate = new DateTime(date('m/d/Y'));
            $sdate = $startdate->format('m/d/Y');
            $sdatetimestamp = strtotime($sdate);
            $all_count = 0;
            foreach ($cat_drp as $drp) {
                foreach($drp['Event'] as $cevent){
                        if($cevent['event_status'] == 1 && $cevent['event_end_timestamp'] >= $sdatetimestamp){
                            $all_count = $all_count + 1;
                        }
                    }
            }
            ?>
            <?php foreach ($cat_drp as $drp) { ?>
            <div class="filter-cont">
                <?php if($drp['Category']['id'] != 111) {  ?>
                <input class="checkbox" type="checkbox" name="categoriesname" value="<?php echo $drp['Category']['id']; ?>">
                <?php } ?>
                <span><?php echo $drp['Category']['category_name']; ?>
                <?php if($drp['Category']['id'] == 111) { ?>
                        
                        <span><?php echo $all_count;?></span></span>
                <?php } elseif(!empty($drp['Event'])) {
                    $count = 0;
                    foreach($drp['Event'] as $cevent){
                        if($cevent['event_status'] == 1 && $cevent['event_end_timestamp'] >= $sdatetimestamp){
                            $count = $count + 1;
                        }
                    }
                 ?>
                 <span><?php echo $count;?></span></span><?php } else { ?>
                    <span>0</span></span>
               <?php } ?>
            </div>
            <?php } ?>
        </div>    
            <div class="sub_cat_list"></div>
    
            <h4>Search By Price</h4>
            <div class="filter-cont">
                <h5>Price Range</h5>
                <p>Choose Price Range To Search In</p>                            
                <input type="text" id="age" readonly style="border:0; color:#f6931f; font-weight:bold;">
                <div id="slider-range_age"></div>
            </div>
        </div>
    </div>
    <div class="content-left" >
        <?php  $event_id_arr=''; if(empty($all_events) || $all_events == null ) { ?>
        <p>No Events matches your search </p>
        <?php } else { $val = 1; $val_static = 1; ?>
        <?php  foreach($all_events as $event) { ?>
        <?php $id = base64_encode($event['Event']['id']); ?>
        <div class="event-thumb search_result" id="search_result_<?php echo $event['Event']['id']; ?>">
            <a href="event-detail?eid=<?php echo $id; ?>">
                <?php echo $this->Html->image($event['Event']['event_image'], array("alt"=>"event_img"));?>
                <h5><?php echo $event['Event']['event_city'];?> - <?php echo $event['Event']['event_title'];?></h5>
              </a>               
            <h4><?php echo $event['Event']['event_title'];?></h4>
            <p><?php echo $event['Event']['event_start_hour'];?>:<?php echo $event['Event']['event_start_min'];?> <?php echo $event['Event']['event_start_ap'];?> - <?php echo $event['Event']['event_end_hour'];?>:<?php echo $event['Event']['event_end_min'];?> <?php echo $event['Event']['event_end_ap'];?></p>
            <?php if($val == 1 || $val == $val_static + 3 ) { ?>
            <div class="pov">
                <?php } else { ?>
                <div class="pov left-pophover">
                <?php } ?>
                <ul class="venue">
                    <?php $venue = base64_encode($event['Event']['event_venue']) ?>
                    <p><?php echo $event['Event']['event_title'];?></p>
                    <?php $test = new DateTime($event['Event']['event_start_date']); ?>
                    <li><i class="fa fa-calendar"></i><?php echo date_format($test, 'l, F d, Y');?></li>
                    <li><i class="fa fa-clock-o"></i><?php echo $event['Event']['event_start_hour'];?>:<?php echo $event['Event']['event_start_min'];?> <?php echo $event['Event']['event_start_ap'];?> - <?php echo $event['Event']['event_end_hour'];?>:<?php echo $event['Event']['event_end_min'];?> <?php echo $event['Event']['event_end_ap'];?></li>
                    <li><a href="view-place?pid=<?php echo $venue; ?>"  target="_blank"><i class="fa fa-map-marker"></i><?php echo $event['Place']['name']; ?></a>, <?php echo $event['Event']['event_city'];?>

                    </li>
                </ul>

                <div class="btn-buy">
                    <h5>Tickets starting from</h5>
                    <?php $price = unserialize($event['Event']['event_ticket_price']);?>
                    <h4>$<?php echo min($price); ?></h4>
                    <a href="event-detail?eid=<?php echo $id; ?>"> BUY NOW </a>
                </div>
                <p class="vika-left-cont">
                    <?php echo $event['Event']['event_description'];?>
                    <a href="event-detail?eid=<?php echo $id; ?>">More</a>
                </p>
            </div>
            <?php     $event_id_arr .= ',' .$event['Event']['id']; ?>
        </div>
        <?php $val = $val + 1; } } ?>
        <input type="hidden" name="event_ids" id="event_ids" value="<?php    echo $event_id_arr; ?>"/>
    </div>
</div>
</div>
<script>
 $(document).ready(function(){	 
             jQuery('body').on('change', '.checkbox',function(){      
             var Cat = [];
             jQuery.each(jQuery("input[name='categoriesname']:checked"), function(){              
              Cat.push(jQuery(this).val());
             });                
             var Catvalue = Cat.join(",");
             var SubCat = [];
                 jQuery.each(jQuery("input[name='subcategoriesname']:checked"), function(){              
                      SubCat.push(jQuery(this).val());
                 });                
            var SubCatvalue=SubCat.join(",");
             //alert(Catvalue);
            jQuery.ajax({
             url: "http://dev.staplelogic.in/worldstashdev/events/searcheventfilter",            
             type:"POST",
             data: ({ 
                  cat_id:Catvalue,
                  subcat_id:SubCatvalue,
                  start_price: $(".min-range").val(),
                  end_price: $(".max-range").val() 
             }),
                 
            success:function(data)
            {
             jQuery('#loading').css({'display':'none'});
                     var outerdiv = jQuery.parseJSON(data);   
                     
                     /* Udpate GOOGLE MAP with new locations*/
                     var locations = outerdiv['locations'];
                     //console.log(locations);
                     var loc_obj = [];
                     i = 1;
                     jQuery.each(locations, function(key,value){
                        var obj = [value.infocity, value.latitude, value.longitude, i];
                        loc_obj.push(obj);
                        i++;
                     });
                    
                     getMap(loc_obj);
                     /* END */

                     var subcategorydiv = outerdiv['Subcategory'];
                     jQuery(".sub_cat_list").html('');
                     jQuery(".sub_cat_list").append('<h4>Search By Subcategory</h4>');
                        
                     jQuery.each(subcategorydiv, function(key,value){
                        var sub_count = 0;
                      jQuery.each(outerdiv['Events'], function(i) {
                         if(outerdiv['Events'][i]['Event'].sub_cat_id === key){
                            sub_count = sub_count + 1;
                         } else {
                            subcount = 0;
                         }
                      });
               
                    if(SubCat.indexOf(key) >= 0){
                      jQuery(".sub_cat_list").append('<div class="filter-cont"><input type="checkbox" value="'+key+'" name="subcategoriesname" class="checkbox" checked><span>'+value+'<span>'+sub_count+'</span></span></div>');
                    }else{
                      jQuery(".sub_cat_list").append('<div class="filter-cont"><input type="checkbox" value="'+key+'" name="subcategoriesname" class="checkbox"><span>'+value+'<span>'+sub_count+'</span></span></div>');
                    }
                });

              /* Reattache Dom */
              addsubcheckbox();

             /* **************** */
             var innerdiv = outerdiv['Events'];
             
             if(innerdiv != "" || innerdiv != null){
                jQuery(".content-left").empty();
            }
            jQuery.each(innerdiv, function(i) {
               var serializedStr = innerdiv[i]['Event'].event_ticket_price;
                      var price = unserialize(serializedStr);
                      var ticket_price = Math.min.apply(Math,price);

                              // Create Base64 Object
                      var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

                       var string = innerdiv[i]['Event'].id;

                       var encodedString = Base64.encode(string);

                       jQuery(".content-left").append('<div id="search_catresult_"'+ innerdiv[i]['Event'].id+'" class="event-thumb search_catresult"><a href="event-detail?eid='+encodedString+'"><img alt="event_img" src="/worldstashdev/'+innerdiv[i]['Event'].event_image+'"><h5>'+innerdiv[i]['Event'].event_city+' - '+innerdiv[i]['Event'].event_title+'</h5><span><i class="fa fa-heart"></i><span>66</span></span></a><h4>'+innerdiv[i]['Event'].event_title+'</h4><p>'+innerdiv[i]['Event'].event_start_hour+':'+innerdiv[i]['Event'].event_start_min+' '+innerdiv[i]['Event'].event_start_ap+' - '+innerdiv[i]['Event'].event_end_hour+':'+innerdiv[i]['Event'].event_end_min+' '+innerdiv[i]['Event'].event_end_ap+'</p><p>'+'</p><div class="pov"><ul class="venue"><p>'+innerdiv[i]['Event'].event_title+'</p><li><i class="fa fa-clock-o"></i>'+innerdiv[i]['Event'].event_start_hour+':'+innerdiv[i]['Event'].event_start_min+' '+innerdiv[i]['Event'].event_end_ap+' - '+innerdiv[i]['Event'].event_end_hour+':'+innerdiv[i]['Event'].event_end_min+' '+innerdiv[i]['Event'].event_end_ap+'</li><li><a href="#"><i class="fa fa-map-marker"></i>'+innerdiv[i]['Place'].name+'</a>,'+innerdiv[i]['Event'].event_city+'</li></ul><div class="btn-buy"><h5>Tickets starting from</h5><h4>$'+ticket_price+'</h4><a href="event-detail?eid='+encodedString+'"> BUY NOW </a></div><p class="vika-left-cont">'+innerdiv[i]['Event'].event_description+'<a href="event-detail?eid='+encodedString+'">More</a></p></div></div>');
            });          
        }
    })
 })
});



jQuery(function() {
    jQuery( "#slider-range_age" ).slider({
		
      range: true,
      min: 0,
      max: 1000,
      values: [ 0, 1000 ],
        slide: function( event, ui ) {
        jQuery( "#age" ).val(ui.values[ 0 ] + '-' + ui.values[ 1 ] );
        var val_1 = ui.values[ 0 ];
        var val_2 = ui.values[ 1 ];
        $('.min-range').val(val_1);
        $('.max-range').val(val_2);

        var v2= jQuery('#event_ids').val();
        var Cat = [];
        $('.checkbox').each(function(i, el){    
			if (this.checked) {
				Cat.push(jQuery(this).val());			
			}
        });
        var Catvalue = Cat.join(",");
        var SubCat = [];
           jQuery.each(jQuery("input[name='subcategoriesname']:checked"), function(){              
                  SubCat.push(jQuery(this).val());
           });                
        var SubCatvalue=SubCat.join(",");
             // console.log(Catvalue);
     jQuery('#loading').css({'display':'block'});
     jQuery.ajax({
            url: "http://dev.staplelogic.in/worldstashdev/events/searcheventfilter",         
            type:"POST",
            data: ({                    
                        start_price:val_1 ,
                        end_price:val_2 ,
                        cat_id:Catvalue,
                        sub_cat_id:SubCatvalue,
                    }),
                 
            success:function(data)
            {
				
			jQuery('#loading').css({'display':'none'});
            var outerdiv = jQuery.parseJSON(data);
            var innerdiv = outerdiv['Events'];
            /* Udpate GOOGLE MAP with new locations*/
                     var locations = outerdiv['locations'];
                     //console.log(locations);
                     var loc_obj = [];
                     i = 1;
                     jQuery.each(locations, function(key,value){
                        var obj = [value.infocity, value.latitude, value.longitude, i];
                        loc_obj.push(obj);
                        i++;
                     });
                    
                     getMap(loc_obj);
                /* END */


            if(innerdiv != "" || innerdiv != null){
                jQuery(".content-left").empty();
            }

            jQuery("#event_ids").val(''); 
             if(innerdiv=='')
               {
				   
				   jQuery(".content-left").append('<span style="margin-top: 55px; text-align: center">No Result Found. Plaese Refine Your search.....</span>');
				   
				}                         
            var event_id='';

            jQuery.each(innerdiv, function(i) {                            
              event_id += ','+ innerdiv[i]['Event'].id;
                var serializedStr = innerdiv[i]['Event'].event_ticket_price;
                var price = unserialize(serializedStr);
                var ticket_price = Math.min.apply(Math,price);

                // Create Base64 Object
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

                var string = innerdiv[i]['Event'].id;

                var encodedString = Base64.encode(string);

            jQuery(".content-left").append('<div id="search_catresult_"'+ innerdiv[i]['Event'].id+'" class="event-thumb search_catresult"><a href="event-detail?eid='+encodedString+'"><img alt="event_img" src="/worldstashdev/'+innerdiv[i]['Event'].event_image+'"><h5>'+innerdiv[i]['Event'].event_city+' - '+innerdiv[i]['Event'].event_title+'</h5><span><i class="fa fa-heart"></i><span>66</span></span></a><h4>'+innerdiv[i]['Event'].event_title+'</h4><p>'+innerdiv[i]['Event'].event_start_hour+':'+innerdiv[i]['Event'].event_start_min+' '+innerdiv[i]['Event'].event_start_ap+' - '+innerdiv[i]['Event'].event_end_hour+':'+innerdiv[i]['Event'].event_end_min+' '+innerdiv[i]['Event'].event_end_ap+'</p><div class="pov"><ul class="venue"><p>'+innerdiv[i]['Event'].event_title+'</p><li><i class="fa fa-clock-o"></i>'+innerdiv[i]['Event'].event_start_hour+':'+innerdiv[i]['Event'].event_start_min+' '+innerdiv[i]['Event'].event_end_ap+' - '+innerdiv[i]['Event'].event_end_hour+':'+innerdiv[i]['Event'].event_end_min+' '+innerdiv[i]['Event'].event_end_ap+'</li><li><a href="#"><i class="fa fa-map-marker"></i>'+innerdiv[i]['Place'].name+'</a>,'+innerdiv[i]['Event'].event_city+'</li></ul><div class="btn-buy"><h5>Tickets starting from</h5><h4>$'+ticket_price+'</h4><a href="event-detail?eid='+encodedString+'"> BUY NOW </a></div><p class="vika-left-cont">'+innerdiv[i]['Event'].event_description+'<a href="event-detail?eid='+encodedString+'">More</a></p></div></div>');
            });
                
              }
            })
      }
    });
    jQuery( "#age" ).val( jQuery( "#slider-range_age" ).slider( "values", 0 ) +" - " + jQuery( "#slider-range_age" ).slider( "values", 1 ) );
    
  });
   

function addsubcheckbox(){
    jQuery('.subcheckbox').change(function(){
             var SubCat = [];
             jQuery.each(jQuery("input[name='subcategoriesname']:checked"), function(){              
             SubCat.push(jQuery(this).val());
 });                
             var SubCatvalue=SubCat.join(",");
             //alert(SubCatvalue);
 jQuery.ajax({
             url: "http://dev.staplelogic.in/worldstashdev/events/searchresultfilter",            
             type:"POST",
             data: ({ 
                subcat_name:SubCatvalue,
                   }),
                 
             success:function(data)
            {
             var innerdiv = jQuery.parseJSON(data);
             if(innerdiv != "" || innerdiv != null){
                jQuery(".content-left").empty();
            }
            jQuery.each(innerdiv, function(i) {

            jQuery(".content-left").append('<div id="search_catresult_"'+ innerdiv[i]['Event'].id+'" class="event-thumb search_catresult"><a href="event-detail?eid=Mg=="><img alt="event_img" src="/worldstashdev/'+innerdiv[i]['Event'].event_image+'"><h5>'+innerdiv[i]['Event'].event_city+' - '+innerdiv[i]['Event'].event_title+'</h5><span><i class="fa fa-heart"></i><span>66</span></span></a><h4>'+innerdiv[i]['Event'].event_title+'</h4><p>'+innerdiv[i]['Event'].event_start_hour+':'+innerdiv[i]['Event'].event_start_min+' '+innerdiv[i]['Event'].event_start_ap+' - '+innerdiv[i]['Event'].event_end_hour+':'+innerdiv[i]['Event'].event_end_min+' '+innerdiv[i]['Event'].event_end_ap+'</p></div>');
            });              
        }
    })
 });
}

function unserialize(data) {
  var that = this,
    utf8Overhead = function(chr) {
      var code = chr.charCodeAt(0);
      if (code < 0x0080) {
        return 0;
      }
      if (code < 0x0800) {
        return 1;
      }
      return 2;
    };
  error = function(type, msg, filename, line) {
    throw new that.window[type](msg, filename, line);
  };
  read_until = function(data, offset, stopchr) {
    var i = 2,
      buf = [],
      chr = data.slice(offset, offset + 1);

    while (chr != stopchr) {
      if ((i + offset) > data.length) {
        error('Error', 'Invalid');
      }
      buf.push(chr);
      chr = data.slice(offset + (i - 1), offset + i);
      i += 1;
    }
    return [buf.length, buf.join('')];
  };
  read_chrs = function(data, offset, length) {
    var i, chr, buf;

    buf = [];
    for (i = 0; i < length; i++) {
      chr = data.slice(offset + (i - 1), offset + i);
      buf.push(chr);
      length -= utf8Overhead(chr);
    }
    return [buf.length, buf.join('')];
  };
  _unserialize = function(data, offset) {
    var dtype, dataoffset, keyandchrs, keys, contig,
      length, array, readdata, readData, ccount,
      stringlength, i, key, kprops, kchrs, vprops,
      vchrs, value, chrs = 0,
      typeconvert = function(x) {
        return x;
      };

    if (!offset) {
      offset = 0;
    }
    dtype = (data.slice(offset, offset + 1))
      .toLowerCase();

    dataoffset = offset + 2;

    switch (dtype) {
      case 'i':
        typeconvert = function(x) {
          return parseInt(x, 10);
        };
        readData = read_until(data, dataoffset, ';');
        chrs = readData[0];
        readdata = readData[1];
        dataoffset += chrs + 1;
        break;
      case 'b':
        typeconvert = function(x) {
          return parseInt(x, 10) !== 0;
        };
        readData = read_until(data, dataoffset, ';');
        chrs = readData[0];
        readdata = readData[1];
        dataoffset += chrs + 1;
        break;
      case 'd':
        typeconvert = function(x) {
          return parseFloat(x);
        };
        readData = read_until(data, dataoffset, ';');
        chrs = readData[0];
        readdata = readData[1];
        dataoffset += chrs + 1;
        break;
      case 'n':
        readdata = null;
        break;
      case 's':
        ccount = read_until(data, dataoffset, ':');
        chrs = ccount[0];
        stringlength = ccount[1];
        dataoffset += chrs + 2;

        readData = read_chrs(data, dataoffset + 1, parseInt(stringlength, 10));
        chrs = readData[0];
        readdata = readData[1];
        dataoffset += chrs + 2;
        if (chrs != parseInt(stringlength, 10) && chrs != readdata.length) {
          error('SyntaxError', 'String length mismatch');
        }
        break;
      case 'a':
        readdata = {};

        keyandchrs = read_until(data, dataoffset, ':');
        chrs = keyandchrs[0];
        keys = keyandchrs[1];
        dataoffset += chrs + 2;

        length = parseInt(keys, 10);
        contig = true;

        for (i = 0; i < length; i++) {
          kprops = _unserialize(data, dataoffset);
          kchrs = kprops[1];
          key = kprops[2];
          dataoffset += kchrs;

          vprops = _unserialize(data, dataoffset);
          vchrs = vprops[1];
          value = vprops[2];
          dataoffset += vchrs;

          if (key !== i)
            contig = false;

          readdata[key] = value;
        }

        if (contig) {
          array = new Array(length);
          for (i = 0; i < length; i++)
            array[i] = readdata[i];
          readdata = array;
        }

        dataoffset += 1;
        break;
      default:
        error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
        break;
    }
    return [dtype, dataoffset - offset, typeconvert(readdata)];
  };

  return _unserialize((data + ''), 0)[2];
}

</script>

<script type="text/javascript">
    jQuery(document).ready(function(){ 
      
      var locations = [
      <?php $c=1;foreach($locations as $loc){?>
      ["<?php echo $loc['city']; ?>", <?php echo $loc['lag']; ?>, <?php echo $loc['long']; ?>, <?php echo $c; ?>],
      <?php $c++;} ?>
      ];
      getMap(locations);
    
    });
    function getMap(locations){
      var locations = locations;

    var map = new google.maps.Map(document.getElementById('map_2385853'), {
      zoom: 7,
      minZoom: 1,
      center: new google.maps.LatLng(37.389092, -5.984459),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

    }
  </script>
