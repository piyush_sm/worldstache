<style>
      #map-canvas {
        width: 365px;
        height: 365px;
      }
    </style>
    <script>
      function initialize() {
        var geocoder= new google.maps.Geocoder();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI:true
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var address = "<?php echo $event_detail['Event']['event_city'];?>";
        //var address = "Phase 3B2, Mohali, India";
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location

        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
      } 

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<div class="event-main">
                <div class="sld-main sld-detail">
                    <ul class="detail-slider">
                        <li><?php echo $this->Html->image($event_detail['Event']['event_image']) ?> </li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_flyer']) ?></li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_cover_image']) ?></li>
                    </ul>
                    <div class="like-detail">
                        <ul>
                            <?php $tags = explode(' ',$event_detail['Event']['event_tags']);
                                $num = count($tags);
                                for($i = 0; $i <= $num-1; $i++){
                            ?>
                            <li><a href="javascript:void(0)"><i class="fa fa-heart heart"></i><i class="fa fa-minus-circle minus"></i><?php echo $tags[$i]?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <h3>
                        <?php $id = base64_encode($event_detail['Event']['id']);?>
                        <?php echo $event_detail['Event']['event_title'];?>
                    </h3>
                    <ul class="venue">
                        <?php $test = new DateTime($event_detail['Event']['event_start_date']); ?>
                        <li><i class="fa fa-calendar"></i><?php echo date_format($test, 'l, F d, Y');?> </li>
                        <li><i class="fa fa-clock-o"></i><?php echo $event_detail['Event']['event_start_hour']?>:<?php echo $event_detail['Event']['event_start_min']?><?php echo $event_detail['Event']['event_start_ap']?></li>
                        <li><a href="#"><i class="fa fa-map-marker"></i>Couture</a>, <?php echo $event_detail['Event']['event_city']?></li>
                    </ul>
                    <p>
                        <?php echo $event_detail['Event']['event_description'];?>
                    </p>
                        <?php if($event_detail['Event']['allow_comments'] == 1 ) { ?>
                        <?php if($user_id != "" && $user_id != null) { ?>
                            <div class="put-cmt">
                                <span><?php echo $this->Html->image($user_data,array('width'=>'100','height'=>'100'));?></span>
                                <form action="post-comment?eid=<?php echo $id;?>" method="post">
                                    <textarea name="data[Comment][comment]" class="form-control" placeholder="Comment here" readonly="readonly"></textarea>
                                    <input type="hidden" value="event" name="data[Comment][comment_type]">
                                    <input type="submit" value="Comment" disabled="disabled">
                                </form>
                            </div>
                            <?php } ?>
                        <?php } ?>
                        <?php if(!empty($all_comments) || $all_comments != null){ ?>
                        <h4>Reviews</h4>
                        <?php foreach($all_comments as $comment) { ?>
                            <div class="review-main">
                           <?php echo $this->Html->image($comment['User']['profile_image'],array('width'=>'50','height'=>'50'));?> <span><?php echo $comment['Comment']['comment'];?></span>
                            </div>
                           <?php } }?>
                        </div>
    
                <div class="map-detail">
                    <div id="map-canvas"></div>
                    <div class="lmtd-time">
                        <h3>LIMITED TIME ONLY!</h3>
                        <?php
                                $date1 = new DateTime(date('m/d/Y h:i:s'));
                                $date2 = new DateTime($event_detail['Event']['event_end_date'].' '.$event_detail['Event']['event_end_hour'].':'.$event_detail['Event']['event_end_min'].':00');
                                $diff=date_diff($date1,$date2);
                                //pr($diff); die();
                                ?>
                        <span><i class="fa fa-clock-o"></i><?php echo $diff->d;?> days  <?php echo $diff->format('%h:%i:%s');?></span>
                    </div>
                    <!--<div class="lmtd-time qunt">
                        <h3>LIMITED QUANTITY AVAILABLE</h3>
                        <span>Over 1,000 Bought.</span> <span class="btn css_blink">Only 5 Left</span>
                    </div>-->
                </div>
                <div class="more-events">
                    <h3>More Upcoming Events...</h3>
                    <div class="slider1">
                        <?php foreach($upcoming_events as $events) { ?>
                        <?php $id = base64_encode($events['Event']['id'])?>
                        <div class="slide">
                        <?php echo $this->Html->link(
                            $this->Html->image($events['Event']['event_image'], array("alt" => "event_img")),"../events/event_detail?eid=".$id,array('escape' => false, 'target'=>'_blank'));?>
                        <?php echo $events['Event']['event_title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
