<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<?php echo $this->Html->css('tag/bootstrap-tagsinput'); ?>
<?php echo $this->Html->script('tag/bootstrap-tagsinput.js'); ?>
<?php echo $this->Html->script('tag/bootstrap-tagsinput.min.js'); ?>
<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Edit Event</a></li>
                        <li role="presentation"><a aria-controls="pending" id="pending-tab" href="../events/add_event_p">Back to Events</a></li>
                       
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-expanded="true" aria-controls="event" data-toggle="tab" role="tab" id="event-tab" href="#event"> Edit </a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Images</a></li>
                            <li role="presentation"><a aria-controls="advanced" data-toggle="tab" id="advanced-tab" role="tab" href="#advanced">Advanced</a></li>
                            <li role="presentation"><a aria-controls="guest" data-toggle="tab" id="guest-tab" role="tab" href="#guest">Guest List</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="event-tab" id="event" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event crtf">
                                <?php echo $this->Form->Create('',array('id'=>'edit_event','enctype'=>'multipart/form-data')) ?>
                                    <!--<h2 class="dash-header">Add Event </h2>-->
                                    <div class="edit-field">
                                        <label>Title</label>
                                       <?php echo $this->Form->input('event_title', array('id'=>'event_title','placeholder'=>'Event Title','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_title'])) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Category</label>
                                       <?php echo $this->Form->input('cat_id', array('options'=> $list_categories,'id'=>'cat_id','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'default'=> $event['Event']['cat_id'])) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                        <label>City</label>
                                        
                                        <?php echo $this->Form->input('event_city', array('id'=>'event_city','class'=>'form-control add-title', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_city'])) ?>
                                    </div>

                                    <div class="edit-field crt-vn">
                                        <label>Venue
                                            <a title="" data-toggle="tooltip" href="#" data-original-title="Start typing the venue you want after having created it Or Select it from the dropdown menu" class="addtooltip"><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        
                                        <?php echo $this->Form->input('event_venue', array('options'=> $list_places, 'id'=>'event_venue','class'=>'form-control add-title', 'label'=>false , 'empty'=>'Select Place','required'=>'required', 'div'=>false,'default'=> $event['Event']['event_venue'])) ?>
                                    </div>
                                    <div class="edit-field crt-vn">
                                        <label>Artist
                                            <a title="" data-toggle="tooltip" href="#" data-original-title="Start typing the artist you want after having created it Or Select it from the dropdown menu" class="addtooltip"><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->input('event_artist', array('options'=> $list_artists, 'id'=>'event_artist', 'class'=>'form-control add-title', 'label'=>false , 'required'=>'required', 'div'=>false,'default'=> $event['Event']['event_artist'])) ?>
                                    </div>
                                    
                                    <div class="edit-field ag-lm">
                                        <label>Age Limit:</label>
                                        <?php $options = array('0'=>'Yes', '1'=>'No'); ?>
                                        <?php echo $this->Form->radio('event_age_check',$options, array('id'=>'age_limit', 'class'=>'form-control age_limit', 'legend'=>false,'value'=> $event['Event']['event_age_check'])); ?>
                                    </div>
                                    <div class="edit-field age-lmt">
                                            <label>Starting Age</label>
                                            <?php $selectstartage = array('1'=>'18Yr','2'=>'20Yr','3'=>'22Yr','4'=>'24Yr') ; ?>
                                            <?php echo $this->Form->input('event_start_age', array('options'=> $selectstartage, 'id'=>'event_start_age', 'class'=>'form-control event_age', 'required'=>'required', 'div'=> false, 'label'=> false,'value'=> $event['Event']['event_start_age'])) ?>
                                    </div>
                                    <div class="edit-field age-lmt">
                                            <label>Ending Age</label>
                                            <?php $selectendage = array('1'=>'30Yr','2'=>'32Yr','3'=>'34Yr','4'=>'36Yr') ; ?>
                                            <?php echo $this->Form->input('event_end_age', array('options'=> $selectendage, 'id'=>'event_end_age', 'class'=>'form-control event_age', 'required'=>'required','div'=> false, 'label'=> false,'value'=> $event['Event']['event_end_age'])) ?>   
                                            </div>
                                    <div class="edit-field">
                                        <label>
                                            Select Date and Time:
                                        </label>
                                        <div class="event-date">
                                            <label>
                                                Start Date:
                                            </label>
                                            <ul class="select-date">
                                                <?php echo $this->Form->input('event_start_date', array('id'=>'dpd1','placeholder'=>'mm/dd/yyyy','class'=>'span6 form-control', 'label'=>false , 'required'=>'required','value'=> $event['Event']['event_start_date'])) ?>  
                                                
                                                <?php $selecthour = array('01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12') ; ?>
                                                
                                                <?php echo $this->Form->input('event_start_hour', array('options'=> $selecthour, 'id'=>'event_start_hour','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_start_hour'])) ?>

                                                <?php $selectmin = array('00'=>'00','05'=>'05','10'=>'10','15'=>'15','20'=>'20','25'=>'25','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'55') ; ?>
                                                <?php echo $this->Form->input('event_start_min', array('options'=> $selectmin, 'id'=>'event_start_min','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_start_min'])) ?>
                                                
                                                <?php $selectap = array('AM'=>'AM','PM'=>'PM') ; ?>
                                                <?php echo $this->Form->input('event_start_ap', array('options'=> $selectap, 'id'=>'event_start_ap','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_start_ap'])) ?>
                                            </ul>
                                                <label>
                                                    Start Date:
                                                </label>
                                            <ul class="select-date">
                                                
                                                <?php echo $this->Form->input('event_end_date', array('id'=>'dpd2','placeholder'=>'mm/dd/yyyy','class'=>'span6 form-control', 'label'=>false , 'required'=>'required','value'=> $event['Event']['event_end_date'])) ?>
                                                
                                                <?php $selecthour = array('01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12') ; ?>                                                
                                                <?php echo $this->Form->input('event_end_hour', array('options'=> $selecthour, 'id'=>'event_end_hour','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_end_hour'])) ?>

                                                <?php $selectmin = array('00'=>'00','05'=>'05','10'=>'10','15'=>'15','20'=>'20','25'=>'25','30'=>'30','35'=>'35','40'=>'40','45'=>'45','50'=>'55') ; ?>
                                                <?php echo $this->Form->input('event_end_min', array('options'=> $selectmin, 'id'=>'event_end_min','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_end_min'])) ?>
                                                
                                                <?php $selectap = array('AM'=>'AM','PM'=>'PM') ; ?>
                                                <?php echo $this->Form->input('event_end_ap', array('options'=> $selectap, 'id'=>'event_end_ap','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_end_ap'])) ?>
                                            
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="edit-field">
                                        <label> 
                                            Recurring Type:
                                        </label>
                                        <div class="recurring">
                                            <label>Weekly</label><input type="radio" name="data[Event][recurring_type]" value="1" class="form-control">
                                            <select id="select_week" name="data[Event][weekly_day]" class="form-control">
                                                    <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                            </select>
                                            <div><label>Monthly</label><input  type="radio" name="data[Event][recurring_type]" value="2" class="form-control"> 
                                                <select id="select_week" name="data[Event][monthly_number]" class="form-control">
                                                        <option value="1">First</option>
                                                        <option value="2">Second</option>
                                                        <option value="3">Third</option>
                                                        <option value="4">Fourth</option>
                                                </select>
                                            <select id="select_week" name="data[Event][monthly_day]" class="form-control">
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                            </select></div>
                                            <label>Not Recurring</label><input type="radio" name="data[Event][recurring_type]" value="3" class="form-control">
                                                
                                            
                                        </div>
<!--                                        <div class="recurring">
                                            <label>Monthly</label><input  type="radio" name="data[Event][recurring_type]" value="2" class="form-control"> 
                                                <select id="select_week" name="data[Event][monthly_number]" class="form-control">
                                                        <option value="1">First</option>
                                                        <option value="2">Second</option>
                                                        <option value="3">Third</option>
                                                        <option value="4">Fourth</option>
                                                </select>
                                                <select id="select_week" name="data[Event][monthly_day]" class="form-control">
                                                        <option value="Monday">Monday</option>
                                                        <option value="Tuesday">Tuesday</option>
                                                        <option value="Wednesday">Wednesday</option>
                                                        <option value="Thursday">Thursday</option>
                                                        <option value="Friday">Friday</option>
                                                        <option value="Saturday">Saturday</option>
                                                        <option value="Sunday">Sunday</option>
                                                </select>
                                            </div>-->
<!--                                            <div class="recurring">
                                                <input type="radio" name="data[Event][recurring_type]" value="3" class="form-control"><label>Not Recurring</label>
                                            </div>-->
                                        </div>
                                    <div class="edit-field">
                                        <label>Seasonal
                                            <a class="addtooltip" data-original-title="Events occuring on special time of the year" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php $selectseason = array('4th of July'=>'4th of July', 'All-Star Game'=>'All-Star Game', 'BPM Festival'=>'BPM Festival', 'Canada Day'=>'Canada Day', 'Cinco De Mayo'=>'Cinco De Mayo', 'Halloween'=>'Halloween', 'Independence Day'=>'Independence Day', 'Labour Day'=>'Labour Day', 'Memorial Day'=>'Memorial Day', 'Miami Music Week'=>'Miami Music Week', 'New Music West'=>'New Music West', 'New Years Eve'=>'New Years Eve', 'Spring Break'=>'Spring Break', 'St. Pattys Day'=>'St. Pattys Day', 'Stampede'=>'Stampede', 'Superbowl'=>'Superbowl', 'Thanksgiving'=>'Thanksgiving','Valentines day'=>'Valentines day') ; ?>
                                        <?php echo $this->Form->input('event_seasonal', array('options'=> $selectseason, 'id'=>'event_seasonal','class'=>'form-control chzn-select', 'label'=>false , 'required'=>'required', 'div'=>false, 'value'=> $event['Event']['event_seasonal'])) ?>
                                    </div>
                                    
                                    <div class="edit-field crt-vn">
                                        <label>Tags
                                            <a class="addtooltip" data-original-title="6 Tags  maximum" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->input('event_tags', array('id'=>'event_tags','class'=>'form-control','data-role'=>'tagsinput', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_tags'])) ?>
                                    </div>
                                    <div class="edit-field crt-vn">
                                        <label>Summary
                                            <a class="addtooltip" data-original-title="Please input upto 350 characters of summary text for this event. Summary Will be displayed on event listing pages. Summary text should be different from main description text for search engine optimization benefits" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->textarea('event_summary', array('id'=>'event_summary','class'=>'form-control custom-control','required'=>'required', 'div'=>false,'value'=> $event['Event']['event_summary'])) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Main Description</label>
                                        <?php echo $this->Form->textarea('event_description', array('id'=>'event_description','class'=>'form-control ckeditor','required'=>'required', 'div'=>false,'value'=> $event['Event']['event_description'])) ?>
                                    </div>
                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Next</a></li>
                            </ul>
                        </div>
                        <div aria-labelledby="images-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="add-new-event adddish">
                                    <?php if(empty($event['Event']['event_image'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_img"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($event['Event']['event_image'], array("width"=>"120px","height"=>"100px","alt"=>"event_img"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label>Event Image</label>
                                        <?php echo $this->Form->input('event_image', array('id'=>'event_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php if(empty($event['Event']['event_flyer'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_flyer"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($event['Event']['event_flyer'], array("width"=>"120px","height"=>"100px","alt"=>"event_flyer"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label>Event Flyer</label>
                                        <?php echo $this->Form->input('event_flyer', array('id'=>'event_flyer', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php if(empty($event['Event']['event_cover_image'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_cover_image"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($event['Event']['event_cover_image'], array("width"=>"120px","height"=>"100px","alt"=>"event_cover_image"));?>
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label>Cover Image</label>
                                        <?php echo $this->Form->input('event_cover_image', array('id'=>'event_cover_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                            <li role="presentation"><a aria-controls="advanced" data-toggle="tab" id="advanced-tab" role="tab" href="#advanced">Next</a></li>
                            </ul>
                        </div>
                        <div aria-labelledby="advanced-tab" id="advanced" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event advnce">
                                <div class="edit-field">
                                    <h5 class="drscode">Dress Code</h5>
                                    <label>Dress Code Tags</label>
                                    <?php echo $this->Form->input('event_dress_code', array('id'=>'event_dress_code','placeholder'=>'Dress Code','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_dress_code'])) ?>
                                </div>
                                <div class="edit-field">
                                    <label>Dress Code Descriptions</label>
                                    <?php echo $this->Form->input('event_dress_code_description', array('id'=>'event_dress_code_description','placeholder'=>'Dress Code Description','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['event_dress_code_description'])) ?>
                                </div>
                                <div class="edit-field">
                                    <h5 class="drscode">Music Style</h5>
                                    <label>Music Style Description</label>
                                    <?php echo $this->Form->input('music_style_description', array('id'=>'music_style_description','placeholder'=>'Music Style Description','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['music_style_description'])) ?>
                                </div>
                                <div class="edit-field ticket-det" id="p_scents">
                            <h5 class="drscode">Ticket Detail</h5>
                            <?php  
                                    $i=1;
                            foreach($event_ticket as $key=>$ticket){						
								
								 ?>
                            <div class="wdth100 pull-left">
                           <ul class="select-date">
								<li><?php echo 'Sno'.'   ' .$i;?></li>
                                <li><label for="event_ticket">Event Ticket</label><input name="data[Event][event_ticket][]" id="event_ticket_<?php echo $i; ?>" placeholder="Event Ticket" class="form-control" required="required" maxlength="255" type="text" value="<?php echo $ticket; ?>"/></li>
                                <li><label for="event_ticket_price">Event Ticket Price</label><input name="data[Event][event_ticket_price][]" id="event_ticket_price_<?php echo $i; ?>" placeholder="Event Price" class="form-control" required="required" type="text" onkeypress="return isNumberKey(event)" value="<?php echo $event_price[$key]; ?>"/></li>
                                <li><label for="event_ticket_seats">Event Ticket Seats</label><input name="data[Event][event_ticket_seats][]" id="event_ticket_seats_<?php echo $i; ?>" placeholder="No of Seats" class="form-control" required="required" type="text" onkeypress="return isNumberKey(event)" value="<?php echo $event_seats[$key]; ?>"/></li>
                            </ul>
                            <ul class="select-date">
                            <label>Event Ticket Description</label>
                            <textarea name="data[Event][event_ticket_description][]" id="event_ticket_description_<?php echo $i; ?>" rows="5" cols="4" class="span6 option"  ><?php // echo $event_seats[$key]; ?></textarea>
                            </ul>
                        </div><?php $i++;} ?>
                            </div>
                            <div class="btn-group submit pull-left">
                                <!--<input type="submit" class="btn btn-warning" value="Add More Ticket Type" >-->
                            <button id="addScnt" class="btn btn-warning mgtop10" type="button">Add More Ticket Type</button>
                            </div>
                                <div class="edit-field">
                                    <h5 class="drscode">Other</h5>
                                </div>
                                
                                <div class="edit-field">
                                    <label >Website url</label>
                                    <?php echo $this->Form->input('website_url', array('id'=>'website_url','placeholder'=>'Website Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['website_url'])) ?>
                                </div>
                                <div class="edit-field">
                                    <label >Comments</label>
                                    <input type="checkbox" class="cmntcheck">
                                    <h6 class="cmttext">allow visitor to post comments about your event.</h6>
                                </div>
                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                            <li role="presentation"><a aria-controls="guest" data-toggle="tab" id="guest-tab" role="tab" href="#guest">Next</a></li>
                            </ul> 
                        </div>
                        <div aria-labelledby="guest-tab" id="guest" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="edit-field">
                                    <label>Show guestlist</label>
                                    <input type="checkbox" class="cmntcheck">
                                    <h6 class="cmttext">allow guestlists for this event.</h6>
                                </div>
                                <div class="edit-field edt-evt">
                                    <label>Notification
                                        <a class="addtooltip guest" data-original-title="Use backspace to delete the venue" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                    </label>
                                    <?php echo $this->Form->input('notification_email', array('id'=>'notification_email','placeholder'=>'Notification Email','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false,'value'=> $event['Event']['notification_email'])) ?>
                                </div>
                                <div class="edit-field gstpls">
                                    <label>Guest "Plus" Max
                                        <a class="addtooltip" data-original-title="What is the maximun number of additional guests each applicant can request" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                    </label>
                                    <?php $max_num = array('1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5',); ?>
                                    <?php echo $this->Form->input('plus_max', array('options'=>$max_num,'id'=>'plus_max','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false, 'default'=>$event['Event']['plus_max'])) ?>
                                </div>
                                <div class="edit-field edt-evt">
                                    <label>Auto approve
                                        <a class="addtooltip guest" data-original-title="Use backspace to delete the venue" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                    </label>
                                    <input type="checkbox" class="cmntcheck">
                                    <h6 class="cmttext">Automatically approve guest list submissions.</h6>
                                </div>
                                <div class="edit-field">
                                    <label>Line Access
                                        <a class="addtooltip" data-original-title="Line access benefits for people on guestlist" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                    </label>
                                    <select class="form-control add-title" >
                                        <option value="VIP line Access">VIP line Access</option>
                                        <option value="Regular Line Access">Regular Line Access</option>
                                    </select>
                                </div>
                               <div class="edit-field">
                                    <label>Cover Charge</label>
                                    <select class="form-control add-title" >
                                        <option value="Complimentary cover charge">Complimentary cover charge</option>
                                        <option value="Discounted Cover Charge">Discounted Cover Charge</option>
                                        <option value="Regular Cover Charge">Regular Cover Charge</option>
                                    </select>
                                </div>
                                <!--<div class="edit-field radgst">
                                    <label>
                                        Cut off time:
                                    </label>
                                    <ul class="add-radio">
                                        <li class="eve-rad"><input type="radio" value="option1" id="inlineRadio1" name=""['Event']['cut_off']"">All night</li>
                                        <li class="eve-rad"><input type="radio" value="option2" id="inlineRadio2" name=""['Event']['cut_off']"">Before</li>

                                    </ul>
                                </div>-->
                                <div class="edit-field">
                                    <label>Benifits</label>
                                    <?php echo $this->Form->textarea('benefits', array('id'=>'benefits','class'=>'form-control custom-control','required'=>'required', 'div'=>false,'value'=> $event['Event']['benefits'])) ?>
                                </div>
                                <div class="edit-field">
                                    <label>Additional Benifits</label>
                                    <?php echo $this->Form->textarea('additional_benefits', array('id'=>'additional_benefits','class'=>'form-control custom-control','required'=>'required', 'div'=>false,'value'=> $event['Event']['additional_benefits'])) ?>
                                </div>
                                <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Update Event">
                          
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>
            </div>
         <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/javascript">
        jQuery(document).ready(function () {
        jQuery(".age_limit").click(function () 
        {
        $id = jQuery(this).val();
             
            if($id == 0)
            {
                jQuery('.age-lmt').css('display','block'); 
                
            }
             if($id == 1)
             {
                jQuery('.age-lmt').css('display','none'); 
             }   
         });
         
        });
   
  </script>
