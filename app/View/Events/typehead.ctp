<!--<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Example of Twitter Typeahead</title>
<h3>Example of Twitter Typeahead</h3>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php //echo $this->Html->script('typeahead.min.js');?>
<script type="text/javascript">
$(document).ready(function(){
    $('input.typeahead').typeahead({
        local: ['Audi', 'BMW', 'Bugatti', 'Ferrari', 'Ford', 'Lamborghini', 'Mercedes Benz', 'Porsche', 'Rolls-Royce', 'Volkswagen']
    });
});  
</script>
<style type="text/css">
.bs-example{
    font-family: sans-serif;
    position: relative;
    margin: 100px;
}
.typeahead, .tt-query, .tt-hint {
    border: 2px solid #CCCCCC;
    border-radius: 8px;
    font-size: 24px;
    height: 30px;
    line-height: 30px;
    outline: medium none;
    padding: 8px 12px;
    width: 396px;
}
.typeahead {
    background-color: #FFFFFF;
}
.typeahead:focus {
    border: 2px solid #0097CF;
}
.tt-query {
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.tt-hint {
    color: #999999;
}
.tt-dropdown-menu {
    background-color: #FFFFFF;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 8px;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    margin-top: 12px;
    padding: 8px 0;
    width: 422px;
}
.tt-suggestion {
    font-size: 24px;
    line-height: 24px;
    padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
    background-color: #0097CF;
    color: #FFFFFF;
}
.tt-suggestion p {
    margin: 0;
}
</style>
</head>
<body>
    <div class="bs-example">
        <input type="text" class="typeahead tt-query" autocomplete="off" spellcheck="false">
    </div>
</body>
</html>-->
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=100" />
<title>Type Ahead</title>
<?php echo $this->Html->css('chosen'); ?>
</head>
<body>
<section class="main-cont">
  <div class="left-side-add"> </div>
  <div class="center-cont">
    <div class="heading">Type Ahead</div>
    <table class="style1">
      <tr>
        <td class="style2"> Select season </td>
        <td><select class="span6 chzn-select">
            <option>Alpha</option>
            <option>Asha2</option>
            <option>Adpha3</option>
            <option>Aepha4</option>
          </select></td>
      </tr>
    </table>
  </div>
  <div class="right-side-add"></div>
  <div class="clear"></div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<?php echo $this->Html->script('chosen.jquery.min.js');?>
<?php echo $this->Html->script('form-component.js');?>
</body>
</html>