<style>
      #map-canvas {
        width: 365px;
        height: 365px;
      }
    </style>
    <script>
      function initialize() {
        var geocoder= new google.maps.Geocoder();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI:true
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var address = "<?php echo $event_detail['Event']['event_city'];?>";
        //var address = "Phase 3B2, Mohali, India";
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location

        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
      } 

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<div class="event-main">
                <div class="sld-main sld-detail">
                    <ul class="detail-slider">
                        <li><?php echo $this->Html->image($event_detail['Event']['event_image']) ?> </li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_flyer']) ?></li>
                        <li><?php echo $this->Html->image($event_detail['Event']['event_cover_image']) ?></li>
                    </ul>
                    <div class="like-detail">
                        <ul>
                            <?php $tags = explode(' ',$event_detail['Event']['event_tags']);
                                $num = count($tags);
                                for($i = 0; $i <= $num-1; $i++){
                            ?>
                            <li><a href="javascript:void(0)"><i class="fa fa-heart heart"></i><i class="fa fa-minus-circle minus"></i><?php echo $tags[$i]?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <h3>
                        <?php $id = base64_encode($event_detail['Event']['id']);?>
                        <?php echo $event_detail['Event']['event_title'];?>
                    </h3>
                    <ul class="venue">
                        <?php $test = new DateTime($event_detail['Event']['event_start_date']); ?>
                        <li><i class="fa fa-calendar"></i><?php echo date_format($test, 'l, F d, Y');?> </li>
                        <li><i class="fa fa-clock-o"></i><?php echo $event_detail['Event']['event_start_hour']?>:<?php echo $event_detail['Event']['event_start_min']?><?php echo $event_detail['Event']['event_start_ap']?></li>
                        <li><a href="#"><i class="fa fa-map-marker"></i>Couture</a>, <?php echo $event_detail['Event']['event_city']?>&nbsp;<!--|<a href="#">show on map</a>--></li>
                    </ul>
                    <p>
                        <?php echo $event_detail['Event']['event_description'];?>
                    </p>
                    <ul class="share-detail">
                        <li><div class="g-plus" data-action="share" data-annotation="none"></div></li>
                        <li onclick="share_event();"><a href="javascript:void(0)"><i class="fa fa-facebook-square"></i></a></li>
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-twitter-square"></i></a></li>-->
                        <!--<li><a href="javascript:void(0)"><i class="fa fa-pinterest-square"></i></a></li>-->
                        <li><a class="twitter-share-button" href="https://twitter.com/share" data-count="none"><i class="fa fa-twitter-square"></i></a></li>
                        <li><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"  data-pin-shape="round" data-pin-height="32"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_round_red_32.png" /><i class="fa fa-pinterest-square"></i></a></li>
                        <li><div class="fb-like" data-href="https://www.facebook.com/Sevilla.WorldStache" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li>
                    </ul>
                </div>
                <div class="map-detail">
                    <div id="map-canvas"></div>
                    <div class="lmtd-time">
                        <h3>LIMITED TIME ONLY!</h3>
                        <?php
                                $date1 = new DateTime(date('m/d/Y h:i:s'));
                                $date2 = new DateTime($event_detail['Event']['event_end_date'].' '.$event_detail['Event']['event_end_hour'].':'.$event_detail['Event']['event_end_min'].':00');
                                $diff=date_diff($date1,$date2);
                                ?>
                        <span><i class="fa fa-clock-o"></i><?php echo $diff->d;?> days  <?php echo $diff->format('%h:%i:%s');?></span>
                    </div>
                    <div class="lmtd-time qunt">
                        <h3>LIMITED QUANTITY AVAILABLE</h3>
                        <span>Over 1,000 Bought.</span> <span class="btn css_blink">Only 5 Left</span>
                    </div>
                    <?php if($user_id != "" && $user_id != null) { ?>
                    <form action="http://dev.staplelogic.in/worldstashdev/events/event_detail?eid=<?php echo $id;?>" method="post">
                    <div class="lmtd-time qunt slcttyp">
                        <?php if(!empty($event_detail['Event']['plus_max'])) {
                        $ticket = $event_detail['Event']['plus_max'];
                              $tickets = $ticket + 1;
                          } else {
                        $tickets =  $event_detail['Event']['plus_max'];
                          }
                        ?>
                        <select name = "total_tickets" id="tickets" empty="Select ticket">
                            <option>Select Tickets</option>
                            <?php for($i=1; $i <= $tickets; $i++) { ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                            <?php } ?>
                        </select>
                        <?php $types = unserialize($event_detail['Event']['event_ticket']);?>
                        <select name = "total_type" id="tickets_type" empty="Select ticket type" onchange="show_price();">
                            <option>Select Ticket Type</option>
                           <?php foreach($types as $type) { ?>
                            <option value="<?php echo $type;?>"><?php echo $type;?></option>
                            <?php } ?>
                        </select>
                        <h5>Ticket price:</h5>
                        <span id="new_price"></span>
                                <input type="hidden" name="event_id" value="<?php echo $event_detail['Event']['id'];?>">
                                <input type="hidden" name="cmd" value="_xclick">
                                <input type="hidden" name="amount" value="<?php echo $event_detail['Event']['event_price'];?>">
                                <input type="hidden" name="item_name" value="<?php echo $event_detail['Event']['event_title'];?>">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="return" value="http://dev.staplelogic.in/worldstashdev/events/success">
                                <input type="submit" name="Pay Now" value="Pay" id="pay_now">
                        </div>
                    </form>
                    <?php } else { ?>
                    <p> You need to sign in to buy tickets </p>
                    <?php } ?>
                </div>
                <div class="more-events">
                    <h3>More Upcoming Events...</h3>
                    <?php //pr($upcoming_events);?>
                    <div class="slider1">
                        <?php foreach($upcoming_events as $events) { ?>
                        <?php $id = base64_encode($events['Event']['id'])?>
                        <div class="slide">
                        <?php echo $this->Html->link(
                            $this->Html->image($events['Event']['event_image'], array("alt" => "event_img")),"../events/event_detail?eid=".$id,array('escape' => false, 'target'=>'_blank'));?>
                        <?php echo $events['Event']['event_title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <script>
        function share_event(){
            var id = "<?php echo $id; ?>";
            var path = "<?php echo $events['Event']['event_image']; ?>"
            var image = "http://staplelogic.com/worldstashdev"+path;
            FB.ui({
              method: 'feed',
              link: '<?php echo Router::url(null, true ); ?>?eid='+id,
              caption: "<?php echo $events['Event']['event_title']; ?>",
              picture: image,
              description: "<?php echo $event_detail['Event']['event_description']; ?>",
            }, function(response){});
    } 
        function show_price(){
            var type = jQuery("#tickets_type").val();
            var num = jQuery("#tickets").val();
            
            if(num == "" || num == null){
                return false
            } else {
            //var price = "<?php echo $event_detail['Event']['event_ticket_price'];?>";
            var price = 20;
            var new_price = parseInt(num) * parseInt(price);
            //alert(new_price);
            jQuery("#new_price").html('');
            jQuery("#new_price").append('<span id="new_price">$'+new_price+'</span>');
            }
        }
        </script>
        <script>
        window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
        </script>
        <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>