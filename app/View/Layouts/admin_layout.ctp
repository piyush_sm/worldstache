<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <link rel="icon" type="image/png" href="images/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <?php echo $this->Html->css('dashboard/style'); ?>
        <?php echo $this->Html->css('dashboard/bootstrap'); ?>
        <?php echo $this->Html->css('dashboard/jsDatePick_ltr.min'); ?>
        <?php echo $this->Html->css('dashboard/datepicker'); ?>
        <?php //echo $this->Html->css('chosen'); ?>
        <?php echo $this->Html->script('ckeditor/ckeditor.js'); ?>

    <?php if($this->params['action'] != "add_place") { ?>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <script>
    function initialize()
        {
            var input = document.getElementById('city_id');
            var autocomplete = new google.maps.places.Autocomplete((input),
                                {
                                    types: ['geocode'],
                                });
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                document.getElementById('event_city_lat').value = place.geometry.location.lat();
                document.getElementById('event_city_long').value = place.geometry.location.lng();
                    if (!place.geometry) {
                      window.alert("Autocomplete's returned place contains no geometry");
                      return;
                    }

            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <?php } ?>
        <title><?php echo $title_for_layout; ?></title>
    </head>
    
    <body>
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="admin-dashboard" class="navbar-brand"><strong>World Stash</strong> Administrator </a>
                    <a href="/worldstashdev" class="navbar-brand"><strong>Home</strong></a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"> <?php echo $_SESSION['Auth']['User']['first_name'];?> <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="admin-dashboard">Dashboard</a></li>
                                <li><a href="edit-profile">Edit Profile</a></li>
                                <li><a href="change-password">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="logout">Logout</a></li>
                            </ul>
                        </li>
                        <li><a href="help">Help</a></li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="container">
<div class="col-sm-3 col-md-3 sidebar">
                <!--<div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="http://staplelogic.com/worldstashdev/users/admin_dashboard#home">Event</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="http://staplelogic.com/worldstashdev/users/admin_dashboard#artist">Artist</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="http://staplelogic.com/worldstashdev/users/admin_dashboard#promoter">Places</a></li>
                        </ul>
                </div>-->
                <div class="pro-detail">
					<ul class = "view-prof edit-prof">
                        <?php if(!empty($_SESSION['Auth']['User']['profile_image'])) { ?>
                        <li><?php echo $this->Html->image($_SESSION['Auth']['User']['profile_image'], array("alt" => "Logo")) ?></li>
                        <?php } else { ?>
                        <li><?php echo $this->Html->image('logo.png', array("alt" => "Logo")) ?></li>
                        <?php } ?>
						<li><?php echo $_SESSION['Auth']['User']['first_name'];?></li>
					</ul>
					
                    <ul id="tab_links" class="view-prof edit-prof">
						<li id="events"><?php echo $this->Html->link('Events',array('controller'=>'events','action'=>'add_event')); ?></li>
						<li id="artists"><?php echo $this->Html->link('Artists',array('controller'=>'artists','action'=>'add_artist')); ?></li>
						<li id="places"><?php echo $this->Html->link('Places',array('controller'=>'places','action'=>'add_place')); ?></li>
                        <li id="blogs"><?php echo $this->Html->link('Blog',array('controller'=>'blogs','action'=>'add_blog')); ?></li>
                        <li id="category"><?php echo $this->Html->link('Category',array('controller'=>'categories','action'=>'add_category')); ?></li>
                        <li id="subcategory"><?php echo $this->Html->link('Sub Category',array('controller'=>'subcategories','action'=>'add_subcategory')); ?></li>
                        <li id="banners"><?php echo $this->Html->link('Manage Banners',array('controller'=>'settings','action'=>'manage_banner')); ?></li>
                        <li id="slider"><?php echo $this->Html->link('Manage Slider',array('controller'=>'settings','action'=>'manage_slider')); ?></li>
                        <li id="footer"><?php echo $this->Html->link('Manage Footer',array('controller'=>'settings','action'=>'manage_footer')); ?></li>
                        <li id="static_pages"><?php echo $this->Html->link('Static Pages',array('controller'=>'settings','action'=>'static_pages')); ?></li>
                        <li id="media"><?php echo $this->Html->link('Social Media',array('controller'=>'settings','action'=>'social_media')); ?></li>
                        <li id="users"><?php echo $this->Html->link('Users',array('controller'=>'users','action'=>'users_list')); ?></li>
                        <li id="professional"><?php echo $this->Html->link('Professional Requests',array('controller'=>'users','action'=>'professional_requests')); ?></li>
                        <li id="addprofessional"><?php echo $this->Html->link('Add Professional',array('controller'=>'users','action'=>'add_professional')); ?></li>
                    </ul>
                    <a href="logout"><button type="button" class="btn btn-warning">Logout</button></a>
                </div>

            </div>

            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>

        </div>

        <footer> 
            <div class="footer-area">
                
            </div>
            <div class="copyright">
                <div class="footer-copy">
                    <p class="left">&copy; 2015 <a href="javascript:void(0)">World Stash</a> All Rights Reserved</p>
                    <p class="right">Stapled by <a target="blank" href="http://staplelogic.com/">StapleLog!c</a></p>
                </div>
            </div>
        </footer>
        <?php if($this->params['action'] == "add_event") { ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
        <?php echo $this->Html->script('dashboard/bootstrap-datepicker.js'); ?>
        <?php echo $this->Html->script('dashboard/bootstrap.min.js'); ?>
        <?php echo $this->Html->script('chosen.jquery.min.js');?>
        <?php echo $this->Html->script('form-component.js');?>
        <?php } else { ?>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <?php echo $this->Html->script('dashboard/bootstrap-datepicker.js'); ?>
        <?php echo $this->Html->script('dashboard/bootstrap.min.js'); ?>
        <?php echo $this->Html->script('chosen.jquery.min.js');?>
        <?php echo $this->Html->script('form-component.js');?>
        <?php } ?>
        
		       <script type="text/javascript">
					$(document).ready(function () {

					$("#tab_links li").click(function(){
							var id = $(this).attr("id");
							$('#' + id).siblings().find(".selec").removeClass("selec");
								
							$('#' + id).addClass("selec");
							localStorage.setItem("selectedolditem", id);
							
							});
							var selectedolditem = localStorage.getItem('selectedolditem');

					if (selectedolditem != null) {
						$('#' + selectedolditem).siblings().find(".selec").removeClass("selec");
						$('#' + selectedolditem).addClass("selec");
					}

                    /*jQuery("#box_links div").click(function(){
                            var id = jQuery(this).attr("id");
                            alert(id); return false; }); */
				});
                
				</script>
        <script type="text/javascript">


            $(function () {
                var nowTemp = new Date();
                var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

                var checkin = $('#dpd1').datepicker({
                    onRender: function (date) {
                        return date.valueOf() < now.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function (ev) {
                    if (ev.date.valueOf() > checkout.date.valueOf()) {
                        var newDate = new Date(ev.date)
                        newDate.setDate(newDate.getDate() + 1);
                        checkout.setValue(newDate);
                    }
                    checkin.hide();
                    $('#dpd2')[0].focus();
                }).data('datepicker');
                var checkout = $('#dpd2').datepicker({
                    onRender: function (date) {
                        return date.valueOf() < checkin.date.valueOf() ? 'disabled' : '';
                    }
                }).on('changeDate', function (ev) {
                    checkout.hide();
                }).data('datepicker');
            });
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
    </body>
</html>
