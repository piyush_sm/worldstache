<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <link rel="icon" type="image/png" href="images/favicon.png" />
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <?php echo $this->Html->css('dashboard/style'); ?>
	<?php echo $this->Html->css('dashboard/bootstrap'); ?>
	<?php echo $this->Html->css('dashboard/jsDatePick_ltr.min'); ?>
	<?php echo $this->Html->css('dashboard/datepicker'); ?>
    <?php echo $this->Html->css('chosen'); ?>
	<script src="//cdn.ckeditor.com/4.4.7/standard/ckeditor.js"></script>
        <title><?php echo $title_for_layout; ?></title>
    </head>
    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/worldstashdev" class="navbar-brand">World Stache</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"> <?php echo $_SESSION['Auth']['User']['first_name'];?> <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><?php echo $this->Html->link('Dashboard',array('controller'=>'users','action'=>'my_profile'));?></li>
                                <li><?php echo $this->Html->link('Edit Profile',array('controller'=>'users','action'=>'my_account'));?></li>
                                <li class="divider"></li>
                                <?php if($user_type == 1) { ?>
                                <li><a href="add-event">My Events</a></li>
                                <?php } ?>
                                <li><a href="my-orders">My Orders</a></li>
                                <li><a href="javascript:void();">Invite Friends</a></li>
                                <?php if($user_type == 1) { ?>
                                <li><a href="guestlist-sold">Guest Lists</a></li>
                                <?php } ?>
                                <?php if($user_type == 2) { ?>
                                <li><a href="professional-request">Become Professional</a></li>
                                <?php } ?>
                                <li><a href="change-password">Change Password</a></li>
                                <li><a href="logout">Logout</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="#"><i class="fa fa-envelope"></i></a></li>
                        <li><a href="#">Settings</a></li>-->
                        <li><a href="help">Help</a></li>
                    </ul>

                </div>
            </div>
        </nav>
        
        <div class="container">
        <?php if($this->params['action'] != "dashboard") { ?>
        <div class="col-sm-3 col-md-3 sidebar">
                <div class="pro-detail">
                    <?php echo $this->Html->image($user_detail['profile_image'], array("alt" => "Logo")) ?>
					<a class="view-profile" href="javascript:void();"><?php echo $_SESSION['Auth']['User']['first_name']; ?></a>
                    <ul id="tab_links" class="view-prof edit-prof">
						<li id="profiles"><?php echo $this->Html->link('Profile',array('controller'=>'users','action'=>'my_profile')); ?></li>
                        <?php if($user_type != 2) { ?>
						<li id="promoters"><?php echo $this->Html->link('Promoter Page',array('controller'=>'promoters','action'=>'add_promoter_p')); ?></li>
                        <li><a href="javascript:void();">Google Analytics</a></li>
                        <li id="events"><?php echo $this->Html->link('Events',array('controller'=>'events','action'=>'add_event_p')); ?></li>
						<li id="artists"><?php echo $this->Html->link('Artists',array('controller'=>'artists','action'=>'add_artist_p')); ?></li>
						<li id="places"><?php echo $this->Html->link('Places',array('controller'=>'places','action'=>'add_place_p')); ?></li>
                        <li id="guestlists"><?php echo $this->Html->link('Guestlist / Sold',array('controller'=>'users','action'=>'guestlist_sold')); ?></li>
                        <li id="payments"><?php echo $this->Html->link('Set Payment Method',array('controller'=>'users','action'=>'set_payment')); ?></li>
                        <?php } ?>
                    </ul>
                    <?php if($user_type == 1) { ?>
                    <a class="view-profile" href="javascript:void();">My Dashboard</a>
                    <ul class="view-prof edit-prof pd-list" style="text-align:left;">
						<li> <span class="dash-heading">Your Promoter Page is</span> <span class="active-btn dash-value">Active</span><div class="clr"></div></li>
						<!--<li> Promoter Page Link: </li>
						<li class="url-bg"> https://www.worldstache.com/piyush </li>-->
						<li> <span class="dash-heading">Number of places :</span> <span class="dash-value">0 </span><div class="clr"></div> </li>
						
						<li> <span class="dash-heading">Number of Events :</span> <span class="dash-value">0 </span><div class="clr"></div> </li>
						<li> <span class="dash-heading">Number of Guestlist :</span> <span class="dash-value"> 0 </span><div class="clr"></div> </li>
                    </ul>
                    <?php } ?>
                    <a href="logout"><button type="button" class="btn btn-warning">Logout</button></a>
                </div>

                <!--<div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#home">Event</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#artist">Artist</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#promoter">Promotor</a></li>
                        <li class="dropdown" role="presentation">
                            <a aria-controls="myTabDrop1-contents" data-toggle="dropdown" class="dropdown-toggle" id="myTabDrop1" href="#">Dropdown <span class="caret"></span></a>
                            <ul id="myTabDrop1-contents" aria-labelledby="myTabDrop1" role="menu" class="dropdown-menu">
                                <li><a aria-controls="dropdown1" data-toggle="tab" id="dropdown1-tab" role="tab" tabindex="-1" href="#dropdown1">@fat</a></li>
                                <li><a aria-controls="dropdown2" data-toggle="tab" id="dropdown2-tab" role="tab" tabindex="-1" href="#dropdown2">@mdo</a></li>
                            </ul>
                        </li>
                    </ul>

                </div> -->

            </div>
        <?php } ?>
            
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>

        </div>

        <footer> 
            <div class="footer-area">
                <div class="footer-mid">
                    <ul class="footer-mid links">
                        <li><a href="/worldstashdev">Home</a></li>
                        <li><a href="about-us">About us</a></li>
                        <li><a href="contact-us">Contact</a></li>
                        <li><a href="#model">Sign up</a></li>
                        <li><a href="privacy">Privacy</a></li>
                    </ul>
                    <ul class="footer-mid links">
                        <li class="first-bold">Printing</li>
                        <li><a href="javascript:void(0)">Timing</a></li>
                        <li><a href="javascript:void(0)">Ticket Printing</a></li>
                        <li><a href="javascript:void(0)">Band</a></li>
                    </ul>
                    <ul class="footer-mid links">
                        <li class="first-bold">Clients</li>
                        <li><a href="javascript:void(0)">Testemonial</a></li>
                        <li><a href="javascript:void(0)">Case Studies</a></li>
                        <li><a href="javascript:void(0)">Improvement</a></li>
                    </ul>
                    <!--<ul class="footer-mid links">
                        <li class="first-bold">Clients</li>
                        <li><a href="javascript:void(0)">Testemonial</a></li>
                        <li><a href="javascript:void(0)">Case Studies</a></li>
                        <li><a href="javascript:void(0)">Improvement</a></li>
                    </ul>-->
                    <ul class="footer-mid links">
                        <li><a href="about-us">About</a></li>
                        <li><a href="team">Team</a></li>
                        <li><a href="javascript:void(0)">Careers</a></li>
                        <li><a href="blog-list">Blog</a></li>
                        <li><a href="javascript:void(0)">Advertising</a></li>
                        <li><a href="javascript:void(0)">Ticket Support</a></li>
                        <li><a href="terms-of-use">Terms of use</a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <div class="footer-copy">
                    <p class="left">&copy; 2015 <a href="javascript:void(0)">World Stache</a> All Rights Reserved</p>
                    <p class="right">Stapled by <a target="blank" href="http://staplelogic.com/">StapleLog!c</a></p>
                </div>
            </div>
        </footer>

            <?php if(($this->params['action'] == "add_event_p") || ($this->params['action'] == "edit_event_p")) { ?>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.2.1/jquery-migrate.min.js"></script>
            <?php echo $this->Html->script('dashboard/bootstrap-datepicker.js'); ?>
            <?php echo $this->Html->script('dashboard/bootstrap.min.js'); ?>
            <?php echo $this->Html->script('chosen.jquery.min.js');?>
            <?php echo $this->Html->script('form-component.js');?>
            <?php } else { ?>
            <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
            <?php echo $this->Html->script('dashboard/bootstrap-datepicker.js'); ?>
            <?php echo $this->Html->script('dashboard/bootstrap.min.js'); ?>
            <?php echo $this->Html->script('chosen.jquery.min.js');?>
            <?php echo $this->Html->script('form-component.js');?>
            <?php } ?>

        <script type="text/javascript">
                    $(document).ready(function () {
                    $("#tab_links li").click(function(){
                            var id = $(this).attr("id");
                            $('#' + id).siblings().find(".selec").removeClass("selec");
                                
                            $('#' + id).addClass("selec");
                            localStorage.setItem("selectedolditem", id);
                            
                            });
                            var selectedolditem = localStorage.getItem('selectedolditem');

                    if (selectedolditem != null) {
                        $('#' + selectedolditem).siblings().find(".selec").removeClass("selec");
                        $('#' + selectedolditem).addClass("selec");
                    }
                });
                </script>
        <script type="text/javascript">
        jQuery(function() {
        jQuery("#datepicker").datepicker({minDate:0});
        });

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>


    </body>
</html>
