<!--
/*/	HTML Document Written by Front-End Developer's Team of StapleLogic (http://www.staplelogic.com)
/*/	In this file you will find all the Front-End layout of site with proper comments.
/*/	This file is copyright to StapleLogic and no reproduction of this file is allowed without the prior written permission from StapleLogic
/*/	Version of file : V1.0
-->
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="format-detection" content="telephone=no" />
        <meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
        <link rel="icon" type="image/png" href="images/favicon.png" />
        
	<?php echo $this->Html->css('style'); ?>
	<?php echo $this->Html->css('jquery.bxslider'); ?>
	<?php echo $this->Html->css('fonts/stylesheet'); ?>
	
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/smoothness/jquery-ui.css">
	<?php echo $this->Html->script('html5.js');?>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js"></script>
	<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>-->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script> 
    <!--<link rel="stylesheet" href="http://resources/demos/style.css">-->
    <script src="//connect.facebook.net/en_US/all.js"></script>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '667189513413476', // App ID
          channelUrl : 'http://dev.staplelogic.in/worldstashdev/',
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true  // parse XFBML
        });
        };
      
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));

    function Login()
        {
     
        FB.login(function(response) {
           if (response.authResponse)
           {	
			   document.cookie="accesstoken="+response.authResponse['accessToken'];
                 saveuserdetail(); // Get User Information.

            } else
            {
             console.log('Authorization failed.');
            }
         },{scope: 'email,user_friends,user_status,user_checkins'});
     
        }


    function getUserInfo() {
        FB.api('/me', function(response) {
    console.log(response);
        });
    }

    function saveuserdetail() {               
                FB.api('/me', function(response) {
                    jQuery.ajax({
                        type: "POST",
                        url: 'http://dev.staplelogic.in/worldstashdev/users/fblogin',
                        data: response,
                        success: function(msg){
            window.location.replace("http://dev.staplelogic.in/worldstashdev/");
                        },
                        
                    });
                });
            }

    </script>
    <?php if($this->params['action'] == "display") {  ?>
	<div id="fb-root"></div>
	<script>
    jQuery(document).ready(function(){
    FB.api(
        "/616598445106528/feed?access_token=667189513413476|pUpI6gw43S-dPH6n0q6VOjH5lIo",
        function (response) {
			console.log(response.error);
          if (response && !response.error) {
			
        jQuery.each(response.data, function(i) {

            if(response.data[i]['picture'] != undefined){
            if(name == null || name == "undefined"){
                name = "";
            }
            var description = response.data[i]['description'];
            var message = response.data[i]['message'];
            if(description == null || description == "undefined" && message != ""){
                description = message.substring(0,100);
            } else {
                description = description.substring(0,100);
            }
            var arrArray = response.data[i]['id'].split('_');
            var cdateArray = response.data[i]['updated_time'].split('T');

            var fb_link = "https://www.facebook.com/Sevilla.WorldStache/posts/"+arrArray['1'];
            var html = jQuery('<div class="item boxes"><a target="_blank" href='+fb_link+'><img src='+response.data[i]['picture']+'><h1>'+name+'</h1></a><span><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></span><div class="date">'+cdateArray['0']+'</div><p>'+description+'</p></div>');
			jQuery('#masonry').append(html);
            }
        }); 
          }
        }
    );
    });
    </script>
    <?php } ?>
    <?php if($this->params['action'] != "search") { ?>
	<script>
	function initialize()
		{
			var input = document.getElementById('where');
			var autocomplete = new google.maps.places.Autocomplete((input),
								{
									types: ['geocode'],
								});
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
					if (!place.geometry) {
					  window.alert("Autocomplete's returned place contains no geometry");
					  return;
					}

			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
    <?php } ?>
    <title><?php echo $title_for_layout; ?></title>
    </head>

    <body>
        <div id="main_container">
            <header>
                <!--<div class="logo">
				<?php echo $this->Html->link(
					$this->Html->image("logo.png", array("alt" => "Logo")),
					"javascript:void(0)",
					 array('escape' => false));?>
		</div>-->
<script>
$(document).ready(function(){
    $(".nav-togle").click(function(){
        $("#change-color").toggle();
    });
});
</script>




<div class="nav-togle"><img src="/worldstashdev/img/togle.png">
<div class="change-color"></div>
</div>
                <nav id="change-color" class="change-color">
                    <ul id="tab_links" class="main-nav">
                        <li id="h_home"><?php echo $this->Html->link('Home',array('controller'=>'pages','action'=>'display')); ?></li>
                        <li id="h_events"><?php echo $this->Html->link('Events',array('controller'=>'categories','action'=>'categories_list')); ?></li>
                        <li id="h_photos"><a href="javascript:void(0)">Photos</a></li>
                        <li id="h_Tickets"><a href="javascript:void(0)">Buy Ticket</a></li>
                        <li id="h_friends" onclick="your_friend();"><a href="javascript:void(0)">Your Friends</a></li>
                        <li id="h_about"><?php echo $this->Html->link('About',array('controller'=>'users','action'=>'about_us')); ?></li>
                        <li id="h_contact"><?php echo $this->Html->link('Contact',array('controller'=>'users','action'=>'contact_us')); ?></li>
                        <li id="h_blog"><?php echo $this->Html->link('Blogs',array('controller'=>'blogs','action'=>'blog_list')); ?></li>
                    </ul>

                    <ul class="right-media">
                        <li class="facebook"><a target="_blank" href="<?php echo $images['Setting']['facebook_page']; ?>"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter"><a target="_blank" href="<?php echo $images['Setting']['twitter_page']; ?>"><i class="fa fa-twitter"></i></a></li>
                        <li class="pinterest"><a href="javascript:void(0)"><i class="fa fa-pinterest-p"></i></a></li>
                        <li class="language">| <a href="javascript:void(0)">EN</a>
                            <ul class="lang-menu">
                                <li><a href="javascript:void(0)">English</a></li>
                                <li><a href="javascript:void(0)">Spanish</a></li>
                            </ul>
                        </li>
                        <li class="dots-drop"><a href="javascript:void(0)"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                            <ul class="sub-menu help">
                                <li><a href="help">Help</a></li>
                                <li><a href="team">We’re Hiring</a></li>
                                <li><a href="privacy">Privacy & Terms</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="right-nav login">
			<?php if(empty($_SESSION['Auth']['User'])) { ?>
                        <li>
                            <a id="login" href="#modal" class="pop-ups">Login</a>

                        </li>
                        <li><a id="sign-up" href="#modal" class="pop-ups" >SignUp</a></li>
			<?php } else { ?>
			             <li class="dots-drop arrow">
						 <?php if(empty($_SESSION['Auth']['User']['profile_image']) || $_SESSION['Auth']['User']['profile_image'] == "") { ?>
                                  <?php echo $this->Html->image("Icon-users.png", array('alt' => 'footerimg1','width'=>'45', 'height'=>'45', 'class'=>'profile-img'));
                                 } else { 
                                 echo $this->Html->image($_SESSION['Auth']['User']['profile_image'], array('alt' => 'footerimg1','width'=>'45', 'height'=>'45','class'=>'profile-img'));
                                 } ?>
                        <?php echo $this->Html->link($_SESSION['Auth']['User']['first_name'],array('controller'=>'users','action'=>'my_profile'),array('class'=>'profile-name')); ?>
                            <ul class="sub-menu">
                                <li><?php echo $this->Html->link('Dashboard',array('controller'=>'users','action'=>'my_profile')); ?></li>
                                <li><?php echo $this->Html->link('Edit Profile',array('controller'=>'users','action'=>'my_account'));?></li>
                                <li><a href="javascript:void();">Invite Friends</a></li>
                                <li><?php echo $this->Html->link('Logout',array('controller'=>'users','action'=>'logout')); ?></li>
                            </ul>
								<?php echo $this->Html->image("arrow.png", array('alt' => 'footerimg1','width'=>'15', 'height'=>'15', 'class'=>'arrow'));?>
                        </li>
			<?php } ?>
                    </ul>
                    <a href="" class="announce css_blink">Announce</a>
                    <div class="clear"></div>
                </nav>

            </header>
	    <?php echo $this->Session->flash(); ?>
            <div class="slider">
		      <?php if($this->params['action'] == "about_us" || $this->params['action'] == "contact_us") { ?>
              <ul class="static-img">
                <li><?php echo $this->Html->image("Trekking.jpg", array('alt' => 'bannerimg1')); ?></li>
              </ul>
              <?php } elseif ($this->params['action'] == "event_detail") { ?>
                <ul class="static-img">
                <li><?php echo $this->Html->image($event_detail['Event']['event_cover_image']) ?></li>
              </ul>
             <?php } elseif ($this->params['action'] == "subcategories_list") { ?>
                <ul class="bxslider">
                    <?php $count = count($all_subcat_images); for($i = 0; $i < $count; $i++) { 
                        $img_url = base64_encode($all_subcat_images[$i]['subcategories']['id']);

                    ?>
                <li><?php echo $this->Html->link(
                        $this->Html->image($all_subcat_images[$i]['subcategories']['subcategory_image']), "http://dev.staplelogic.in/worldstashdev/events-list?eid=".$img_url, array('target'=>'_blank','escape' => false));
                    ?>
                </li>
                <?php } ?>
              </ul>
             <?php } elseif (($this->params['action'] == "events_list") || ($this->params['action'] == "events_list_s")){ ?>
                 <ul class="static-img">
                <li><?php echo $this->Html->image($sub_cat_image) ?></li>
              </ul>
             <?php } elseif ($this->params['action'] == "categories_list") { ?>
                 <ul class="bxslider">
                    <?php $count = count($all_cat_images); for($i = 0; $i < $count-1; $i++) { 
                        $img_url = base64_encode($all_cat_images[$i]['categories']['id']);
                    ?>
                <li><?php echo $this->Html->link(
                        $this->Html->image($all_cat_images[$i]['categories']['category_image']), "http://dev.staplelogic.in/worldstashdev/subcategories-list?scid=".$img_url, array('target'=>'_blank','escape' => false));
                    ?>
                </li>
                <?php } ?>
              </ul>
             <?php } else { ?>
			<?php if($images['Setting']['banner_type'] == '0') { ?>
             <ul class="bxslider">
            <?php $count = count($banner_data['images']); ?>
			<?php for($i = 0; $i < $count; $i++) { ?>
                <li><?php echo $this->Html->link(
                        $this->Html->image($banner_data['images'][$i]), $banner_data['urls'][$i], array('target'=>'_blank','escape' => false));
                    ?>
                </li>
                <?php } ?>
                </ul>
                <?php } else { ?>
				<ul class="slider">
				 <li>
				<?php if(empty($images['Setting']['videos_url'])) { ?>	 
				 <video id="pretzel-video" class="video-playing" loop="loop" autoplay="autoplay">
					<source src="<?php echo $images['Setting']['video_url_1'] ?>" type="video/mp4" ></source>
					<source src="<?php echo $images['Setting']['video_url'] ?>" type="video/webm"></source>
				</video>
				<?php } else { ?>
					<iframe width="100%" height="715" src="<?php echo 'http://www.youtube.com/embed/'. $images['Setting']['videos_url'].'?autoplay=1&loop=1&rel=0&showinfo=0&color=white&iv_load_policy=3frameborder="0" allowfullscreen' ?>"></iframe>
				<?php } ?>
				</li>
               </ul>
		 <!--<iframe width="820" height="715" src="http://www.youtube.com/embed/XGSy3_Czz8k?autoplay=1"></iframe>-->
	<?php } ?>
    <?php } ?>
    <?php if($this->params['action'] != "about_us") { ?>
                <div class="search">
				
                    <div class="sld-search">
                        <form id="search_form" method="post" action="search">
                            <?php echo $this->Form->input('cat_id', array('type'=>'select','options'=> $list_categories, 'id'=>'cat_id', 'empty' => 'Select Category','class'=>'form-control','onchange'=>'categorySelected()', 'required'=>'required','label'=>false, 'div'=>false)) ?>
                            
                            <select class="form-control" id="sub_cat_id" name="sub_cat_id">
                                <option> Select Sub-Category </option>
                            </select>
                            <select class="form-control" name="date_option" id="date_option" onchange="calenderdate();">
                                <option value=""> Select Date </option>
								<option value="0"> All </option>
                                <option value="1"> Today </option>
                                <option value="2"> Tommorow </option>
                                <option value="4"> This Week </option>
                                <option value="3"> This Weekend </option>
                                <option value="5"> Next Week </option>
                            </select>                            
                            <?php $age = array('All'=>'All años','14-22'=>'14 – 22 años','23-29'=>'23 – 29 años','30-37'=>'30 – 37 años','38-45'=>'38 – 45 años','46-53'=>'46 – 53 años','54-61'=>'54 – 61 años','62-70'=>'62 – 70 años') ?>
                            <?php echo $this->Form->input('age_limit', array('options'=> $age,'id'=>'age_limit', 'class'=>'form-control','default'=>'Select Age', 'required'=>'required','label'=>false, 'div'=>false)) ?>
                            <?php echo $this->Form->input('where', array('id'=>'where', 'class'=>'form-control','placeholder'=>'Where', 'required'=>'required','label'=>false, 'div'=>false)) ?>
                            <span class="date">
				<?php echo $this->Html->image("calender.png", array("alt" => "calender")); ?>
                                <input id="datepicker" name="event_date" placeholder="Select Date" class="form-control" onchange="dateoption();">
                            </span>
                            
                            <input class="submit" value="Search" type="submit">
                        </form>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="clear"></div>

				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>

		<!-- Footer starts Here -->
        <footer>
            <div class="footer-media">
                <ul>
                    <li><a target="_blank" href="<?php echo $images['Setting']['twitter_page']; ?>"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="<?php echo $images['Setting']['facebook_page']; ?>"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                    <li class="css_blink"><a href="javascript:void(0)"><i class="fa fa-heart"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-pinterest-p"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-youtube-square"></i></a></li>
                    <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li>
                </ul>
            </div>
            <div class="footer-area">
                <div class="footer-mid">
                   <!-- <?php // if($this->params['action'] == "display") { ?> -->
                   <?php  if($frontend_footer == 1) { ?>
                    <ul>
			<?php foreach($footer_data as $data) { ?>
                        <li><?php echo $this->Html->image($data, array('alt' => 'footerimg1')); ?></li>
			<?php } ?>
                    </ul>
                    <?php  } ?>
                    <ul class="footer-mid links">
                        <li><a href="/">Home</a></li>
                        <li><a href="about-us">About us</a></li>
                        <li><a href="contact-us">Contact</a></li>
                        <li><a href="#model">Sign up</a></li>
                        <li><a href="privacy">Privacy</a></li>
                    </ul>
                    <ul class="footer-mid links">
                        <li><a href="javascript:void(0)">Printing</a></li>
                        <li><a href="javascript:void(0)">Timing</a></li>
                        <li><a href="javascript:void(0)">Ticket Printing</a></li>
                        <li><a href="javascript:void(0)">Band</a></li>
                    </ul>
                    <ul class="footer-mid links">
                        <li><a href="javascript:void(0)">Clients</a></li>
                        <li><a href="javascript:void(0)">Testemonial</a></li>
                        <li><a href="javascript:void(0)">Case Studies</a></li>
                        <li><a href="javascript:void(0)">Improvement</a></li>
                    </ul>
                    <ul class="footer-mid links">
                        <li><a href="about-us">About</a></li>
                        <li><a href="team">Team</a></li>
                        <li><a href="javascript:void(0)">Careers</a></li>
                        <li><a href="blog-list">Blog</a></li>
                        <li><a href="javascript:void(0)">Advertising</a></li>
                        <li><a href="javascript:void(0)">Ticket Support</a></li>
                        <li><a href="terms-of-use">Terms of use</a></li>
                    </ul>
                </div>
            </div>
            <div class="copyright">
                <div class="footer-copy">
                    <p class="left">© <?php echo date('Y');?> <a href="javascript:void(0)">World Stash</a> All Rights Reserved</p>
                    <p class="right">Stapled by <a href="http://staplelogic.com/" target="blank">StapleLog!c</a></p>
                </div>
            </div>
        </footer>
	<!-- Footer Ends Here -->

		<div id="modal" class="modal-cont">
			<div class="popupContainer">
            <span class="modal_close"><i class="fa fa-times"></i></span>

            <section class="popupBody">

                <!-- Social Login -->
                <div class="social_login">
                    <div class="">
                        <a href="javascript:void(0)" onclick="return Login()" class="social_box fb">
                            <span class="icon"><i class="fa fa-facebook"></i></span>
                            <span class="icon_title">Login with Facebook</span>

                        </a>

                        <a href="<?php echo $authUrl; ?>" class="social_box google">
                            <span class="icon"><i class="fa fa-google-plus"></i></span>
                            <span class="icon_title">Login with Google</span>
                        </a>
                    </div>

                    <div class="centeredText">
                        <span><hr><p>Or</p></span>
                    </div>
                    <div class="user_login">
                        <form id="user_login" method="post" action="users/login">
                            <div class="reg-input">
                                <input type="text" placeholder="Email Address" name="User[email]" id="email" required="required" />
                                <span><i class="fa fa-at"></i></span>
                            </div>
                            <div class="reg-input">
                                <input type="password" placeholder="Password" name="User[password]" id="password" required="required" />
                                <span><i class="fa fa-lock"></i></span>
                            </div>

                            <div class="checkbox">
                                <input id="remember" type="checkbox" />
                                <label for="remember">Remember me </label>
                            </div>

                        <a href="javascript:void(0)" class="forgot_password">Forgot password?</a>
                    

                    <div class="action_btns">
                        <button class="one_half last"> Login </button>
                        
                        <hr>
                        <div class="text-left">
                            Don't have an account?
                            <a data-modal-href="javascript:void(0)" class="pop-signup"  href="/signup_login">Sign up</a>  
                        </div>
                    </div>
		  </form>
		</div>
               </div>

                <!-- Username & Password Login form -->

                <!-- Register Form -->
				<div class="sign-up">
                    <div class="">
                        <a href="javascript:void(0)"  onclick="return Login()"  class="social_box fb">
                            <span class="icon"><i class="fa fa-facebook"></i></span>
                            <span class="icon_title">Sign up with Facebook</span>

                        </a>

                        <a href="<?php echo $authUrl; ?>" class="social_box google">
                            <span class="icon"><i class="fa fa-google-plus"></i></span>
                            <span class="icon_title">Sign up with Google</span>
                        </a>
                    </div>

                    <div class="centeredText">
                        <span><hr><p>Or</p></span>
                    </div>
                    

                    <div class="action_btns">
                        <div class="one_half"><a href="javascript:void(0)" class="btn pop-signup-email">Sign up with Email</a></div>
						<p class="reg-links">By signing up, I agree to World Stash <a href="terms-of-use">Terms of Service</a>, <a href="privacy">Privacy Policy</a>, <a href="javascript:void(0)"> Refund Policy</a>,<a href="javascript:void(0)"> Cookie Policy</a> & consent &nbsp;to receiveving marketing communications from worldstache.</p>
					<hr>
                            <div class="text-left">
                                Do you have an account?
                                <a href="/signup_login" class="pop-login" data-modal-href="javascript:void(0)">Login</a>  
                            </div>
							<div class="clear"></div>
                    </div>
					
                </div>

                <div class="user_register">
                    <form id="user_register" method="post" action="users/register">
                        <p>Sign up with <a href="javascript:void(0)" onclick="return Login()">Facebook</a> or <a href="javascript:void(0)">Google</a></p>
                        <div class="centeredText">
                            <span><hr><p>Or</p></span>
                        </div>
                        <div class="reg-input">
                            <input type="text" placeholder="First Name" name="User[first_name]" id="first_name" required="required"/>
                            <span><i class="fa fa-user"></i></span>
                        </div>
                        <div class="reg-input">
                            <input type="text" placeholder="Last Name" name="User[last_name]" id="last_name" required="required"/>
                            <span><i class="fa fa-user"></i></span>
                        </div>
                        <div class="reg-input">
                            <input type="email" placeholder="Email Address" name="User[email]" id="email_address" required="required"/>
                            <span><i class="fa fa-at"></i></span>
                        </div>
                        <div class="reg-input">
                            <input type="password" placeholder="Password" name="User[password]" id="password" required="required"/>
                            <span><i class="fa fa-lock"></i></span>
                        </div>
                        <label><b>Birthday</b></label>
                        <div class="control-group-birth">
                            <div class="select">
                                 <select name="User[birthday_month]" id="user_birthday_month" required="required">
                                    <option value="">Month</option>
                                    <option value="1">January</option>
                                    <option value="2">February</option>
                                    <option value="3">March</option>
                                    <option value="4">April</option>
                                    <option value="5">May</option>
                                    <option value="6">June</option>
                                    <option value="7">July</option>
                                    <option value="8">August</option>
                                    <option value="9">September</option>
                                    <option value="10">October</option>
                                    <option value="11">November</option>
                                    <option value="12">December</option>
                                </select>

                            </div>
                            <div class="select">
                                <select name="User[birthday_day]" id="user_birthday_day" required="required">
                                    <option value="">Day</option>
				    <?php for($i=1; $i<=31; $i++) { ?>
                                    <option value="<?php echo $i ?>"> <?php echo $i ?></option>
				    <?php } ?>
                                </select>

                            </div>
                            <div class="select">

                                <select name="User[birthday_year]" id="user_birthday_year" required="required">
                                    <option value="">Year</option>

                                    <?php
                                    for ($x=date("Y") - 18; $x>1930; $x--)
                                      {
                                        echo'<option value="'.$x.'">'.$x.'</option>'; 
                                      } 
                                    ?> 
                                </select>

                            </div>
                        </div>
                        <div class="checkbox">
                            <input id="send_updates" type="checkbox" name="User[auto_updates]" />
                            <label for="send_updates"> Tell me about World Stash news</label>
                        </div>
                        <p class="reg-links">By signing up, I agree to World Stash <a href="terms-of-use">Terms of Service</a>, <a href="privacy">Privacy Policy</a>, <a href="javascript:void(0)">Guest Refund Policy</a>, and <a href="javascript:void(0)">Host Guarantee Terms</a>. </p>
                        <div class="action_btns bgyellow">
                            
                            <button class="one_half last"> Register </button>

                            <hr>
                            <div class="text-left">
                                Do you have an account?
                                <a href="/signup_login" class="pop-login" data-modal-href="javascript:void(0)">Login</a>  
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
		</div>
		
    <span class="totop"></span>
		<script type="text/javascript">
					$(document).ready(function () {
					$("#tab_links li").click(function(){
							var id = $(this).attr("id");
							$('#' + id).siblings().find(".act").removeClass("act");
								
							$('#' + id).addClass("act");
							localStorage.setItem("selectedolditem", id);
							
							});
							var selectedolditem = localStorage.getItem('selectedolditem');

					if (selectedolditem != null) {
						$('#' + selectedolditem).siblings().find(".act").removeClass("act");
						$('#' + selectedolditem).addClass("act");
					}
					
				});
                 
				</script>
	 <script>
		jQuery(function() {
		jQuery("#datepicker").datepicker({minDate:0});
		});
	</script>
	<script>
	function categorySelected() {
	var id = jQuery("#cat_id").val();
	if(id == 1) {
		jQuery("#sub_cat_id").find('option').remove().end();
		jQuery("#sub_cat_id").append(jQuery("<option>",{value: 'All',text: 'All'}));
		} else {
	jQuery.ajax({
                        type: "POST",
                        url: 'http://dev.staplelogic.in/worldstashdev/subcategories/getSubcategories',
                        data: {id: id},
                        success: function(message){
                        var options = jQuery.parseJSON(message);
                        var temp = [];
                        jQuery("#sub_cat_id").find('option').remove().end();
                        jQuery("#sub_cat_id").append('<option value="All">All</option>');
                        jQuery.each(options, function(key, value) {
                            temp.push({v:value, k: key});
                        });
                        temp.sort(function(a,b){
                           if(a.v > b.v){ return 1}
                            if(a.v < b.v){ return -1}
                              return 0;
                        });
                        jQuery.each(temp, function(key, obj) {
                        jQuery("#sub_cat_id").append(jQuery("<option>",{
                            value: obj.k,
                            text: obj.v
                            }));
                        });
						},
                        
                    });
	}
	}
	
    function dateoption(){

        var opt = jQuery("#date_option").val();
        if(opt != "Select Date"){
        jQuery("#date_option").val("");
        }
    }

    function calenderdate(){

        var opt = jQuery("#datepicker").val();
        if(opt != ""){
        jQuery("#datepicker").val("");   
        }
    }

    </script>
    <!--JS FIles-->
    <?php echo $this->Html->script('modernizr-2.5.3.min.js'); ?>
    <?php echo $this->Html->script('jquery.placeholder.js'); ?>
    <?php echo $this->Html->script('footer-script.js'); ?>
    <?php echo $this->Html->script('jquery.masonry.min.js'); ?>
    <?php echo $this->Html->script('PositionSticky.min.js'); ?>
    <?php echo $this->Html->script('jQuery.positionSticky.min.js'); ?>
    <?php echo $this->Html->script('jquery.bxslider.min.js'); ?>
    <?php echo $this->Html->script('jquery.leanModal.min.js'); ?>
    <?php echo $this->Html->script('event_custom.js'); ?>
    <?php if($this->params['action'] == "display") { ?>
    <script>
      $.Mason.prototype.resize = function() {
        this._getColumns();
        this._reLayout();
      };
      
      $.Mason.prototype._reLayout = function( callback ) {
        var freeCols = this.cols;
        if ( this.options.cornerStampSelector ) {
          var $cornerStamp = this.element.find( this.options.cornerStampSelector ),
              cornerStampX = $cornerStamp.offset().left - 
                ( this.element.offset().left + this.offset.x + parseInt($cornerStamp.css('marginLeft')) );
          freeCols = Math.floor( cornerStampX / this.columnWidth );
        }

        var i = this.cols;
        this.colYs = [];
        while (i--) {
          this.colYs.push( this.offset.y );
        }

        for ( i = freeCols; i < this.cols; i++ ) {
          this.colYs[i] = this.offset.y + $cornerStamp.outerHeight(true);
        }
        this.layout( this.$bricks, callback );
      };

      $(window).on('load', function(){
        $('#masonry').masonry({
          itemSelector: '.item,.itemeve',
          columnWidth: 60,
          cornerStampSelector: '.corner-stamp'
        });
      });
    </script>
    <?php } else { ?>
        <script>
            $(function(){
            
            $('#masonry').masonry({
              itemSelector: '.item,.itemeve',
              columnWidth: 20,
            });
            
          });
        </script>

    <?php } ?>
    </body>
</html>
