<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">View Artist</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#create">Create New Artist</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_artist)) { ?>
                                <p> No Pending Artist</p>
                                <?php } ?>
                            <?php foreach($all_artist as $artist) { 
                                $id = base64_encode($artist['Artist']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $artist['Artist']['artist_name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: Professional</p>
                                        <?php if($artist['Artist']['artist_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($artist['Artist']['artist_status'] == '0') { ?>
                                       <a href='../artists/edit_artist_p?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <?php } ?>
                                       <a href='../artists/delete_artist_p?aid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                </div>  
                        </div>
                        <div aria-labelledby="profile-tab" id="create" class="tab-pane fade" role="tabpanel">
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-controls="event" data-toggle="tab" id="event-tab" role="tab" href="#event">General</a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Images</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="event-tab" id="event" class="tab-pane fade in active" role="tabpanel">
                        <div class="content add-event">
                    <?php echo $this->Form->Create('',array('id'=>'add_artist','enctype'=>'multipart/form-data')) ?>
                
                      <div class="edit-field">
                <?php echo $this->Form->input('artist_name', array('id'=>'artist_name','placeholder'=>'Artist Name','class'=>'form-control', 'required'=>'required', 'div'=>false )) ?>
                                </div>
                      <div class="edit-field">
                      <label>Global Artist </label>
                <?php echo $this->Form->input('global_artist',array('type' => 'checkbox','div'=>false,'class'=>'form-control global-radio', 'label'=>false)); ?>
                                </div>
                      <div class="edit-field">
                                    <label> Music / Style Tags </label>
                <?php echo $this->Form->textarea('music_style_tags', array('id'=>'artist_description','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'required')) ?>
                                </div>
                      <div class="edit-field">
                                    <label> Description </label>
                <?php echo $this->Form->textarea('artist_description', array('id'=>'artist_description','rows'=>'4','cols'=>'4','class'=>'form-control','required'=>'required')) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('facebook_page_url', array('id'=>'facebook_page_url','placeholder'=>'Facebook Page Url','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('website_url', array('id'=>'artist_name','placeholder'=>'Website Url','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('phone_number', array('id'=>'artist_name','placeholder'=>'Phone Number','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('email', array('id'=>'phone_number','placeholder'=>'Email','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                 </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('soundcloud', array('id'=>'email','placeholder'=>'Soundcloud','class'=>'form-control', 'required'=>'', 'div'=>false)) ?>
                                 </div>
                        <div class="edit-field">
                                    <label> Twitter Widget </label>
                <?php echo $this->Form->textarea('twitter_widget', array('id'=>'twitter_widget','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'', 'div'=>false)) ?>
                                 </div>

                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Next</a></li>
                            </ul>
                        </div>
                        <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_logo"));?>
                                    <div class="addnew-discription">
                                        <label>Artist Logo</label>
                                        <?php echo $this->Form->input('artist_logo', array('id'=>'artist_logo', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_cover_image"));?>
                                    <div class="addnew-discription">
                                        <label>Artist Cover Image</label>
                                        <?php echo $this->Form->input('artist_cover_image', array('id'=>'artist_cover_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                               </div>
                               <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save Artist">
                          
                                </div>
                             <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
