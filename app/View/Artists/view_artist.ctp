<div class="event-main view-artist">
                <div class="sld-main sld-detail">
                    <!--<ul class="detail-slider">
                        <li><?php// echo $this->Html->image($artist_detail['Artist']['artist_cover_image']) ?> </li>
                    </ul>-->
                    <?php $id = base64_encode($artist_detail['Artist']['id']);?>
                    <ul class="static-img">
                        <li><?php echo $this->Html->image($artist_detail['Artist']['artist_cover_image']) ?></li>
                    </ul>
                    <?php if($user_id != "" && $user_id != null) { ?>
    <div class="put-cmt">
        <span><?php echo $this->Html->image($user_data,array('width'=>'100','height'=>'100'));?></span>
        <form action="post-comment?eid=<?php echo $id;?>" method="post">
            <textarea name="data[Comment][comment]" class="form-control" placeholder="Comment here" readonly="readonly"></textarea>
            <input type="hidden" value="artist" name="data[Comment][comment_type]">
            <input type="submit" value="Comment" disabled="disabled">
        </form>
    </div>
    <?php } ?>

        <?php if(!empty($all_comments) || $all_comments != null){ ?>
            <h4>Reviews</h4>
            <?php foreach($all_comments as $comment) { ?>
                <div class="review-main">
               <?php echo $this->Html->image($comment['User']['profile_image'],array('width'=>'50','height'=>'50'));?> <span><?php echo $comment['Comment']['comment'];?></span>
                </div>
               <?php } }?>
                </div>
                <div class="map-detail">
                    <h3>
                        <?php echo $artist_detail['Artist']['artist_name'];?>
                    </h3>
                    <ul class="venue">
                        <?php if(!empty($artist_detail['Artist']['phone_number'])) { ?>
                        <li><i class="fa fa-phone"></i><?php echo $artist_detail['Artist']['phone_number'];?> </li>
                        <?php } ?>
                        <?php if(!empty($artist_detail['Artist']['website_url'])) { ?>
                        <li><i class="fa fa-globe"></i><?php echo $artist_detail['Artist']['website_url'];?> </li>
                        <?php } ?>
                    </ul>
                    <p>
                        <?php echo $artist_detail['Artist']['artist_description'];?>
                    </p>
                </div>
                <?php if(!empty($artist_detail['Artist']['facebook_widget'])) { ?>
                    <div class="tweet-artist">
                        <?php echo $artist_detail['Artist']['facebook_widget'];?>
                    </div>
                    <?php } ?>
                <div class="more-events">
                    <h3>More Upcoming Events...</h3>
                    <div class="slider1">
                        <?php foreach($upcoming_events as $events) { ?>
                        <?php $id = base64_encode($events['Event']['id'])?>
                        <div class="slide">
                        <?php echo $this->Html->link(
                            $this->Html->image($events['Event']['event_image'], array("alt" => "event_img")),"../events/event_detail?eid=".$id,array('escape' => false, 'target'=>'_blank'));?>
                        <?php echo $events['Event']['event_title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>