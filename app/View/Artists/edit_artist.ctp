<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Edit Artist</a></li>
                        <li role="presentation"><a aria-controls="pending" data-toggle="tab" id="pending-tab" role="tab" href="#pending">Pending Artists</a></li>
                       <li role="presentation"><a aria-controls="active" data-toggle="tab" id="active-tab" role="tab" href="#active">Active Artist</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-expanded="true" aria-controls="artist" data-toggle="tab" role="tab" id="artist-tab" href="#artist"> Edit </a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Images</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="artist-tab" id="artist" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                  <?php echo $this->Form->Create('',array('id'=>'edit_artist','enctype'=>'multipart/form-data')) ?>
                
                      <div class="edit-field">
                <?php echo $this->Form->input('artist_name', array('id'=>'artist_name','placeholder'=>'Artist Name','class'=>'form-control', 'required'=>'required', 'div'=>false,'value'=> $artist['Artist']['artist_name'])) ?>
                                </div>
                      <div class="edit-field">
                      <label>Global Artist 
                        <a title="" data-toggle="tooltip" href="#" data-original-title="Check this box if artist has a global career" class="addtooltip"><i class="fa fa-info-circle"></i></a>
                      </label>
                <?php echo $this->Form->input('global_artist',array('type' => 'checkbox','div'=>false,'class'=>'form-control global-radio', 'label'=>false)); ?>
                                </div>
                      <div class="edit-field">
                                    <label> Music / Style Tags </label>
                <?php echo $this->Form->textarea('music_style_tags', array('id'=>'artist_description','rows'=>'5','cols'=>'4','class'=>'form-control','required'=>'required','value'=> $artist['Artist']['music_style_tags'])) ?>
                                </div>
                      <div class="edit-field">
                                    <label> Description </label>
                <?php echo $this->Form->textarea('artist_description', array('id'=>'artist_description','rows'=>'4','cols'=>'4','class'=>'form-control','required'=>'required','value'=> $artist['Artist']['artist_description'])) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('facebook_page_url', array('id'=>'facebook_page_url','placeholder'=>'Facebook Page Url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=> $artist['Artist']['facebook_page_url'])) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('website_url', array('id'=>'artist_name','placeholder'=>'Website Url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=> $artist['Artist']['website_url'])) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('phone_number', array('id'=>'artist_name','placeholder'=>'Phone Number','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=> $artist['Artist']['phone_number'])) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('email', array('id'=>'phone_number','placeholder'=>'Email','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=> $artist['Artist']['email'])) ?>
                                 </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('soundcloud', array('id'=>'email','placeholder'=>'Soundcloud','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=> $artist['Artist']['soundcloud'])) ?>
                                 </div>
                        <div class="edit-field">
                                    <label> Twitter Widget </label>
                <?php echo $this->Form->textarea('facebook_widget', array('id'=>'facebook_widget','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'', 'div'=>false,'value'=> $artist['Artist']['facebook_widget'])) ?>
                                 </div>

                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Next</a></li>
                            </ul>
                        </div>
                        <div aria-labelledby="images-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="add-new-event adddish">
                                    <?php if(empty($artist['Artist']['artist_logo'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_logo"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"120px","height"=>"100px","alt"=>"artist_logo"));?>    
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label> Artist Logo </label>
                                        <?php echo $this->Form->input('artist_logo', array('id'=>'artist_logo', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php if(empty($artist['Artist']['artist_cover_image'])) { ?>
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_logo"));?>
                                    <?php } else { ?>
                                    <?php echo $this->Html->image($artist['Artist']['artist_cover_image'], array("width"=>"120px","height"=>"100px","alt"=>"artist_cover_image"));?>    
                                    <?php } ?>
                                    <div class="addnew-discription">
                                        <label> Artist Cover Image </label>
                                        <?php echo $this->Form->input('artist_cover_image', array('id'=>'artist_cover_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                </div>
                               
                               <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Update Artist">
                          
                                </div>
                             </div>
                           <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                        <div aria-labelledby="pending-tab" id="pending" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($pending_artists)) { ?>
                                <p> No Pending Artist</p>
                                <?php } ?>
                                <?php foreach($pending_artists as $artist) { 
                                $id = base64_encode($artist['Artist']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"120px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $artist['Artist']['artist_name']; ?></h4>
                                        <?php if($artist['Artist']['artist_status'] == '1') { ?> 
                                        <p>Status: Active </p>
                                        <?php } else { ?>
                                        <p>Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($artist['Artist']['artist_status'] == '0') { ?>
                                       <a href='../artists/activate_artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } ?>
                                       <a href='../artists/edit_artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Edit</button></a>
                                       <a href="../places/delete_artist?aid=<?php echo $id; ?>"><button type="button" onclick="if (confirm("Are you sure you want to delete this place ?")) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div> 
                        </div>
                        <div aria-labelledby="active-tab" id="active" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_artist)) { ?>
                                <p> No Active Artist</p>
                                <?php } ?>
                                <?php foreach($all_artist as $artist) { 
                                $id = base64_encode($artist['Artist']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"120px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $artist['Artist']['artist_name']; ?></h4>
                                        <?php if($artist['Artist']['artist_status'] == '1') { ?> 
                                        <p>Status: Active </p>
                                        <?php } else { ?>
                                        <p>Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($artist['Artist']['artist_status'] == '0') { ?>
                                       <a href='../artists/activate_artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } else { ?>
                                       <a href='../artists/deactivate_artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Deactivate</button></a>
                                       <?php } ?>
                                       <a href="../artists/delete_artist?aid=<?php echo $id; ?>"><button type="button" onclick="if (confirm("Are you sure you want to delete this place ?")) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>