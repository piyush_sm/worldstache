<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<?php echo $this->Html->css('tag/bootstrap-tagsinput'); ?>
<?php echo $this->Html->script('tag/bootstrap-tagsinput.js'); ?>
<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Pending Artist</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#create">Create New Artist</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#advanced">Active Artists</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
						<div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
								<?php if(empty($pending_artists)) { ?>
                                <p> No Pending Artist</p>
                                <?php } ?>
							<?php foreach($pending_artists as $artist) { 
                                $id = base64_encode($artist['Artist']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $artist['Artist']['artist_name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: <?php echo $artist['User']['first_name'] ?></p>
                                        <?php if(!empty($artist['Artist']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $artist['Artist']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($artist['Artist']['website_url'])) { ?>
                                        <p><i class="fa fa-globe"></i> Website: <?php echo $artist['Artist']['website_url'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($artist['Artist']['email'])) { ?>
                                        <p><i class="fa fa-facebook"></i> Email: <?php echo $artist['Artist']['email'];?></p>
                                        <?php } ?>
                                        <?php if($artist['Artist']['artist_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($artist['Artist']['artist_status'] == '0') { ?>
                                        <a href='view-artist?aid=<?php echo $id; ?>' target="_blank"><button type="button" class="btn btn-warning">View Artist</button></a>
                                       <a href='activate-artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } ?>
                                       <a href='admin-edit-artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <a href='delete-artist?aid=<?php echo $id; ?>'><button type="button" onclick="if (confirm('Are you sure you want to delete this Artist ?')) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                </div>  
                        </div>
                        <div aria-labelledby="profile-tab" id="create" class="tab-pane fade" role="tabpanel">
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-controls="event" data-toggle="tab" id="event-tab" role="tab" href="#event">General</a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" class="next_form" href="#images">Images</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="event-tab" id="event" class="tab-pane fade in active" role="tabpanel">
                        <div class="content add-event eventhtml">
                	<?php echo $this->Form->Create('',array('id'=>'add_artist','enctype'=>'multipart/form-data')) ?>
                
                      <div class="edit-field">
                <?php echo $this->Form->input('artist_name', array('id'=>'artist_name','placeholder'=>'Artist Name','class'=>'form-control', 'required'=>'required', 'div'=>false )) ?>
                                </div>
                      <div class="edit-field">
					  <label>Global Artist 
                        <a title="" data-toggle="tooltip" href="#" data-original-title="Check this box if artist has a global career" class="addtooltip"><i class="fa fa-info-circle"></i></a>
                      </label>
                <?php echo $this->Form->input('global_artist',array('type' => 'checkbox','div'=>false,'class'=>'form-control global-radio', 'label'=>false)); ?>
                                </div>
                          
                                <div class="edit-field">
                <?php echo $this->Form->input('music_style_tags', array('type' => 'text','id'=>'music_style_tags','placeholder'=>'Style tags','class'=>'form-control optional','div'=>false, 'data-role'=>'tagsinput')) ?>
                                </div>
                      <div class="edit-field">
                                    <label> Description </label>
                <?php echo $this->Form->textarea('artist_description', array('id'=>'artist_description','rows'=>'4','cols'=>'4','class'=>'form-control','required'=>'required')) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('facebook_page_url', array('id'=>'facebook_page_url','placeholder'=>'Facebook Page Url','class'=>'form-control optional', 'required'=>'', 'div'=>false)) ?>
                                </div>
                      <div class="edit-field">
                <?php echo $this->Form->input('website_url', array('id'=>'artist_name','placeholder'=>'Website Url','class'=>'form-control optional', 'required'=>'', 'div'=>false)) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('phone_number', array('id'=>'phone_number','placeholder'=>'Phone Number','class'=>'form-control', 'required'=>'', 'div'=>false,'onkeypress'=>'return isNumberKey(event)')) ?>
                                </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('email', array('id'=>'email','placeholder'=>'Email','class'=>'form-control', 'onkeyup'=>"checkvalidemail(this.value)" , 'div'=>false)) ?>
                                  <span id="error" class="error" style="display:none display: block;color: red;font-size: 14px";></span>
                                 </div>
                        <div class="edit-field">
                <?php echo $this->Form->input('soundcloud', array('id'=>'soundcloud','placeholder'=>'Soundcloud','class'=>'form-control optional', 'required'=>'', 'div'=>false)) ?>
               
                
                                 </div>
                        <div class="edit-field">
                                    <label> Facebook Widget </label>
                <?php echo $this->Form->textarea('facebook_widget', array('id'=>'facebook_widget','rows'=>'5','cols'=>'8','class'=>'form-control','required'=>'', 'div'=>false)) ?>
                                 </div>

                            </div>
                            <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" class="next_form" role="tab" href="#images">Next</a></li>
                            </ul>
                        </div> 
                        <div aria-labelledby="profile-tab" id="images" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_logo"));?>
                                    <div class="addnew-discription">
                                        <label>Artist Logo</label>
                                        <?php echo $this->Form->input('artist_logo', array('id'=>'artist_logo', 'type'=>'file', 'label'=>false ,'div'=>false,'class'=>'fileup')) ?>
                                    </div>
                                </div>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"artist_cover_image"));?>
                                    <div class="addnew-discription">
                                        <label>Artist Cover Image</label>
                                        <?php echo $this->Form->input('artist_cover_image', array('id'=>'artist_cover_image', 'type'=>'file', 'label'=>false ,'div'=>false ,'class'=>'fileup',)) ?>
                                    </div>
                                </div>
                               </div>
                               <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning fileupload" value="Save Artist">
                          
                                </div>
                             <?php echo $this->Form->end(); ?>
                        </div>
                    </div>
                </div>
                        <div aria-labelledby="profile-tab" id="advanced" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_artist)) { ?>
                                    <p> No Active artist </p>
                                <?php } else { ?>
                                <?php foreach($all_artist as $artist) { 
                                $id = base64_encode($artist['Artist']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($artist['Artist']['artist_logo'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $artist['Artist']['artist_name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: <?php echo $artist['User']['first_name']; ?></p>
                                        <?php if(!empty($artist['Artist']['phone_number'])) { ?>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $artist['Artist']['phone_number'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($artist['Artist']['website_url'])) { ?>
                                        <p><i class="fa fa-globe"></i> Website: <?php echo $artist['Artist']['website_url'];?></p>
                                        <?php } ?>
                                        <?php if(!empty($artist['Artist']['email'])) { ?>
                                        <p><i class="fa fa-facebook"></i> Email: <?php echo $artist['Artist']['email'];?></p>
                                        <?php } ?>
                                        <p><i class="fa fa-user"></i> Activated By: <?php echo $_SESSION['Auth']['User']['first_name'];?>
                                        <?php if($artist['Artist']['artist_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($artist['Artist']['artist_status'] == '0') { ?>
                                       <a href='activate-artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } ?>
                                       <a href='deactivate-artist?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Deactivate </button></a>
                                       <a href='delete-artist?aid=<?php echo $id; ?>'><button type="button" onclick="if (confirm('Are you sure you want to delete this Artist ?')) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } } ?>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
<script>
  jQuery(document).ready(function () {
  jQuery(".next_form").click(function () 
        {   var isValid = true;
            jQuery('.eventhtml :input').each(function() 
            {          
            if(jQuery(this).val() ==""  && !jQuery(this).hasClass('optional'))            
             {              
                isValid = false;
                jQuery(this).css({"border": "1px solid red","background": "#FFCECE"});  
                 
                     
             }else {
				     error= jQuery('.error').text();
				     if(error !='')
				     {
						  isValid = false;
					 } 
			        jQuery(this).css({"border": "","background": ""});  
                    return true; 
                  }
                });
              return isValid;
             });
              jQuery(".fileupload").click(function () 
        {
            var isValid = true;             
            jQuery('.fileup').each(function() {  
                               
            if(jQuery(this).val() =="" )
             {              
                isValid = false;
                jQuery(this).css({"border": "1px solid red","background": "#FFCECE"});
            }    
           else {
                jQuery(this).css({"border": "","background": ""});  
                return true;
           }
           });
              return isValid;
         });
        });          
         function isNumberKey(evt)
         {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
         return true;
        }
        function checkvalidemail(val)
        {
			
        if(val!='')
	{ 
		var emailID = val;
		atpos = emailID.indexOf("@");
		dotpos = emailID.lastIndexOf(".");
   if (atpos < 1 || ( dotpos - atpos < 2 )) 
   {    document.getElementById("error").style.display="block";		
		document.getElementById("error").innerHTML="Please enter correct email ID";		
       return false;
      }else{ 	document.getElementById("error").innerHTML="";	}
	 }
}
</script>
