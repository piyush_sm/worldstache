<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Static Pages Content</a></li>
                        </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'manage_footer', 'enctype'=>'multipart/form-data')) ?>
                            <?php $option = array('about_us'=>'About Us', 'contact_us'=>'Contact Us', 'privacy'=>'Privacy', 'terms_of_use'=>'Terms Of Use', 'team'=>'Team'); ?>

                            <div class="edit-field">
                            <label> Select Page </label>
                            <?php echo $this->Form->input('page_selected', array('options'=>$option,'id'=>'page_selected', 'label'=>false ,'empty'=>'Select Page','div'=>false)) ?>
                          </div>

                          <div class="edit-field">
                            <label> Page Content </label>
                            <?php echo $this->Form->textarea('page_content', array('id'=>'page_content', 'class'=>'form-control custom-control ckeditor','label'=>false,'div'=>false)) ?>
                          </div>
                            
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save">
                            </div>
                          </div>
                       </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script>
CKEDITOR.replace('textareaId', {
    "filebrowserImageUploadUrl": "http://dev.staplelogic.in/worldstashdev/app/webroot/js/ckeditor/plugins/imgupload.php"
  });

/*jQuery("#page_selected").on('change',function(){

  var page = jQuery("#page_selected").val();
  alert(page); return false;
}); */
</script>
