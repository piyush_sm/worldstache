<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Banner Images</a></li>
                        </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'manage_banner', 'enctype'=>'multipart/form-data')) ?>
                           <?php //$options = array('0'=>'Image section', '1'=>'Video section'); ?> 

							<?php //echo $this->Form->radio('banner_type',$options, array('id'=>'banner_type', 'class'=>'form-control banner_type', 'value'=> $banner_settings['Setting']['banner_type'])); ?>
							<div class="edit-field">
							<label>Image Section</label>
							<input type="radio" class="mg-top banner_type" name="data[Setting][banner_type]" checked="checked" value="0">
							</div>
							<div class="edit-field">
							<label>video Section</label>
							<input type="radio" class="mg-top banner_type" name="data[Setting][banner_type]" value="1">
							</div>
                            <?php $file = 5;
							for( $i = 1; $i <= $file; $i++) {

							if(!empty($data['images'][$i-1])){ ?>
							<div class="image_section" style="display:block">	
                            <div class="add-new-event adddish">
                             <?php echo $this->Html->image($data['images'][$i-1], array('alt' => 'Image '.$i ,'height' => '120', 'width' => '120', 'div' => false));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false,'div'=>false)) ?>
                                    </div>
                             
                             <?php } else { ?>
								 <div class="add-new-event adddish">
                             <?php echo $this->Html->image("calendaradd.png", array("alt"=>"footer_image"));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                             
							<?php } ?>
                                <div class="addnew-discription">
                                        <?php echo $this->Form->input('image_url.', array('id'=>'image_url', 'class'=>'manege-url-input','div'=>false,'value'=> $data['urls'][$i-1])) ?>
                             </div>
                             <span class="err_message"></span>
                             </div>
							</div>
							<?php } ?>
							<div class="video_section" style="display:none">
							<div class="addnew-discription">
                                        <?php echo $this->Form->input('videos_url', array('id'=>'video_url', 'div'=>false, 'placeholder'=>'Video Url','label'=>false, 'class'=>'form-control')) ?>
                             </div>
                             </div>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save">
                            </div>
                          </div>
                       </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript">
    	jQuery(document).ready(function () {
    	jQuery(".banner_type").click(function () 
		{
		$id = jQuery(this).val();
			 
			if($id == 0)
			{
			    jQuery('.image_section').css('display','block'); 
			    jQuery('.video_section').css('display','none');
				
			}
			 if($id == 1)
			 {
				
				  jQuery('.video_section').css('display','block');
			      jQuery('.image_section').css('display','none'); 
			 }	 
		 });		 
		 $('input:file').change(function(e) {
			
        var files = e.originalEvent.target.files;
        for (var i=0, len=files.length; i<len; i++){
			if(files[i].size >= '252590')
			{
				$(this).val('');
				alert('Size cannot exceed more than 250 KB');
				//$(".err_message").html('Size cannot exceed more than 250 KB');
 			}           
        }
    });
		 
    	});
   
  </script>
