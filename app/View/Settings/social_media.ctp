<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Social Media</a></li>
                        </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'social_media', 'enctype'=>'multipart/form-data')) ?>
                            
							<div class="edit-field">
                <?php echo $this->Form->input('facebook_page', array('id'=>'facebook_page','placeholder'=>'Facebook Url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=>$facebook_page)) ?>
                                </div>
                                
                                <div class="edit-field">
                <?php echo $this->Form->input('twitter_page', array('id'=>'twitter_page','placeholder'=>'Twitter Url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=>$twitter_page)) ?>
                                </div>
                                
                                <div class="edit-field">
                <?php echo $this->Form->input('youtube_channel', array('id'=>'youtube_channel','placeholder'=>'Youtube url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=>$youtube_channel)) ?>
                                </div>
                                
                                <div class="edit-field">
                <?php echo $this->Form->input('instagram_page', array('id'=>'instagram_page','placeholder'=>'Instagram Url','class'=>'form-control', 'required'=>'', 'div'=>false,'value'=>$instagram_page)) ?>
                                </div>

					
							
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save">
                            </div>
                          
                       </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
