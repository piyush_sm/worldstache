<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Slider Images</a></li>
                        </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'manage_slider', 'enctype'=>'multipart/form-data')) ?>
                            <?php $count = 5;
							
							for( $i = 0; $i < count($data['images']); $i++) { ?>
							<div class="mng-sld">

							<?php  if(!empty($data['images'][$i])) { ?>
								
                            <div class="add-new-event adddish">
                             <?php echo $this->Html->image($data['images'][$i], array('alt' => 'Image '.$i ,'height' => '120', 'width' => '120', 'div' => false));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                             
                             <?php } else { ?>
								 <div class="add-new-event adddish">
                             <?php echo $this->Html->image("calendaradd.png", array("alt"=>"footer_image"));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                             
							<?php } ?>
							<div class="addnew-discription">
                                        <?php echo $this->Form->input('image_url.', array('id'=>'image_url', 'class'=>'manege-url-input','div'=>false,'value'=> $data['urls'][$i])) ?>
                             </div>
							 </div>
							<?php } ?>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save">
                            </div>
                          </div>
                       </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
