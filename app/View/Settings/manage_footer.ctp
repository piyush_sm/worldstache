<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Footer Images</a></li>
                        </ul>
                    <div class="tab-content" id="myTabContent">
                        <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                            <?php echo $this->Form->create('',array('id'=>'manage_footer', 'enctype'=>'multipart/form-data')) ?>
                            <?php $option = array('1'=>'Yes', '2'=>'No'); ?>
                            <label> Show footer on frontend </label>
                            <?php echo $this->Form->input('show_footer', array('options'=>$option,'id'=>'upload_image', 'label'=>false ,'div'=>false, 'empty'=>'Show Footer', 'default'=>$show_footer)) ?>
                            <?php $count = 4;
							for( $i = 1; $i <= $count; $i++) { 
								if(!empty($data[$i-1])) { ?>
                            <div class="add-new-event adddish">
                             <?php echo $this->Html->image($data[$i-1], array('alt' => 'Image '.$i ,'height' => '120', 'width' => '120', 'div' => false));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                             </div>
                             <?php } else { ?>
								 <div class="add-new-event adddish">
                             <?php echo $this->Html->image("calendaradd.png", array("alt"=>"footer_image"));?>
                                    <div class="addnew-discription">
                                        <?php echo $this->Form->input('upload_image.', array('id'=>'upload_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                             </div>
							<?php } } ?>
                            <div class="btn-group event-sub">
                                    <input type="submit" class="btn btn-warning" value="Save">
                            </div>
                          </div>
                       </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
          </div>
</div>
