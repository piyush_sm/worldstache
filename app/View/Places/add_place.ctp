<style>
      html, body, #map-canvas {
        height: 90%;
        margin: 0px;
        padding: 0px
      }
      .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 280px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        position:relative;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        margin-top: 5px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
        position: static !important;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
#cke_place_description {
    float: right;
}
    </style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
<script>
function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(-33.8688, 151.2195),
    zoom: 13
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  var input = /** @type {HTMLInputElement} */(document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    google.maps.event.addDomListener(radioButton, 'click', function() {
      autocomplete.setTypes(types);
    });
  }

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">Create New Place</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#create">Pending Places</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#advanced">Active Places</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                          <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-controls="event" data-toggle="tab" id="event-tab" role="tab" href="#event">General</a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images" class="next_form">Images</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="event-tab" id="event" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event placehtml">
                                <?php echo $this->Form->Create('',array('id'=>'add_place','enctype'=>'multipart/form-data')) ?>
                                    <!--<h2 class="dash-header">Add Place </h2>-->
                                    <div class="edit-field">
                                       <label>Name</label>
                                       <?php echo $this->Form->input('name', array('id'=>'name','placeholder'=>'Name','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                        
                                       <?php echo $this->Form->input('city', array('id'=>'pac-input','placeholder'=>'Select Place','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <label>Map Location</label>
                                    <div class="addart-arm">
                            <?php //echo $this->Form->input('map_location', array('id'=>'pac-input','placeholder'=>'Location','class'=>'form-control','label'=>false ,'required'=>'required', 'div'=>false)) ?>
                                     <div id="type-selector" class="controls">
                                    <input type="radio" name="type" id="changetype-all" checked="checked">
                                     <div id="map-canvas"></div>
                                     </div>
                                   </div>
                   
                                    <div class="edit-field">
                                        <label>Region</label>
                                        <?php $selectregion = array('region1'=>'region1','region2'=>'region2','region3'=>'region3','region4'=>'region4') ; ?>
                                        <?php echo $this->Form->input('region', array('id'=>'region','class'=>'form-control add-title', 'label'=>false , 'required'=>'', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Address
                                            <a class="addtooltip" data-original-title="6 Tags  maximum" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->textarea('address', array('id'=>'address','class'=>'form-control custom-control','required'=>'required', 'div'=>false)) ?>
                                    </div>

                                    <div class="edit-field">
                                       <label>Place Types</label>
                                       <?php echo $this->Form->input('place_types', array('id'=>'place_types','placeholder'=>'Place types','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Description
                                            <a class="addtooltip" data-original-title="6 Tags  maximum" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->textarea('place_description', array('id'=>'place_description','class'=>'form-control custom-control ckeditor','required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Website</label>
                                       <?php echo $this->Form->input('website', array('id'=>'website','placeholder'=>'Website Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Phone Number</label>
                                       <?php echo $this->Form->input('phone_number', array('id'=>'phone_number','placeholder'=>'Phone Number','class'=>'form-control optional', 'label'=>false ,'onkeypress'=>'return isNumberKey(event)', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Email</label>
                                       <?php echo $this->Form->input('email', array('id'=>'email','placeholder'=>'Email','class'=>'form-control', 'label'=>false ,  'onkeyup'=>"checkvalidemail(this.value)" ,  'div'=>false)) ?>
                                         <span id="error" class="error" style="display:none display: block;color: red;font-size: 14px";></span>
                                    </div>
                                    <div class="edit-field">
                                       <label>Twitter Url</label>
                                       <?php echo $this->Form->input('twitter_url', array('id'=>'email','placeholder'=>'Twitter Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                       <label>Facebook Url</label>
                                       <?php echo $this->Form->input('facebook_url', array('id'=>'email','placeholder'=>'Facebook Url','class'=>'form-control optional', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                        <label>Facebook Page Widget</label>
                                        <?php echo $this->Form->textarea('facebook_widget', array('id'=>'facebook_widget','class'=>'form-control custom-controls','required'=>'required', 'div'=>false)) ?>
                                    </div>
                                  </div>
                                  <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                                  <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images" class="next_form">Next</a></li>
                                  </ul>
                                </div>

                                <div aria-labelledby="images-tab" id="images" class="tab-pane fade" role="tabpanel">
                                  <div class="content add-event">
                                    <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_img"));?>
                                    <div class="addnew-discription">
                                      <label> Place Image </label>
                                        <?php echo $this->Form->input('place_image', array('id'=>'place_image', 'type'=>'file', 'class'=>'fileup', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                   </div>
                                  <div class="btn-group event-sub">
                                   <input type="submit" class="btn btn-warning fileupload" value="Save Place">
                                </div> 
                            </div>  
                        </div>
                      </div>
                    </div>
                      <div aria-labelledby="profile-tab" id="create" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                             <?php //pr($pending_place); ?>
								<?php if(empty($pending_place)) { ?>
                                <p> No Pending Place</p>
                                <?php } ?>
                      <?php foreach($pending_place as $place) { 
                                $id = base64_encode($place['Place']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($place['Place']['place_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $place['Place']['name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: <?php echo $place['User']['first_name'] ?></p>
                                        <p><i class="fa fa-location-arrow"></i> Location: <?php echo $place['Place']['city'];?></p>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $place['Place']['phone_number'];?></p>
                                        <p><i class="fa fa-globe"></i> Website: <?php echo $place['Place']['website'];?></p>
                                        <?php if($place['Place']['place_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($place['Place']['place_status'] == '0') { ?>
                                        <a href='view-place?pid=<?php echo $id; ?>' target="_blank"><button type="button" class="btn btn-warning">View Place</button></a>
                                       <a href='activate-place?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } ?>
                                       <a href='admin-edit-place?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <a href='delete-place?aid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>  
                        </div>
                        
                        <div aria-labelledby="profile-tab" id="advanced" class="tab-pane fade" role="tabpanel">
                            <div class="content add-event">
                                <?php if(empty($all_place)) { ?>
                                <p> No Active Place</p>
                                <?php } ?>
                                <?php foreach($all_place as $place) { 
                                $id = base64_encode($place['Place']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                <?php if(empty($place['Place']['place_image'])){?>
                                <?php echo $this->Html->image("calendaradd.png", array("alt"=>"logo"));?>
                                <?php } else { ?>
                                    <?php echo $this->Html->image($place['Place']['place_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                 <?php } ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $place['Place']['name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: <?php echo $place['User']['first_name'] ?></p>
                                        <p><i class="fa fa-location-arrow"></i> Location: <?php echo $place['Place']['city'];?></p>
                                        <p><i class="fa fa-phone"></i> Phone: <?php echo $place['Place']['phone_number'];?></p>
                                        <p><i class="fa fa-globe"></i> Website: <?php echo $place['Place']['website'];?></p>
                                        <p><i class="fa fa-user"></i> Activated By: <?php echo $_SESSION['Auth']['User']['first_name'];?>
                                        <?php if($place['Place']['place_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($place['Place']['place_status'] == '0') { ?>
                                       <a href='activate-place?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning">Activate</button></a>
           
                                       <?php } ?>
                                       <a href='deactivate-place?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Deactivate </button></a>
                                       <a href='delete-place?aid=<?php echo $id; ?>'><button type="button" onclick="if (confirm('Are you sure you want to delete this place ?')) { return true; } return false;" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>

            </div>
<script>
  jQuery(document).ready(function () {
  jQuery(".next_form").click(function () 
        {   var isValid = true;                     
            jQuery('.placehtml :input').each(function() 
            {          
            if(jQuery(this).val() ==""  && !$(this).hasClass('optional'))            
             {              
                isValid = false;
                jQuery(this).css({"border": "1px solid red","background": "#FFCECE"});
                 
                     
             }else {
				     error= jQuery('.error').text();
				     if(error !='')
				     {
						  isValid = false;
					 } 
			        jQuery(this).css({"border": "","background": ""});  
                    return true; 
                  }
                });
              return isValid;
             });
   jQuery(".fileupload").click(function ()  
    {
			var isValid = true;             
			                               
			if(jQuery('.fileup').val() =="" )            {              
			isValid = false;
			jQuery('.fileup').css({"border": "1px solid red","background": "#FFCECE"});
			}    
			else {
			jQuery('.fileup').css({"border": "","background": ""});  
			return true;
			}
			
			return isValid;
			});
 });          
         function isNumberKey(evt)
         {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
         return true;
        }
        function checkvalidemail(val)
        {
			
        if(val!='')
	{ 
		var emailID = val;
		atpos = emailID.indexOf("@");
		dotpos = emailID.lastIndexOf(".");
   if (atpos < 1 || ( dotpos - atpos < 2 )) 
   {    document.getElementById("error").style.display="block";		
		document.getElementById("error").innerHTML="Please enter correct email ID";		
       return false;
      }else{ 	document.getElementById("error").innerHTML="";	}
	 }
}
</script>
