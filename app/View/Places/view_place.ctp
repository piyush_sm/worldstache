<style>
      #map-canvas {
        width: 365px;
        height: 365px;
      }
    </style>
    <script>
      function initialize() {
        var geocoder= new google.maps.Geocoder();
        var mapCanvas = document.getElementById('map-canvas');
        var mapOptions = {
          center: new google.maps.LatLng(44.5403, -78.5463),
          zoom: 10,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI:true
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        var address = "<?php echo $place_detail['Place']['city'];?>";
        //var address = "Phase 3B2, Mohali, India";
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location

        });
      } else {
        alert("Geocode was not successful for the following reason: " + status);
      }
    });
      } 

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<div class="event-main">
                <div class="sld-main sld-detail view-place">
                    <!--<ul class="detail-slider">
                        <li><?php// echo $this->Html->image($place_detail['Place']['place_image']) ?> </li>
                    </ul>-->
                    <ul class="static-img">
                        <li><?php echo $this->Html->image($place_detail['Place']['place_image']) ?></li>
                    </ul>
                    <h3>
                        <?php $id = base64_encode($place_detail['Place']['id']);?>
                        <?php echo $place_detail['Place']['name'];?>
                    </h3>
                    <h5><?php echo $place_detail['Place']['city']?></h5>
                    <ul class="venue">
                        <li><i class="fa fa-phone"></i><?php echo $place_detail['Place']['phone_number'];?> </li>
                        <li><i class="fa fa-globe"></i><?php echo $this->Html->link("Website",$place_detail['Place']['website'],array('escape' => false, 'target'=>"_blank"));?>
                        <li><a href="#"><i class="fa fa-map-marker"></i>Couture</a>, <?php echo $place_detail['Place']['city']?></li>
                    </ul>
                    <p>
                        <?php echo $place_detail['Place']['place_description'];?>
                    </p>
                        <?php if($user_id != "" && $user_id != null) { ?>
    <div class="put-cmt">
        <span><?php echo $this->Html->image($user_data,array('width'=>'100','height'=>'100'));?></span>
        <form action="post-comment?eid=<?php echo $id;?>" method="post">
            <textarea name="data[Comment][comment]" class="form-control" placeholder="Comment here" readonly="readonly"></textarea>
            <input type="hidden" value="place" name="data[Comment][comment_type]">
            <input type="submit" value="Comment" disabled="disabled">
        </form>
    </div>
    <?php } ?>

        <?php if(!empty($all_comments) || $all_comments != null){ ?>
            <h4>Reviews</h4>
            <?php foreach($all_comments as $comment) { ?>
                <div class="review-main">
               <?php echo $this->Html->image($comment['User']['profile_image'],array('width'=>'50','height'=>'50'));?> <span><?php echo $comment['Comment']['comment'];?></span>
                </div>
               <?php } }?>
        </div>
    
                <div class="map-detail">
                    <div id="map-canvas"></div>
                    <?php if(!empty($place_detail['Place']['facebook_widget'])) { ?>
                    <div class="facebook-widget">
                        <?php echo $place_detail['Place']['facebook_widget'];?>
                    </div>
                    <?php } ?>
                </div>

            <div class="more-events">
                    <h3>More Upcoming Events...</h3>
                    <div class="slider1">
                        <?php foreach($upcoming_events as $events) { ?>
                        <?php $id = base64_encode($events['Event']['id'])?>
                        <div class="slide">
                        <?php echo $this->Html->link(
                            $this->Html->image($events['Event']['event_image'], array("alt" => "event_img")),"../events/event_detail?eid=".$id,array('escape' => false, 'target'=>'_blank'));?>
                        <?php echo $events['Event']['event_title'];?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>