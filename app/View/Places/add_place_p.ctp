<style>
      html, body, #map-canvas {
        height: 90%;
        margin: 0px;
        padding: 0px
      }
      .controls {
        margin-top: 16px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 280px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        position:relative;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        margin-top: 5px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
Logo	Sam

    Profile
    Promoter Page
    Google Analytics
    Events
    Artists
    Places
    Guestlist / Sold
    Set Payment Method

My Dashboard

    Your Promoter Page is Active
    Number of places : 0
    Number of Events : 0
    Number of Guestlist : 0

    View Places
    Create New Place

Logo
IT park

Posted By: Professional

Status: Pending
Logo
Panchkula

Posted By: Professional

Status: Pending
Logo
Chandigarh

Posted By: Professional

Status: Active

        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
        position: static !important;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
#cke_place_description {
    float: right;
}
    </style>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script>
function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(-33.8688, 151.2195),
    zoom: 13
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);

  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));

  var types = document.getElementById('type-selector');
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  // Sets a listener on a radio button to change the filter type on Places
  // Autocomplete.
  function setupClickListener(id, types) {
    var radioButton = document.getElementById(id);
    google.maps.event.addDomListener(radioButton, 'click', function() {
      autocomplete.setTypes(types);
    });
  }

  setupClickListener('changetype-all', []);
  setupClickListener('changetype-address', ['address']);
  setupClickListener('changetype-establishment', ['establishment']);
  setupClickListener('changetype-geocode', ['geocode']);
}

google.maps.event.addDomListener(window, 'load', initialize);

</script>

<div class="col-sm-9  col-md-9  main">
                <div data-example-id="togglable-tabs" role="tabpanel" class="bs-example bs-example-tabs">
                    <ul role="tablist" class="nav nav-tabs" id="myTab">
                        <li class="active" role="presentation"><a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#general">View Places</a></li>
                        <li role="presentation"><a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#create">Create New Place</a></li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                      <div aria-labelledby="home-tab" id="general" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                <?php if(empty($all_place)) { ?>
                                <p> No Place Created by You</p>
                                <?php } ?>
                      <?php foreach($all_place as $place) { 
                                $id = base64_encode($place['Place']['id']);
                                ?>
                                <div class="add-new-event adddish">
                                    <?php echo $this->Html->image($place['Place']['place_image'], array("width"=>"100px","height"=>"100px","alt" => "Logo")) ?>
                                    <div class="addnew-discription">
                                        <h4><?php echo $place['Place']['name']; ?></h4>
                                        <p><i class="fa fa-user"></i> Posted By: Professional</p>
                                        <?php if($place['Place']['place_status'] == '1') { ?> 
                                        <p><i class="fa fa-map-marker"></i> Status: Active </p>
                                        <?php } else { ?>
                                        <p><i class="fa fa-map-marker"></i> Status: Pending</p>
                                        <?php } ?>
                                    </div>
                                    <div class="adddel">
                                        <?php if($place['Place']['place_status'] == '0') { ?>
                                       <a href='../places/edit_place_p?aid=<?php echo $id; ?>'><button type="button" class="btn btn-warning"> Edit </button></a>
                                       <?php } ?>
                                       <a href='../places/delete_place_p?aid=<?php echo $id; ?>'><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>  
                        </div>
                        <div aria-labelledby="profile-tab" id="create" class="tab-pane fade" role="tabpanel">
                          <ul role="tablist" class="nav nav-tabs" id="myNewTab">
                            <li class="active" role="presentation"><a aria-controls="event" data-toggle="tab" id="event-tab" role="tab" href="#event">General</a></li>
                            <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Images</a></li>
                            </ul>
                        <div class="tab-content" id="myNewTabContent">
                        <div aria-labelledby="event-tab" id="event" class="tab-pane fade in active" role="tabpanel">
                            <div class="content add-event">
                                <?php echo $this->Form->Create('',array('id'=>'add_place','enctype'=>'multipart/form-data')) ?>
                                    <!--<h2 class="dash-header">Add Place </h2>-->
                                    <div class="edit-field">
                                       <label>Name</label>
                                       <?php echo $this->Form->input('name', array('id'=>'name','placeholder'=>'Name','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                        
                                       <?php echo $this->Form->input('city', array('id'=>'pac-input','placeholder'=>'Select Place','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <label>Map Location</label>
                                    <div class="addart-arm">
                            
                                     <div id="type-selector" class="controls">
                                    <input type="radio" name="type" id="changetype-all" checked="checked">
                                     <div id="map-canvas"></div>
                                     </div>
                                   </div>
                   
                                    <div class="edit-field">
                                        <label>Region</label>
                                        <?php echo $this->Form->input('region', array('id'=>'region','class'=>'form-control add-title', 'label'=>false , 'required'=>'', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field crt-vn">
                                        <label>Address
                                            <a class="addtooltip" data-original-title="6 Tags  maximum" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->textarea('address', array('id'=>'address','class'=>'form-control custom-control','required'=>'required', 'div'=>false)) ?>
                                    </div>

                                    <div class="edit-field">
                                       <label>Place Types</label>
                                       <?php echo $this->Form->input('place_types', array('id'=>'place_types','placeholder'=>'Place types','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field crt-dn">
                                        <label>Description
                                            <a class="addtooltip" data-original-title="6 Tags  maximum" href="#" data-toggle="tooltip" title=""><i class="fa fa-info-circle"></i></a>
                                        </label>
                                        <?php echo $this->Form->textarea('place_description', array('id'=>'place_description','class'=>'form-control custom-control ckeditor','required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Website</label>
                                       <?php echo $this->Form->input('website', array('id'=>'place_types','placeholder'=>'Website Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Phone Number</label>
                                       <?php echo $this->Form->input('phone_number', array('id'=>'phone_number','placeholder'=>'Phone Number','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    
                                    <div class="edit-field">
                                       <label>Email</label>
                                       <?php echo $this->Form->input('email', array('id'=>'email','placeholder'=>'Email','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                       <label>Twitter Url</label>
                                       <?php echo $this->Form->input('twitter_url', array('id'=>'email','placeholder'=>'Twitter Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                    <div class="edit-field">
                                       <label>Facebook Url</label>
                                       <?php echo $this->Form->input('facebook_url', array('id'=>'email','placeholder'=>'Facebook Url','class'=>'form-control', 'label'=>false , 'required'=>'required', 'div'=>false)) ?>
                                    </div>
                                  </div>
                                  <ul role="tablist" class="nav nav-tabs" id="myNewTab">                            
                                  <li role="presentation"><a aria-controls="images" data-toggle="tab" id="images-tab" role="tab" href="#images">Next</a></li>
                                  </ul>
                                </div>

                                <div aria-labelledby="images-tab" id="images" class="tab-pane fade" role="tabpanel">
                                  <div class="content add-event">
                                    <div class="add-new-event adddish">
                                    <?php echo $this->Html->image("calendaradd.png", array("alt"=>"event_img"));?>
                                    <div class="addnew-discription">
                                      <label> Place Image </label>
                                        <?php echo $this->Form->input('place_image', array('id'=>'place_image', 'type'=>'file', 'label'=>false ,'div'=>false)) ?>
                                    </div>
                                   </div>
                                  <div class="btn-group event-sub">
                                   <input type="submit" class="btn btn-warning" value="Save Place">
                                </div> 
                            </div>  
                        </div>
                         <?php echo $this->Form->end(); ?>
                      </div>
                    </div> 
                    </div>
                </div>

            </div>
