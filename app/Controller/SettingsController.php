<?php 
	
/* 
 Manages all Admin Seting Activities

 This controller uses Setting Model
*/


Class SettingsController extends AppController {

   public $components = array('Session', 'Cookie');
   public $helpers = array('Form', 'Html', 'Js', 'Time');    


	public function manage_banner() {
		
		/* Getting the previously saved banner settings */
		if($this->Auth->user('user_type') == 3) {
		$slider_settings = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));

		$data = unserialize($slider_settings['Setting']['banner_images']);
		//pr($data);die();
		$this->set('data',$data);

		$id = $slider_settings['Setting']['id'];
		if($this->request->is('post')) {
			
			if($this->request->data['Setting']['banner_type'] == 0) {

			$files = $this->request->data['Setting']['upload_image'];
			$imgs = $this->request->data['Setting']['image_url'];

			$slider_images['images'] = array();
			$slider_images['urls'] = array();
			//$images_urls = array();
			$i = 0;
			$j = 0;
			foreach($files as $key => $file) {
			
			$i= $i + 1;
				
			/* Moving the images */

			if(empty($file['name']) || $file['name'] == "") {
			
			$filename = $data['images'][$i-1];
			
			array_push($slider_images['images'], $filename);
			
			} else {
			$filename = $file['name'];

			move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/banner/'. basename($filename));
	
			/* Saving the image path in database */
			
			$this->request->data['Setting']['banner_image_'.$i] = '/images/banner/'.$filename;
			
			array_push($slider_images['images'], $this->request->data['Setting']['banner_image_'.$i]);

		}	

		   	}
			foreach($imgs as $key => $img){

			$j= $j + 1;
			if(empty($img[$j]) || $img[$j] == "") {

			$img_url = $data['urls'][$j-1];

			array_push($slider_images['urls'], $img_url);

			} else {

			$img_url = $img;
			array_push($slider_images['urls'], $img_url);
			}

		}
			//pr($slider_images); die();
			$this->Setting->id = $id;
			
			$this->request->data['Setting']['banner_images'] = serialize($slider_images);
			//$this->request->data['Setting']['images_urls'] = serialize($images_urls);
			
			$this->Setting->save($this->request->data);
			
			$this->Session->setFlash('Upload sucessfull');
			$this->redirect(array('controller'=>'settings', 'action'=>'manage_banner'));
		 
	} else {
		
	//	pr($this->request->data); die();
	if(!empty($this->request->data['Setting']['videos_url']))
		     {
				$str=$this->request->data['Setting']['videos_url'];
		       $strurl= explode("?v=",$str); 			
				$this->request->data['Setting']['videos_url'] = $strurl['1'];
			 }
		$this->Setting->id = $id;
		$this->Setting->save($this->request->data);

		$this->Session->setFlash('Banner Settings has been saved');
		$this->redirect(array('controller'=>'settings', 'action'=>'manage_banner'));
		}
	}
	    $this->layout = 'admin_layout';
            $this->render('manage_banner');
	    } else {
     	    $this->Session->setFlash("You cannot access this page");
     	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

	}

	
	public function manage_footer() {
		
		/* Get previously saved footer settings */
		if($this->Auth->user('user_type') == 3) {
         	$footer_settings = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));

         	$show_footer = $footer_settings['Setting']['show_footer'];
         	$this->set('show_footer',$show_footer);

		$data = unserialize($footer_settings['Setting']['footer_images']);

		$this->set('data',$data);
		
		$id = $footer_settings['Setting']['id'];			
		
			if($this->request->is('post')) {

			$files = $this->request->data['Setting']['upload_image'];
			$footer_images = array();
			$i = 0;
			foreach($files as $key => $file) {
			
			$i= $i + 1;
				
			/* Moving the images */

			if(empty($file['name']) && $file['name'] == "") {
			
			$filename = $data[$i-1];
			
			array_push($footer_images, $filename);
			
			} else {
			$filename = $file['name'];

			move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/footer/'. basename($filename));
	
			/* Saving the image path in database */
			
			$this->request->data['Setting']['footer_image_'.$i] = '/images/footer/'.$filename;
			
			array_push($footer_images, $this->request->data['Setting']['footer_image_'.$i]);

		}	

		   	}
			$this->Setting->id = $id;
			
			$this->request->data['Setting']['footer_images'] = serialize($footer_images);
			
			$this->Setting->save($this->request->data);
			
			$this->Session->setFlash('Upload sucessfull');
			$this->redirect(array('controller'=>'settings', 'action'=>'manage_footer'));
		 }

			$this->layout = 'admin_layout';
		    	$this->render('manage_footer');
			} else {
	     	    $this->Session->setFlash("You cannot access this page");
	     	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	}


	public function manage_slider() {
		
		/* Get previously saved slider settings */
		if($this->Auth->user('user_type') == 3) {
         	$slider_settings = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));

		$data = unserialize($slider_settings['Setting']['slider_images']);
		
		$this->set('data',$data);

		$id = $slider_settings['Setting']['id'];			
		
			if($this->request->is('post')) {
			
			$files = $this->request->data['Setting']['upload_image'];
			$imgs = $this->request->data['Setting']['image_url'];

			$slider_images['images'] = array();
			$slider_images['urls'] = array();
			//$images_urls = array();
			$i = 0;
			$j = 0;
			foreach($files as $key => $file) {
			
			$i= $i + 1;
				
			/* Moving the images */

			if(empty($file['name']) || $file['name'] == "") {
			
			$filename = $data['images'][$i-1];
			
			array_push($slider_images['images'], $filename);
			
			} else {
			$filename = $file['name'];

			move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/slider/'. basename($filename));
	
			/* Saving the image path in database */
			
			$this->request->data['Setting']['slider_image_'.$i] = '/images/slider/'.$filename;
			
			array_push($slider_images['images'], $this->request->data['Setting']['slider_image_'.$i]);

		}	

		   	}
			foreach($imgs as $key => $img){

			$j= $j + 1;
			if(empty($img[$j]) || $img[$j] == "") {

			$img_url = $data['urls'][$j-1];

			array_push($slider_images['urls'], $img_url);

			} else {

			$img_url = $img;
			array_push($slider_images['urls'], $img_url);
			}

		}
			//pr($slider_images); die();
			$this->Setting->id = $id;
			
			$this->request->data['Setting']['slider_images'] = serialize($slider_images);
			//$this->request->data['Setting']['images_urls'] = serialize($images_urls);
			
			$this->Setting->save($this->request->data);
			
			$this->Session->setFlash('Upload sucessfull');
			$this->redirect(array('controller'=>'settings', 'action'=>'manage_slider'));
		 }

			$this->layout = 'admin_layout';
		    	$this->render('manage_slider');
			} else {
	     	    $this->Session->setFlash("You cannot access this page");
	     	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	}
	
	public function social_media() {
		
		/* Get previously saved slider settings */
		if($this->Auth->user('user_type') == 3) {
			
        $slider_settings = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));

		$facebook_page = $slider_settings['Setting']['facebook_page'];
		$this->set('facebook_page',$facebook_page);
		
		$twitter_page = $slider_settings['Setting']['twitter_page'];
		$this->set('twitter_page',$twitter_page);
		
		$youtube_channel = $slider_settings['Setting']['youtube_channel'];
		$this->set('youtube_channel',$youtube_channel);
		
		$instagram_page = $slider_settings['Setting']['instagram_page'];
		$this->set('instagram_page',$instagram_page);

		$id = $slider_settings['Setting']['id'];			
		
			if($this->request->is('post')) {
			
			$this->Setting->id = $id;
			
			$this->Setting->save($this->request->data);
			
			$this->Session->setFlash('Media saved sucessfully');
			$this->redirect(array('controller'=>'settings', 'action'=>'social_media'));
		 }

			$this->layout = 'admin_layout';
		    $this->render('social_media');
			} else {
	     	    $this->Session->setFlash("You cannot access this page");
	     	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
		
		}


		public function static_pages() {
		
		/* Get previously saved slider settings */
		if($this->Auth->user('user_type') == 3) {
			
        $slider_settings = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));

		$facebook_page = $slider_settings['Setting']['facebook_page'];
		$this->set('facebook_page',$facebook_page);
		
		$twitter_page = $slider_settings['Setting']['twitter_page'];
		$this->set('twitter_page',$twitter_page);
		
		$youtube_channel = $slider_settings['Setting']['youtube_channel'];
		$this->set('youtube_channel',$youtube_channel);
		
		$instagram_page = $slider_settings['Setting']['instagram_page'];
		$this->set('instagram_page',$instagram_page);

		$id = $slider_settings['Setting']['id'];

		if($this->request->is('post'))
		{
			//pr($this->request->data);

			$page_selec = $this->request->data['Setting']['page_selected'];
			$page_content = $this->request->data['Setting']['page_content'];

			//echo $page_selec; die();

			$this->request->data['Setting'][$page_selec] = $page_content;
			//pr($this->request->data); die();
			$this->Setting->id = $id;
			if($this->Setting->save($this->request->data)){
			$this->Session->setFlash('Page content updated succesfully');
			$this->redirect(array('controller'=>'settings', 'action'=>'static_pages'));
			}
		}

			$this->layout = 'admin_layout';
		    $this->render('static_pages');
			} else {
	     	    $this->Session->setFlash("You cannot access this page");
	     	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
		
		}
		
		public function contains($substring, $string) {
			$pos = strpos($string, $substring);
	 
			if($pos === false) {
					
				   return false;
			}
			else {
					
					return true;
			}	 
}
	
}

?>
