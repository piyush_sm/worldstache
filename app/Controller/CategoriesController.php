<?php 
	
/* 

 This controller uses Category Model

*/


Class CategoriesController extends AppController {

   public $components = array('Session', 'Cookie','Paginator');
   public $helpers = array('Form', 'Html', 'Js', 'Time');
   public $paginate = array(
        'limit' => 10
        ); 

   public function add_category() {
	
	if($this->Auth->user('user_type') == 3){
    
	$all_categories = $this->Category->find('all');
	
	$this->set('all_categories',$all_categories);

	if($this->request->is('post')) {

	$filename = $this->request->data['Category']['category_image']['name'];

	// Move the image
	
	move_uploaded_file( $this->request->data['Category']['category_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/category/'. basename($filename));

	// Set the image path in the database

	$this->request->data['Category']['category_image'] = '/images/category/'.$filename;
	if($this->Category->save($this->request->data)){

	$this->Session->setFlash("Category has been saved successfully");
	$this->redirect(array('controller'=>'categories', 'action'=>'add_category'));

	} else {

	$this->Session->setFlash("Opps!!.. Category not saved.Please try again ");
	} 
		
     }
     $this->layout = 'admin_layout';
     $this->render("add_category");
     } else {
     $this->Session->setFlash("You cannot access this page");
     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
  }


    public function edit_category($id=null){

	if($this->Auth->user('user_type') == 3){
	$id = base64_decode($_GET['cid']);

	$category = $this->Category->findById($id);
	$this->set('category', $category);

	$all_categories = $this->Category->find('all');
	$this->set('all_categories',$all_categories);

 	if($this->request->is('post','put')){
	
	$filename = $this->request->data['Category']['category_image']['name'];
	if($filename == ""){
		$this->request->data['Category']['category_image'] = $category['Category']['category_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Category']['category_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/category/'. basename($filename));
	// Set the image path in the database

	$this->request->data['Category']['category_image'] = '/images/category/'.$filename;
	}
	$this->Category->id = $id;	
	if($this->Category->save($this->request->data)){

	$this->Session->setFlash("Category has been updated successfully");
	$this->redirect(array('controller'=>'categories', 'action'=>'add_category'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 
	
	}
	$this->layout = 'admin_layout';
	$this->render('edit_category');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

 }

	public function delete_category ($id=null) {

	if($this->Auth->user('user_type') == 3){
	$id = base64_decode($_GET['cid']);

	$this->Category->id = $id;
        if($this->Category->delete()){
	
	$this->Session->setFlash("Category has been deleted succesfully");
	$this->redirect(array('controller'=>'categories','action'=>'add_category'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'categories','action'=>'add_category'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function categories_list() {


	$all_categories = $this->Category->find('all', array('conditions'=> array('NOT'=>array('id'=>'111')),
	    'order' => 'id DESC'
	));

	/*$all_cat_images = $this->Category->find('all', array('fields'=>array('category_image'),'conditions'=> array('NOT'=>array('id'=>'111')),
	    'order' => 'id DESC'
	));*/

	$all_cat_images = $this->Category->query('SELECT `id`,`category_image` FROM categories WHERE id != "111"');

	$this->set('title_for_layout','All Categories | Worldstache');
	$this->set('all_categories',$all_categories);
	$this->set('all_cat_images',$all_cat_images);

	//pr($all_cat_images); die();
	$this->render('categories_list');
	
	}

	
}

?>
