<?php 
	
/* 
 This controller uses Subcategory Model
*/

Class BlogsController extends AppController {

	public $components = array('Session','Cookie');
	public $helpers = array('Form','Html','Js','Time');


    public function add_blog() {
	
	//pr($this->request->data); die();
	if($this->Auth->user('user_type') == 3){
	$all_posts = $this->Blog->find('all', array(
	    'conditions' => array(
		'post_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_posts',$all_posts);
	
	if($this->request->is('post')) {

	$filename = $this->request->data['Blog']['post_image']['name'];
	// Move the image	
	move_uploaded_file( $this->request->data['Blog']['post_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/blog/'. basename($filename));

	// Set the image path in the database
	$this->request->data['Blog']['post_image'] = '/images/blog/'.$filename;

	
	 	if($this->Blog->save($this->request->data)){

		$this->Session->setFlash('Post has been saved successfully','default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'blogs', 'action'=>'add_blog'));

		} else {

		$this->Session->setFlash('Opps!!.. Not saved. Try again later');
	
		}
	
   	}
	$this->layout = 'admin_layout';
	$this->render('add_blog');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


   public function edit_blog() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['bid']);

	$post = $this->Blog->findById($id);
	$this->set('post', $post);
	
	$all_posts = $this->Blog->find('all');	
	$this->set('all_posts',$all_posts);

	if($this->request->is('post','put')) {
	
	$filename = $this->request->data['Blog']['post_image']['name'];
	if($filename == ""){
		$this->request->data['Blog']['post_image'] = $post['Blog']['post_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Blog']['post_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/blog/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Blog']['post_image'] = '/images/blog/'.$filename;
	}
	$this->Blog->id = $id;	
	if($this->Blog->save($this->request->data)){

	$this->Session->setFlash("Blog post has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'blogs', 'action'=>'add_blog'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'admin_layout';
	$this->render('edit_blog');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function delete_blog($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['bid']);

	$this->Blog->id = $id;
        if($this->Blog->delete()) {
	
	$this->Session->setFlash("Sub-Category has been deleted succesfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'blogs','action'=>'add_blog'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'blogs','action'=>'add_blog'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	
	public function deactivate_bolg() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['bid']);
	
	
	$this->Blog->id = $id;
	
	if($this->Blog->saveField('post_status', 0)){

	$this->Session->setFlash("Post has been deactivated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'blogs', 'action'=>'add_blog'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_blog');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	
	}
	
	
	public function activate_bolg() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['bid']);
	
	
	$this->Blog->id = $id;
	
	if($this->Blog->saveField('post_status', 1)){

	$this->Session->setFlash("Post has been activated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'blogs', 'action'=>'add_blog'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_blog');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	
	}
	
	public function blog_list() {

	$all_blogs = $this->Blog->find('all', array(
	    'conditions' => array(
		'post_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_blogs',$all_blogs);
	$this->render('blog_list');
	}

	public function blog_detail() {

	$id = base64_decode($_GET['bid']);
	$blog_detail = $this->Blog->find('first', array(
	    'conditions' => array(
		'id'=>$id
	    ),
	    'order' => 'id DESC'
	));
	$this->set('blog_detail',$blog_detail);
	$this->render('blog_detail');
	
	}



}  


?>
