<?php 
	
/* 

 This controller uses Option Model

*/
 App::uses('ClassRegistry', 'Utility');
 Class OptionsController extends AppController {

	public $components = array('Session','Cookie');
	public $helpers = array('Form','Html','Js','Time');



	public function add_option() {

	if($this->Auth->user('user_type') == 3) {
	$all_options = $this->Option->find('all');
	$this->set('all_options',$all_options);

	if($this->request->is('post')){


		if($this->Option->save($this->request->data)) {

		$this->Session->setFlash("Option is succesfully saved");
		$this->redirect(array('controller'=>'options', 'action'=>'add_option'));

		} else {

       		$this->Session->setFlash("Opps!!... Information Not saved Try Again later");

		}
	
     }
    
	$this->layout = 'admin_layout';
	$this->render('add_option');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
}

	public function edit_option($id=null) {

		if($this->Auth->user('user_type') == 3) {
		$id = base64_decode($_GET['oid']);

		$option = $this->Option->findById($id);
		$this->set('option', $option);

		$all_options = $this->Option->find('all');
		$this->set('all_options',$all_options);

	 	if($this->request->is('post','put')){

		$this->Option->id = $id;	
		if($this->Option->save($this->request->data)){

		$this->Session->setFlash("Option has been updated successfully");
		$this->redirect(array('controller'=>'options', 'action'=>'add_option'));

		} else {
		$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
		} 
	
		}
		$this->layout = 'admin_layout';
		$this->render('edit_option');
		} else {
	     	$this->Session->setFlash("You cannot access this page");
	     	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	}
		public function delete_option ($id=null) {

		if($this->Auth->user('user_type') == 3) {
		$id = base64_decode($_GET['oid']);

		$this->Option->id = $id;
		if($this->Option->delete()){
	
		$this->Session->setFlash("You have successfully deleted option");
		$this->redirect(array('controller'=>'options','action'=>'add_option'));

		} else {

		$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
		$this->redirect(array('controller'=>'options','action'=>'add_option'));
		}
		$this->Session->setFlash("You cannot access this page");
	     	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	

		}


}
?>
