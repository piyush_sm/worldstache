<?php 
	
/* 

 This controller uses Promoter Model

*/

Class PromotersController extends AppController {


    public function add_promoter() {
	
	if($this->Auth->user('user_type') == 3) {

	$all_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_promoters',$all_promoters);

	$pending_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '0'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('pending_promoters',$pending_promoters);

	if($this->request->is('post')) {
	
	$this->request->data['Promoter']['user_id'] = $this->Auth->user('id');

	/* Process event images here */
	$filename = $this->request->data['Promoter']['header_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['header_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['event_image'] = '/images/event/'.$filename;

	$filename = $this->request->data['Promoter']['logo_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['logo_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['logo_image'] = '/images/event/'.$filename;

	$filename = $this->request->data['Promoter']['tile_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['tile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['tile_image'] = '/images/event/'.$filename;
	/*Event images processing ends here */
	if($this->Promoter->save($this->request->data)) {
	$this->Session->setFlash("Page has been saved successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter'));
	} else {

	$this->Session->setFlash("Opps!!.. Event could not be saved. Please try again later");
	}
   	}
	$this->layout = 'admin_layout';
	$this->render('add_promoter');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


   public function edit_promoter() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['eid']);
	
	$all_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_promoters',$all_promoters);

	$pending_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '0'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('pending_promoters',$pending_promoters);
	
	$promoter = $this->Promoter->findById($id);
	$this->set('promoter', $promoter);

	if($this->request->is('post','put')) {

	$filename = $this->request->data['Promoter']['header_image']['name'];
	if($filename == ""){
		$this->request->data['Promoter']['header_image'] = $promoter['Promoter']['header_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['header_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Promoter']['header_image'] = '/images/promoter/'.$filename;
	}
	
	$filename_cover = $this->request->data['Promoter']['cover_image']['name'];
	if($filename_cover == ""){
		$this->request->data['Promoter']['cover_image'] = $promoter['Promoter']['cover_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename_cover));
	// Set the image path in the database
	$this->request->data['Promoter']['cover_image'] = '/images/promoter/'.$filename_cover;
	}
	
	$filename_tile = $this->request->data['Promoter']['tile_image']['name'];
	if($filename_tile == ""){
		$this->request->data['Promoter']['tile_image'] = $promoter['Promoter']['tile_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['tile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename_tile));
	// Set the image path in the database
	$this->request->data['Promoter']['tile_image'] = '/images/promoter/'.$filename_tile;
	}
	
	$this->Promoter->id = $id;	
	if($this->Promoter->save($this->request->data)){

	$this->Session->setFlash("Page has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'promoters', 'action'=>'add_promoter'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'admin_layout';
	$this->render('edit_promoter');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function delete_promoter($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['eid']);

	$this->Promoter->id = $id;
    if($this->Promoter->delete()) {
	
	$this->Session->setFlash("Page has been deleted succesfully");
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function activate_promoter() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['eid']);
	
	
	$this->Promoter->id = $id;
	
	if($this->Promoter->saveField('page_status', 1)){

	$this->Session->setFlash("Page has been activated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events', 'action'=>'add_event'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_promoter');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}

	public function add_promoter_p() {

	if($this->Auth->user('user_type') == 1) {
	
	$all_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_promoters',$all_promoters);

	if($this->request->is('post')) {
	
	$this->request->data['Promoter']['user_id'] = $this->Auth->user('id');

	/* Process Promoter page images here */
	$filename = $this->request->data['Promoter']['header_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['header_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['header_image'] = '/images/promoter/'.$filename;

	$filename = $this->request->data['Promoter']['cover_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['cover_image'] = '/images/promoter/'.$filename;

	$filename = $this->request->data['Promoter']['tile_image']['name'];
	move_uploaded_file( $this->request->data['Promoter']['tile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	$this->request->data['Promoter']['tile_image'] = '/images/promoter/'.$filename;
	/* Promoter page images processing ends here */
	if($this->Promoter->save($this->request->data)) {
	$this->Session->setFlash("Page has been saved successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter_p'));
	} else {

	$this->Session->setFlash("Opps!!.. Page could not be saved. Please try again later");
	}
   	}
	$this->layout = 'dashboard_layout';
	$this->render('add_promoter_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

	}

	public function edit_promoter_p() {

	if($this->Auth->user('user_type') == 1) {	
	$id = base64_decode($_GET['eid']);
	
	$all_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_promoters',$all_promoters);

	$pending_promoters = $this->Promoter->find('all', array(
	    'conditions' => array(
		'page_status' => '0'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('pending_promoters',$pending_promoters);
	
	$promoter = $this->Promoter->findById($id);
	$this->set('promoter', $promoter);

	if($this->request->is('post','put')) {

	$filename = $this->request->data['Promoter']['header_image']['name'];
	if($filename == ""){
		$this->request->data['Promoter']['header_image'] = $promoter['Promoter']['header_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['header_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Promoter']['header_image'] = '/images/promoter/'.$filename;
	}
	
	$filename_cover = $this->request->data['Promoter']['cover_image']['name'];
	if($filename_cover == ""){
		$this->request->data['Promoter']['cover_image'] = $promoter['Promoter']['cover_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename_cover));
	// Set the image path in the database
	$this->request->data['Promoter']['cover_image'] = '/images/promoter/'.$filename_cover;
	}
	
	$filename_tile = $this->request->data['Promoter']['tile_image']['name'];
	if($filename_tile == ""){
		$this->request->data['Promoter']['tile_image'] = $promoter['Promoter']['tile_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Promoter']['tile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/promoter/'. basename($filename_tile));
	// Set the image path in the database
	$this->request->data['Promoter']['tile_image'] = '/images/promoter/'.$filename_tile;
	}
	
	$this->Promoter->id = $id;	
	if($this->Promoter->save($this->request->data)){

	$this->Session->setFlash("Page has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'promoters', 'action'=>'add_promoter_p'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'dashboard_layout';
	$this->render('edit_promoter_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

	}
	
	public function delete_promoter_p($id=null) {

	if($this->Auth->user('user_type') == 1) {
	$id = base64_decode($_GET['eid']);

	$this->Promoter->id = $id;
        if($this->Promoter->delete()) {
	$this->Session->setFlash("Page has been deleted succesfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter_p'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'promoters','action'=>'add_promoter_p'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}

}  


?>
