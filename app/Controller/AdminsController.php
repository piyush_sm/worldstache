<?php 
	
/* 

 Manages all Admin Activities

 This controller uses Admin Model

*/


Class AdminsController extends AppController {

   public $components = array('Session', 'Cookie');
   public $helpers = array('Form', 'Html', 'Js', 'Time');    

   public function index() {
	
	
	//$this->layout = "/admin_layouts/admin_layout";
	$this->render("index");

   }
   public function sign_up() {

	if($this->request->is('post')) {

		pr($this->request->data);
		die();

	/* Save admin user */
		
	}

     $this->render("sign_up");

  }


    public function sign_in(){

 	if($this->request->is('post')){

		pr($this->request->data);
	
	$username = $this->request->data['Admin']['username'];
	$password = $this->request->data['Admin']['password'];
	
	}
	/* Authenticate admin login  */

	

	/* if login succesfull write admin session */


	$this->render('sign_in');

 }


	public function manage_banner() {


	if($this->request->is('post')){

		//pr($this->request->data['Admin']['upload_image']);
		//die();

		$files = $this->request->data['Admin']['upload_image'];

	foreach($files as $key => $file) {


	// Move the image

		$filename = $file['name'];

		move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstash/app/webroot/images/banner/'. basename($filename));


	// Set the image path in the database

        $this->request->data['Admin']['banner_image'] = '/images/banner/'.$filename;

	$this->Admin->update($this->request->data);	
	   }
	$this->Session->setFlash('Upload sucessfull');
	}

	$this->layout = 'admin_layout';
        $this->render('manage_banner');

	}
	
}

?>
