<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
 
 
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;
	
		/* Disabling the layout */

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$session = $this->Session->check('Auth.User');
		
		$this->loadModel('Blog');
		
		$all_posts = $this->Blog->find('all', array(
	    'conditions' => array(
		'post_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_posts',$all_posts);
/*	App::import('Vendor', 'google-outh');	
			$google_client_id = '371737360541-uq9nmeqhp1uoisdh2oth3p753gg2p6d9.apps.googleusercontent.com';
			$google_client_secret = 'rP9tJ3vvYuYVTUn6mYn25Itf';
			$google_redirect_url = 'http://dev.staplelogic.in/worldstashdev/';
			$google_developer_key = 'AIzaSyDyWnFnADDJoIw13NgBJHqnyqp9GaQZ0-U';
			App::import("Vendor", "google-outh/src/Google_Client");
			App::import("Vendor", "google-outh/src/contrib/Google_Oauth2Service");
			$gClient = new Google_Client();
			$gClient->setApplicationName('Login to Google');
			$gClient->setClientId($google_client_id);
			$gClient->setClientSecret($google_client_secret);
			$gClient->setRedirectUri($google_redirect_url);
			$gClient->setDeveloperKey($google_developer_key);
            $google_oauthV2 = new Google_Oauth2Service($gClient);
			if (isset($_REQUEST['reset']))
			{
			$this->set('msg', 'Logout');

			$this->Session->delete('token');
			$gClient->revokeToken();
			header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
			}
			if (isset($_REQUEST['code']))
			{
			$gClient->authenticate($_REQUEST['code']);
			$this->Session->write('token', $gClient->getAccessToken());
			$this->redirect(filter_var($google_redirect_url, FILTER_SANITIZE_URL), null, false);
			//header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
			return;
			}

			if ($this->Session->read('token'))
			{
			$gClient->setAccessToken($this->Session->read('token'));
			}
			if ($gClient->getAccessToken())
			{		
			$user = $google_oauthV2->userinfo->get();
			$user_id = $user['id'];
			$user_name = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
			$email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			//$profile_url = filter_var($user['link'], FILTER_VALIDATE_URL);
			$profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
			$personMarkup = "$email<div><img src='$profile_image_url?sz=50'></div>";
			$this->Session->write('token', $gClient->getAccessToken());
			}
			else
			{

			$authUrl = $gClient->createAuthUrl();
			}

			if(isset($authUrl)) 
			{
			$this->set('authUrl', $authUrl);
			}
					else // user logged in
					{ $this->loadModel('User');
					$result = $this->User->find('count', array('conditions' => array('email' => $email)));
					if($result > 0)
					{
						$msg = 'Welcome back '.$user_name.'!<br />';
						$msg .= '<br />';
						$msg .= '<img src="'.$profile_image_url.'" width="100" align="left" hspace="10" vspace="10" />';
						$msg .= '<br />';
						$msg .= '&nbsp;Name: '.$user_name.'<br />';
						$msg .= '&nbsp;Email: '.$email.'<br />';
						$msg .= '<br />';
						$this->set('msg', $msg);
					}
					else
					{
						pr($user); die('here');
					$msg1 = 'Hi '.$user_name.', Thanks for Registering!';
					$msg1 .= '<br />';
					$msg1 .= '<img src="'.$profile_image_url.'" width="100" align="left" hspace="10" vspace="10" />';
					$msg1 .= '<br />';
					$msg1 .= '&nbsp;Name: '.$user_name.'<br />';
					$msg1 .= '&nbsp;Email: '.$email.'<br />';
					$msg1 .= '<br />';
					$this->set('msg', $msg1);
					$this->User->query("INSERT INTO users (first_name, email) VALUES ('$user_name', '$email')");
			}
} */

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
		
	}

}
