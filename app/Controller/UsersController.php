<?php 
	
/* 
 This controller uses User Model
*/
App::uses('Controller', 'Controller');
App::uses('CakeEmail', 'Network/Email');
 
Class UsersController extends AppController {

	
	/* Function to Register Users */
	public function register() {
		
		if($this->request->is('post')):

		/* Create Token using form data and random number to ensure its unique and cannot be replicated */
		
		$hash=sha1($this->request->data['User']['first_name'].rand(0,100));
		
		$this->request->data['User']['token_hash']=$hash;
		
			if($this->User->validates()):
			
			/* Save all form data including the tokenhash */
			$this->request->data['User']['date_of_birth'] = $this->request->data['User']['birthday_day']."-".$this->request->data['User']['birthday_month']."-".$this->request->data['User']['birthday_year'];
			
				if($this->User->save($this->request->data)):
				echo 'http://dev.staplelogic.in/worldstashdev/users/verify/t:'.$hash.'/n:'.$this->request->data['User']['email'].'';
				die();
				$ms = "";
				$ms.='http://dev.staplelogic.in/worldstashdev/users/verify/t:'.$hash.'/n:'.$this->request->data['User']['email'].'';
					
					$ms=wordwrap($ms,70);
					
					$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array('piyushrana609@gmail.com' => 'Worldstache'))
						->to($this->request->data['User']['email'])
						->subject('Confirm Registration for Worldstash')
						->viewVars(array('name' => $this->request->data['User']['first_name'],
								'email' => $this->request->data['User']['email'],
                                'confirm_code' => $ms
						))
						->send();
				
					/* $this->request->data['Userdetail']['user_id'] = $this->User->id;
					$this->request->data['Userdetail']['firstname'] = $this->request->data['User']['firstname'];
					$this->request->data['Userdetail']['lastname'] = $this->request->data['User']['lastname'];
					
					$this->User->Userdetail->save($this->request->data); */
					
					$this->redirect(array(
					'controller' => 'users', 'action' => 'emailconfirmation', '?' => array(
							'email' => base64_encode($this->request->data['User']['email'])
					))
					);
				endif;
			endif;
		endif;
        $this->render('signup');
	}


	public function verify() {
	
	/* check if the token is valid */
		
		if (!empty($this->passedArgs['n']) && !empty($this->passedArgs['t']))
		{
			$args = $this->passedArgs['n'];
			$tokenhash = $this->passedArgs['t'];
			$results = $this->User->findByEmail($args);
			
			/* check if the user is already activated */
			if ($results['User']['status']==0)
			{
				/* check the token */

				if($results['User']['token_hash']==$tokenhash)
				{
					/* Set user status to 1 */
					$email = $results['User']['email'];
					
					$status=1;
					/* Update the user status */
					$this->User->updateAll(
						array('status' => $status),
						array('email' => $email)
					);
					$this->Session->setFlash('Your registration is complete');
					$this->redirect(array('controller'=>'pages', 'action'=>'display'));
				}
				else
				{
					$this->Session->setFlash('Your registration failed please try again');
					$this->redirect(array('controller'=>'pages', 'action'=>'display'));
				}
			}
		}
		
	}

	public function login() {
		
		if ($this->request->is('post')) {
		
		    if ($this->Auth->login()) {
				
					if ($this->Auth->user('status') == 1 && $this->Auth->user('user_type') != 3) {

						$this->redirect(array('controller'=>'pages','action'=>'display'));
						}
					elseif($this->Auth->user('user_type') == 3) {
						$this->redirect(array('controller'=>'users','action'=>'admin_dashboard'));
						}
					else {
						$this->Session->setFlash('Your account is not activated');
						}
		    } else {
		        $this->Session->setFlash('Your Username/Password combination was incorrect');
		    }
		}
		$this->redirect(array('controller'=>'pages','action'=>'display'));
	    }

	public function logout() {
		$this->Session->destroy();
		$this->Session->destroy('token');
		$this->redirect($this->Auth->logout());
	    }


	public function fblogin() {
		if($this->request->is('post')) {

		$check_email = $this->User->checkEmail($_POST['email']);
		//pr($check_email); die();
		if(!empty($check_email['User']['id'])) {
		$this->Session->write('Auth.User',$this->request->data);
		//$this->Session->write('Auth',$this->request->data['User']);
		return true;
		} else {
		$this->request->data['User']['first_name'] = $_POST['first_name'];
		$this->request->data['User']['last_name'] = $_POST['last_name'];
		$this->request->data['User']['email'] = $_POST['email'];
		$this->request->data['User']['status'] = 1;

		$this->User->save($this->request->data);
		$this->Session->write('Auth.User',$this->request->data);
		 }
		}
		
	}

	
	public function fbposts() {

	if($this->request->is("post")){
	$fb_data = $_POST['data'];
	$this->set('fb_data',$fb_data);


	foreach($fb_data as $data) {


	//post code here 

		}
	}
	}


	public function dashboard() {
	
	$user_type = $this->Auth->user('user_type');
	$this->set('user_type', $user_type);

	$user_detail = $this->Auth->user();
	$this->set('user_detail', $user_detail);
	
	/*$this->loadModel('Event');
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_events',$all_events);
	
	$this->loadModel('Artist');
	$all_artist = $this->Artist->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_artist',$all_artist); */
	$this->layout = "dashboard_layout";
	$this->render("dashboard");

	}

	public function admin_dashboard() {
	
	if($this->Auth->user('user_type') == 3) {
		//pr($this->Auth->user());
	$this->set('title_for_layout','Admin Dashboard');

	$all_users = $this->User->find('count', array(
	    'conditions' => array(
			'user_type' => '2',
			'status' => '1',
			'request_professional'=>'1'
		    )
	));
	$this->set('all_users',$all_users);

	$this->loadModel('Event');
	$all_events = $this->Event->find('count', array(
	    'conditions' => array(
		'event_status' => 0,
	    )
	));
	$this->set('all_events',$all_events);

	$this->loadModel('Artist');
	$all_artist = $this->Artist->find('count', array(
	    'conditions' => array(
		'artist_status' => 0,
	    )
	));
	$this->set('all_artist',$all_artist);

	$this->loadModel('Place');
	$all_place = $this->Place->find('count', array(
	    'conditions' => array(
		'place_status' => 0,
	    )
	));
	$this->set('all_place',$all_place);

	$this->loadModel('Category');
	$all_category = $this->Category->find('count', array(
	    'conditions' => array(
		'category_status' => 0,
	    )
	));
	$this->set('all_category',$all_category);

	$this->loadModel('Subcategory');
	$all_subcategory = $this->Subcategory->find('count', array(
	    'conditions' => array(
		'subcategory_status' => 0,
	    )
	));
	$this->set('all_subcategory',$all_subcategory);

	$this->layout = "admin_layout";
	$this->render("admin_dashboard");
	} else {
	     $this->Session->setFlash("You cannot access this page",'default', array('class'=>'btn-success success_msg'));
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

	}

	public function my_profile() {
	
	if($this->Auth->user('user_type') == 3){
		$this->layout = 'admin_layout';
	} else {
		$this->layout = 'dashboard_layout';
	}
	$this->render("my_profile");
	}

	public function edit_profile() {

	$data = $this->Session->read('Auth.User');
	$this->set('data', $data);

	$id = $data['id'];
	if($this->request->is('post','put')){

	$this->User->id = $id;
	if($this->User->save($this->request->data)){

	$this->Session->setFlash("Your profile has been updated");
	$this->redirect(array('controller'=>'users','action'=>'dashboard'));

	} else {

	$this->Session->setFlash("Problem updating data. Please try after some time");
	$this->redirect(array('controller'=>'users','action'=>'edit_profile'));
	}

        }
    if($this->Auth->user('user_type') == 3){
		$this->layout = 'admin_layout';
	} else {
		$this->layout = 'dashboard_layout';
	}
	$this->render = "edit_profile";


	}
	
	public function users_list() {

	if($this->Auth->user('user_type') == 3) {
	
	$all_users = $this->User->find('all', array(
	    'conditions' => array(
			'user_type' => '2',
			'status' => '1'
		    ),
	    'order' => 'User.id DESC'
	));
	$this->set('all_users',$all_users);
	$all_professionals = $this->User->find('all', array(
	    'conditions' => array(
			'user_type' => '1',
			'status' => '1'
		    ),
	    'order' => 'User.id DESC'
	));
	$this->set('all_professionals',$all_professionals);
    $this->layout = 'admin_layout';
	$this->render('users_list');
 	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function my_account() {
		
	$user_type = $this->Auth->user('user_type');
	$this->set('user_type', $user_type);

	$user = $this->User->find('first', array(
	    'conditions' => array(
		'User.id' => $this->Auth->user('id'),
	    )
	));
	$this->set('user',$user);
	if($this->request->is('post')){
		//pr($this->request->data);
		//die();
		$id = $user['User']['id'];

		$filename = $this->request->data['User']['profile_image']['name'];
		if($filename == ""){
		$this->request->data['User']['profile_image'] = $user['User']['profile_image'];
		} else {
		move_uploaded_file( $this->request->data['User']['profile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/user/'. basename($filename));
		$this->request->data['User']['profile_image'] = '/images/user/'.$filename;
		}


		$this->User->id = $id;
		if($this->User->save($this->request->data)){
		 $this->Session->setFlash("Information is updated successfully",'default', array('class'=>'btn-success success_msg'));
	     $this->redirect(array('controller'=>'users', 'action'=>'my_account'));
		} else {
		 $this->Session->setFlash("Some problem updating information. Please try again later",'default', array('class'=>'btn-success success_msg'));
		 $this->redirect(array('controller'=>'users', 'action'=>'my_account'));
		}
	}
	
    if($this->Auth->user('user_type') == 3){
		$this->layout = 'admin_layout';
	} else {
		$this->layout = 'dashboard_layout';
	}
	$this->render('my_account');

	}

	public function change_password() {
	
	if($this->request->is('post')){
		//pr($this->request->data);
		//die();
		$user = $this->User->find('first', array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
		$id = $user['User']['id'];
		if($user['User']['password'] == $this->Auth->password($this->request->data['User']['old_password'])){

				if($this->request->data['User']['password'] == $this->request->data['User']['confirm_password']){

					//$this->request->data['User']['password'] = $this->Auth->password($this->request->data['User']['password']);
	
					$this->User->id = $id;
					if($this->User->saveField('password',$this->request->data['User']['password'])){
						$this->Session->setFlash("Password is updated succesfully",'default', array('class'=>'btn-success success_msg'));
						$this->redirect(array('controller'=>'users', 'action'=>'my_profile'));	
					} else {
						$this->Session->setFlash("There is some problem in updating password.Please try again later.",'default', array('class'=>'btn-success success_msg'));
					}
				}else{
					$this->Session->setFlash("New password and confirm password did not match. Please try again.",'default', array('class'=>'btn-success success_msg'));
					$this->redirect(array('controller'=>'users', 'action'=>'change_password'));
				}
		
			} else {
						$this->Session->setFlash("You entered a wrong password. Please try again.",'default', array('class'=>'btn-success success_msg'));
						$this->redirect(array('controller'=>'users', 'action'=>'change_password'));
			}
	}
	
	if($this->Auth->user('user_type') == 3){
		$this->layout = 'admin_layout';
	} else {
		$this->layout = 'dashboard_layout';
	}
	$this->render("change_password");

	}

	public function about_us() {
	
	$this->loadModel('Setting');
	$content = $this->Setting->find('first',array('fields'=>array('Setting.about_us')));
	$this->set('content',$content);

	$this->render("about_us");

	}

	public function contact_us() {
	
	if($this->request->is('post')){
		//pr($this->request->data);
		/*s
		$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array($this->request->data['User']['email_id'] => 'User'))
						->to('piyushrana609@gmail.com')
						->subject('Contact Us Email')
						->message('Hello Test Email')
						->viewVars(array('name' => $this->request->data['User']['user_name'],
								'email' => $this->request->data['User']['email_id'],
								'address' => $this->request->data['User']['address']
						))
						->send();
	die("Here"); */
		$this->Session->setFlash('Your message is submitted successfully','default', array('class'=>'btn-success success_msg'));
	}
	$this->render("contact_us");

	}

	public function privacy() {
	
	$this->loadModel('Setting');
	$content = $this->Setting->find('first',array('fields'=>array('Setting.privacy')));
	$this->set('content',$content);
	$this->render("privacy");

	}

	public function terms_of_use() {
	
	$this->loadModel('Setting');
	$content = $this->Setting->find('first',array('fields'=>array('Setting.terms_of_use')));
	$this->set('content',$content);
	$this->render("terms_of_use");

	}

	public function team() {

	$this->loadModel('Setting');
	$content = $this->Setting->find('first',array('fields'=>array('Setting.team')));
	$this->set('content',$content);
	$this->render("team");

	}

	public function help() {
	
	if($this->Auth->user('user_type') == 3){
		$this->layout = 'admin_layout';
	} else {
		$this->layout = 'dashboard_layout';
	}
	$this->render("help");
	}

	public function professional_request() {

	$id = $this->Auth->user('id');
	$this->request->data['User']['request_professional'] = 1;
	$this->User->id = $id;
	if($this->User->save($this->request->data)){

		$this->Session->setFlash("Your request has been submitted succesfully");
		$this->redirect(array('controller'=> 'users', 'action'=>'my_profile'));
	}
	}


	public function professional_requests() {

	if($this->Auth->user('user_type') == 3) {
	
	$all_users = $this->User->find('all', array(
	    'conditions' => array(
			'user_type' => '2',
			'status' => '1',
			'request_professional'=>'1'
		    ),
	    'order' => 'User.id DESC'
	));
	$this->set('all_users',$all_users);
    $this->layout = 'admin_layout';
	$this->render('professional_requests');
 	} else {
	     $this->Session->setFlash('You cannot access this page','default', array('class'=>'btn-success success_msg'));
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function process_professional($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['uid']);
 	//echo $id; die();
 	$user = $this->User->findById($id);
 	$toEmail = $user['User']['email'];
	$this->request->data['User']['user_type'] = 1;
	$this->User->id = $id;
        if($this->User->save($this->request->data)) {
        		$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array('phpdevelopernk@gmail.com' => 'Worldstache'))
						->to($toEmail)
						->subject('Professional Request Approved')
						->send('You request to become professional has been approved. Please login to view your new dashboard');
	$this->Session->setFlash("User has succesfully become professional",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users','action'=>'professional_requests'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while Updating. Please try again later");
	$this->redirect(array('controller'=>'users','action'=>'professional_requests'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}

	public function cancel_professional($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['uid']);

	$user = $this->User->findById($id);
 	$toEmail = $user['User']['email'];
	$this->request->data['User']['request_professional'] = 0;
	
	$this->User->id = $id;
        if($this->User->save($this->request->data)) {
        	$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array('phpdevelopernk@gmail.com' => 'Worldstache'))
						->to($toEmail)
						->subject('Professional Request Disapproved')
						/*->viewVars(array('name' => $this->request->data['User']['first_name'],
								'email' => $this->request->data['User']['email'],
                                'confirm_code' => $ms
						)) */
						->send('You request to become professional has been disapproved');
	$this->Session->setFlash("Professiona Request of User is canceled",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users','action'=>'professional_requests'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while Canceling. Please try again later");
	$this->redirect(array('controller'=>'users','action'=>'professional_requests'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

	}

	public function set_payment() {

	$user = $this->User->find('first', array(
	    'conditions' => array(
		'User.id' => $this->Auth->user('id'),
	    )
	));
	$this->set('user',$user);

	$id = $user['User']['id'];
	if($this->request->is('post')){
		$this->User->id = $id;
		if($this->User->save($this->request->data)){
		$this->Session->setFlash("Payment Information is updated succesfully",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'users','action'=>'set_payment'));
		}
	}
    $this->layout = 'dashboard_layout';
	$this->render('set_payment');

	}


	public function guestlist_sold() {


	$this->loadModel('Event');
	$user_events = $this->Event->find('all', array(
		'conditions'=>array(
			'user_id'=>$this->Auth->user('id'),
			'event_status'=> '1'
			)
		));
	$this->set('user_events',$user_events);

	if($this->request->is('post')){
		//$this->User->id = $id;
		/*if($this->User->save($this->request->data)){
		$this->Session->setFlash("Paypal account updated",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'users','action'=>'set_payment'));
		} */
	}
    $this->layout = 'dashboard_layout';
	$this->render('guestlist_sold');

	}

	public function showguestlist() {
			
		$event_id = $this->request->data['id'];

		$this->loadModel('Payment');
		$result = $this->Payment->find('all', array('conditions'=>array('event_id'=>$event_id)));
		//pr($result); die();
		echo json_encode($result);          
		$this->layout = false;
		$this->render(false);
	}


	public function post_comment(){

		$type_id = base64_decode($_GET['eid']);
		$user_id = $this->Auth->user('id');

		if($this->request->is('post')){

			$this->request->data['Comment']['type_id'] = $type_id;
			$this->request->data['Comment']['user_id'] = $user_id;

			if($this->request->data['Comment']['comment_type'] == "event"){
				$view = "view_event";
				$controller = "events";
				$mid = "eid";
			} elseif($this->request->data['Comment']['comment_type'] == "artist"){
				$view = "view_artist";
				$controller = "artists";
				$mid = "aid";
			} else{
				$view = "view_place";
				$controller = "places";
				$mid = "pid";
			}

			$this->loadModel('Comment');
			if($this->Comment->save($this->request->data)){
				$this->Session->setFlash("Comment has been submitted succresfully.",'default', array('class'=>'btn-success success_msg'));
				$this->redirect(array('controller'=>$controller, 'action'=>$view, '?' => array($mid => $_GET['eid'])));
			} else {
				$this->Session->setFlash("Cannot post your comment. Please try again later.",'default', array('class'=>'btn-success success_msg'));
				$this->redirect(array('controller'=>$controller, 'action'=>$view, '?' => array($mid => $_GET['eid'])));
			}
		}


	}

	public function delete_user($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['uid']);

	$this->User->id = $id;
        if($this->Event->delete()) {
	
	$this->Session->setFlash("User has been deleted succesfully");
	$this->redirect(array('controller'=>'users','action'=>'user_list'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'users','action'=>'user_list'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}

	public function deactivate_user() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['uid']);
	
	$this->User->id = $id;
	
	if($this->User->saveField('status', 0)){

	$this->Session->setFlash("User has been deactivated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users', 'action'=>'user_list'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users', 'action'=>'user_list'));
	} 

	$this->layout = 'admin_layout';
	$this->render('add_event');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function edit_user() {
   $this->set('title_for_layout','Edit Events');
	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['uid']);
	
	$user = $this->User->find('first',array('fields'=> array('first_name','last_name','email','phone_number','profile_image','address'), 'conditions'=>array('User.id' => $id)));
	$this->set('user', $user);

	if($this->request->is('post','put')) {

		$filename = $this->request->data['User']['profile_image']['name'];
		if($filename == ""){
		$this->request->data['User']['profile_image'] = $user['User']['profile_image'];
		} else {
		move_uploaded_file( $this->request->data['User']['profile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/user/'. basename($filename));
		$this->request->data['User']['profile_image'] = '/images/user/'.$filename;
		}
	
		$this->User->id = $id;	
		if($this->User->save($this->request->data)){

		$this->Session->setFlash("User has been updated successfully",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'users', 'action'=>'users_list'));

		} else {

		$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
		$this->redirect(array('controller'=>'users', 'action'=>'users_list'));
		} 
	}
	$this->layout = 'admin_layout';
	$this->render('edit_user');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}

	public function my_orders() {

	//echo $this->Auth->user('id');
	$this->loadModel('Payment');
	$user_orders = $this->Payment->find('all', array(
		'conditions'=>array(
			'user_id'=>$this->Auth->user('id')
			)
		));
	$this->set('user_orders',$user_orders);

    $this->layout = 'dashboard_layout';
	$this->render('my_orders');

	}

	public function manage_orders() {
	$this->set('title_for_layout','Edit Events');
	if($this->Auth->user('user_type') == 3) {
		$this->loadModel('Payment');
		$paypal_orders = $this->Payment->find('all', array(
			'conditions'=>array(
				'mode_of_payment'=>'Paypal'
				)
			));
		$this->set('paypal_orders',$paypal_orders);

		$cashondeleivery = $this->Payment->find('all', array(
			'conditions'=>array(
				'mode_of_payment'=>'Cashondelievery'
				)
			));
		$this->set('cashondeleivery',$cashondeleivery);

	    $this->layout = 'admin_layout';
		$this->render('manage_orders');

		} else {
		 $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	}

	public function add_professional() {

	if($this->Auth->user('user_type') == 3) {

		if($this->request->is('post')){

			$email_exits = $this->User->findByEmail($this->request->data['User']['email']);

			if(!empty($email_exits)){
				$this->Session->setFlash('This email address already exits.','default', array('class'=>'btn-success success_msg'));
	     		$this->redirect(array('controller'=>'users', 'action'=>'add_professional'));
			} else {
				
			$this->request->data['User']['user_type'] = 2;
			$this->request->data['User']['status'] = 1;
			$filename = $this->request->data['User']['profile_image']['name'];
			if($filename != ""){
			move_uploaded_file( $this->request->data['User']['profile_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/user/'. basename($filename));
			$this->request->data['User']['profile_image'] = '/images/user/'.$filename;
			} else {
			$this->request->data['User']['profile_image'] = "";
			}

			if($this->User->save($this->request->data)){
				$this->Session->setFlash('Professional is added successfully','default', array('class'=>'btn-success success_msg'));
	     		$this->redirect(array('controller'=>'users', 'action'=>'add_professional'));

			} else {
				$this->Session->setFlash('Professional cannot be created at this time. Please try again later.','default', array('class'=>'btn-success success_msg'));
	     		$this->redirect(array('controller'=>'users', 'action'=>'add_professional'));				
			}
		}
	}
	
    $this->layout = 'admin_layout';
	$this->render('add_professional');
 	} else {
	     $this->Session->setFlash('You cannot access this page','default', array('class'=>'btn-success success_msg'));
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}

}

	?>
