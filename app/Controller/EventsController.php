<?php 
	
/* 

 This controller uses Subcategory Model

*/
App::uses('CakeEmail', 'Network/Email');
Class EventsController extends AppController {


    public function add_event() {
	$this->set('title_for_layout','Add Events');
	if($this->Auth->user('user_type') == 3) {

	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1'
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('all_events',$all_events);

	$pending_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '0'
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('pending_events',$pending_events);

	if($this->request->is('post')) {
	
	$this->request->data['Event']['event_ticket'] = serialize($this->request->data['Event']['event_ticket']);
	$this->request->data['Event']['event_ticket_price'] = serialize($this->request->data['Event']['event_ticket_price']);
	$this->request->data['Event']['event_ticket_seats'] = serialize($this->request->data['Event']['event_ticket_seats']);
	$this->request->data['Event']['event_ticket_description'] = serialize($this->request->data['Event']['event_ticket_description']);

	$tags = explode(',', $this->request->data['Event']['event_tags']);
	$this->loadModel('Tag');
	foreach($tags as $tag){
		$data = array('tag_name'=> $tag);
		$exits = $this->Tag->find('first', array('conditions'=> array('tag_name' => $tag)));
		if(empty($exits) || $exits == "" || $exits == null){
			$this->Tag->create();
        	$this->Tag->save(array('tag_name' =>  $tag));
		} else {
			$id = $exits['Tag']['id'];
			$count = $exits['Tag']['tag_count'] + 1;

			$this->Tag->id = $id;
			$this->Tag->save(array('tag_count' =>  $count));
		}
	}
	
	
	
	$this->request->data['Event']['user_id'] = $this->Auth->user('id');
	$this->request->data['Event']['event_start_timestamp'] = strtotime($this->request->data['Event']['event_start_date']);
	$this->request->data['Event']['event_end_timestamp'] = strtotime($this->request->data['Event']['event_end_date']);

	/* Process event images here */
	$filename = $this->request->data['Event']['event_image']['name'];
	move_uploaded_file( $this->request->data['Event']['event_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_image'] = '/images/event/'.$filename;

	$filename = $this->request->data['Event']['event_flyer']['name'];
	move_uploaded_file( $this->request->data['Event']['event_flyer']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_flyer'] = '/images/event/'.$filename;

	$filename = $this->request->data['Event']['event_cover_image']['name'];
	move_uploaded_file( $this->request->data['Event']['event_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_cover_image'] = '/images/event/'.$filename;
	/*Event images processing ends here */
	if($this->Event->save($this->request->data)) {
	$this->Session->setFlash("Event has been saved successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events','action'=>'add_event'));
	} else {

	$this->Session->setFlash("Opps!!.. Event could not be saved. Please try again later");
	}
   	}
	$this->layout = 'admin_layout';
	$this->render('add_event');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


   public function edit_event() {
   $this->set('title_for_layout','Edit Events');
	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['eid']);
	
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1'
	    ),
	    'order' => 'Event.id DESC'
	));
 

	$this->set('all_events',$all_events);
	
	$pending_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '0'
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('pending_events',$pending_events);
	
	    $event = $this->Event->findById($id);	
	  //  pr($event['Event']['event_ticket_description']); die();	
	       $event_ticket=unserialize($event['Event']['event_ticket']);
	       $event_price=unserialize($event['Event']['event_ticket_price']);
	       $event_seats=unserialize($event['Event']['event_ticket_seats']);
	     //  $event_description=unserialize($event['Event']['event_ticket_description']);
	      	       
				$countt= count($event_ticket); // die();	
				
						
			  $this->set('event_ticket', $event_ticket);	
			  $this->set('event_price', $event_price);	
			  $this->set('event_seats', $event_seats);	
			//  $this->set('event_description', $event_description);
			  $this->set('countt', $countt);
	          $this->set('event', $event);
	if($this->request->is('post','put')) {
	$this->request->data['Event']['event_ticket'] = serialize($this->request->data['Event']['event_ticket']);
	$this->request->data['Event']['event_ticket_price'] = serialize($this->request->data['Event']['event_ticket_price']);
	$this->request->data['Event']['event_ticket_seats'] = serialize($this->request->data['Event']['event_ticket_seats']);
	$this->request->data['Event']['event_ticket_description'] = serialize($this->request->data['Event']['event_ticket_description']);
		//pr($this->request->data); die();
	
	$filename = $this->request->data['Event']['event_image']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_image'] = $event['Event']['event_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_image'] = '/images/event/'.$filename;
	}
	
	$filename = $this->request->data['Event']['event_flyer']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_flyer'] = $event['Event']['event_flyer'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_flyer']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_flyer'] = '/images/event/'.$filename;
	}
	
	$filename = $this->request->data['Event']['event_cover_image']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_cover_image'] = $event['Event']['event_flyer'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_cover_image'] = '/images/event/'.$filename;
	}
	
	$this->Event->id = $id;	
	if($this->Event->save($this->request->data)){

	$this->Session->setFlash("Event has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events', 'action'=>'add_event'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'admin_layout';
	$this->render('edit_event');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function delete_event($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['eid']);
	$this->Event->id = $id;
        if($this->Event->delete()) {
	
	$this->Session->setFlash("Event has been deleted succesfully");
	$this->redirect(array('controller'=>'events','action'=>'add_event'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'events','action'=>'add_event'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function activate_event() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['eid']);
	
	
	$this->Event->id = $id;
	
	if($this->Event->saveField('event_status', 1)){

	$this->Session->setFlash("Event has been activated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events', 'action'=>'add_event'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_event');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function deactivate_event() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['eid']);
	
	
	$this->Event->id = $id;
	
	if($this->Event->saveField('event_status', 0)){

	$this->Session->setFlash("Event has been deactivated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events', 'action'=>'add_event'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_event');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}

	
	public function item_list() {

	if($this->Auth->user('user_type') == 3) {
		
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1'
	    ),
	    'order' => 'id DESC'
	));
	$this->set('all_events',$all_events);
	
	$this->layout = 'admin_layout';
	$this->render('item_list');
	
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}

	public function add_event_p() {


	if($this->Auth->user('user_type') == 1){
	
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('all_events',$all_events);
	

	if($this->request->is('post')) {

	
		
	$this->request->data['Event']['event_ticket'] = serialize($this->request->data['Event']['event_ticket']);
	$this->request->data['Event']['event_ticket_price'] = serialize($this->request->data['Event']['event_ticket_price']);
	$this->request->data['Event']['event_ticket_seats'] = serialize($this->request->data['Event']['event_ticket_seats']);
	$this->request->data['Event']['event_ticket_description'] = serialize($this->request->data['Event']['event_ticket_description']);

	$tags = explode(',', $this->request->data['Event']['event_tags']);
	$this->loadModel('Tag');
	foreach($tags as $tag){
		$data = array('tag_name'=> $tag);
		$exits = $this->Tag->find('first', array('conditions'=> array('tag_name' => $tag)));
		if(empty($exits) || $exits == "" || $exits == null){
			$this->Tag->create();
        	$this->Tag->save(array('tag_name' =>  $tag));
		} else {
			$id = $exits['Tag']['id'];
			$count = $exits['Tag']['tag_count'] + 1;

			$this->Tag->id = $id;
			$this->Tag->save(array('tag_count' =>  $count));
		}
	}
	$this->request->data['Event']['user_id'] = $this->Auth->user('id');
	//$this->request->data['Event']['event_options'] = serialize($this->request->data['Event']['event_option']);
	
	/* Process event images here */
	$filename = $this->request->data['Event']['event_image']['name'];
	move_uploaded_file( $this->request->data['Event']['event_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_image'] = '/images/event/'.$filename;

	$filename = $this->request->data['Event']['event_flyer']['name'];
	move_uploaded_file( $this->request->data['Event']['event_flyer']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_flyer'] = '/images/event/'.$filename;

	$filename = $this->request->data['Event']['event_cover_image']['name'];
	move_uploaded_file( $this->request->data['Event']['event_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	$this->request->data['Event']['event_cover_image'] = '/images/event/'.$filename;
	/*Event images processing ends here */
	
	if($this->Event->save($this->request->data)) {
	$this->Session->setFlash("Event has been saved successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'events','action'=>'add_event_p'));
	} else {

	$this->Session->setFlash("Opps!!.. Event could not be saved. Please try again later");
	}
   	}
	$this->layout = 'dashboard_layout';
	$this->render('add_event_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}


	}

	public function edit_event_p() {


	if($this->Auth->user('user_type') == 1) {	
	$id = base64_decode($_GET['eid']);
	
	        $event = $this->Event->findById($id);	
	
	       $event_ticket=unserialize($event['Event']['event_ticket']);
	       $event_price=unserialize($event['Event']['event_ticket_price']);
	       $event_seats=unserialize($event['Event']['event_ticket_seats']);
	     //  $event_description=unserialize($event['Event']['event_ticket_description']);
	     //pr($event_price);
	      	       
				$countt= count($event_ticket); // die();	
				
						
			  $this->set('event_ticket', $event_ticket);	
			  $this->set('event_price', $event_price);	
			  $this->set('event_seats', $event_seats);	
			//  $this->set('event_description', $event_description);
			  $this->set('countt', $countt);
	          $this->set('event', $event);

	if($this->request->is('post','put')) {
		
	$this->request->data['Event']['event_ticket'] = serialize($this->request->data['Event']['event_ticket']);
	$this->request->data['Event']['event_ticket_price'] = serialize($this->request->data['Event']['event_ticket_price']);
	$this->request->data['Event']['event_ticket_seats'] = serialize($this->request->data['Event']['event_ticket_seats']);
	$this->request->data['Event']['event_ticket_description'] = serialize($this->request->data['Event']['event_ticket_description']);

	$filename = $this->request->data['Event']['event_image']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_image'] = $event['Event']['event_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_image'] = '/images/event/'.$filename;
	}
	
	$filename = $this->request->data['Event']['event_flyer']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_flyer'] = $event['Event']['event_flyer'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_flyer']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_flyer'] = '/images/event/'.$filename;
	}
	
	$filename = $this->request->data['Event']['event_cover_image']['name'];
	if($filename == ""){
		$this->request->data['Event']['event_cover_image'] = $event['Event']['event_cover_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Event']['event_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/event/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Event']['event_cover_image'] = '/images/event/'.$filename;
	}

	
	$this->Event->id = $id;	
	if($this->Event->save($this->request->data)){

	$this->Session->setFlash("Event has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users', 'action'=>'dashboard'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'dashboard_layout';
	$this->render('edit_event_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}


	}
	
	public function delete_event_p($id=null) {

	if($this->Auth->user('user_type') == 1) {
	$id = base64_decode($_GET['eid']);

	$this->Event->id = $id;
        if($this->Event->delete()) {
	$this->Session->setFlash("Event has been deleted succesfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'users','action'=>'dashboard'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'users','action'=>'dashboard'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	
	public function search() {
		
		/*$this->loadModel('Tag');
		$tags = $this->Tag->find('all',array('order'=> 'Tag.id DESC'));
		$this->set('tags',$tags); */

		$this->loadModel('Category');
		$cat_drp = $this->Category->find('all',array(
		'order'=> 'Category.category_name ASC'
		));
		$this->set('cat_drp',$cat_drp);
		
		if($this->request->is('post')){

			if($this->request->data['age_limit'] !== "All") {
				$age = (explode("-",$this->request->data['age_limit']));
				$start_age = $age['0'];
				$end_age = $age['1'];
				} else {
				$start_age = "14";
				$end_age = "70";
				}

				if($this->request->data['event_date'] == "" && $this->request->data['date_option'] ==""){
					//$startdate = new DateTime(date('m/d/Y'));
					//$sdate = $startdate->format('m/d/Y');
					//$this->request->data['event_date'] = $sdate;
					$this->request->data['date_option'] = 0;
				}
			$data = array(
				'cat_id'=>$this->request->data['cat_id'],
				'date_option'=>$this->request->data['date_option'],
				'sub_cat_id'=>$this->request->data['sub_cat_id'],
				'where'=>$this->request->data['where'],
				'event_date'=>$this->request->data['event_date'],
				'start_age'=>$start_age,
				'end_age'=>$end_age,
				);
			$this->Event->bindModel(array('belongsTo' => array(
									        'Place' => array(
									            'className' => 'Place',
									            'foreignKey' => 'event_venue',
									            'fields' => array('Place.id', 'Place.name')
									        )
								        )
        					));
			$all_events = $this->Event->getEventsBySearch($data);
			$this->set('all_events',$all_events);
		}

		//$this->layout = 'search_layout';
		$this->render('search');
		
		}
		
		//**********Seach Result********//
		
		public function searchresult() {
			
			
		 $str= ltrim ($this->request->data['evenids'], ',');	
			$strArry =explode(',',$str); 
			//pr($strArry);
		if(isset($this->request->data['tag_name'])){
                 
                 	
		   $resultData = $this->Event->find('all', array('conditions' => array(
		   'MATCH(Event.event_tags) AGAINST("' . $this->request->data['tag_name'] . '" IN BOOLEAN MODE)','Event.id'=>$strArry)));
			
		}else if(isset($this->request->data['start_rang']) || isset($this->request->data['end_rang'])){
			
			$resultData = $this->Event->find('all', array('conditions' => array('Event.event_price >='=>$this->request->data['start_rang'],'Event.event_price <='=>$this->request->data['end_rang'],'Event.id'=>$strArry)));
			
		       }
		      
		      else if(isset($this->request->data['age_1']) && isset($this->request->data['evenids'])){				
			
			$resultData = $this->Event->find('all', array('conditions' => array('Event.event_start_age >='=>$this->request->data['age_1'],'Event.event_start_age <='=>$this->request->data['end_rang_age'],'Event.id'=>$strArry)));
			
		      }
		      else if(empty($this->request->data['evenids'])){				
			
			$resultData = $this->Event->find('all', array('conditions' => array('Event.event_start_age >='=>$this->request->data['age_1'],'Event.event_start_age <='=>$this->request->data['end_rang_age'])));
			
		      }
		     //pr($resultData);
		    echo json_encode($resultData);
		    $this->layout = false;
		    $this->render(false);
		
		}
		
		//***************End***************//
	public function events_list() {

	$id = base64_decode($_GET['eid']);

	if($id == 0){
	$title = "All";
	$this->set('title', $title);
	$sub_cat_image = "all.png";
	$this->set('sub_cat_image',$sub_cat_image);

	$this->loadModel('Category');
	$cat_drp = $this->Category->find('all',array(
		'order'=> 'Category.category_name ASC'
		));
	$this->set('cat_drp',$cat_drp);
	$startdate = new DateTime(date('m/d/Y'));
	$sdate = $startdate->format('m/d/Y');
	$sdatetimestamp = strtotime($sdate);
	$this->Event->bindModel(array('belongsTo' => array(
									        'Place' => array(
									            'className' => 'Place',
									            'foreignKey' => 'event_venue',
									            'fields' => array('Place.id', 'Place.name')
									        )
								        )
        					));
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1',
		'event_end_timestamp >=' => $sdatetimestamp
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('title_for_layout','All Events | Worldstache');
	} else {

	$this->loadModel('Category');
	$cat_drp = $this->Category->find('all',array(
		'order'=> 'Category.category_name ASC'
		));
	$this->set('cat_drp',$cat_drp);

	$this->loadModel('Subcategory');
	$cat_name = $this->Subcategory->find('first', array(
		'conditions'=> array('Subcategory.id'=>$id)
		));
	$title = $cat_name['Subcategory']['subcategory_name'];
	$this->set('title',$title);
	$this->set('title_for_layout',$title.' | Worldstache');
	$sub_cat_image = $cat_name['Subcategory']['subcategory_image'];
	$this->set('sub_cat_image',$sub_cat_image);

	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1',
		'sub_cat_id'=>$id
	    ),
	    'order' => 'Event.id DESC'
	));
	}
	$this->set('all_events',$all_events);
	$this->render('events_list');
	
	}
	
	
	public function events_list_s() {
	
	$id = base64_decode($_GET['eid']);
	$this->loadModel('Category');

	$sub_cat_image = "all.png";
	$this->set('sub_cat_image',$sub_cat_image);

	$startdate = new DateTime(date('m/d/Y'));
	$sdate = $startdate->format('m/d/Y');
	$sdatetimestamp = strtotime($sdate);

	$cat_name = $this->Category->find('first', array(
		'conditions'=> array('Category.id'=>$id)
		));
	$title = $cat_name['Category']['category_name'];
	$this->set('title',$title);

	$cat_drp = $this->Category->find('all',array(
		'order'=> 'Category.category_name ASC'
		));
	$this->set('cat_drp',$cat_drp);

	$this->set('title_for_layout',$title.' | Worldstache');
	$this->Event->bindModel(array('belongsTo' => array(
									        'Place' => array(
									            'className' => 'Place',
									            'foreignKey' => 'event_venue',
									            'fields' => array('Place.id', 'Place.name')
									        )
								        )
        					));
	$all_events = $this->Event->find('all', array(
	    'conditions' => array(
		'event_status' => '1',
		'cat_id'=>$id,
		'event_end_timestamp >=' => $sdatetimestamp
	    ),
	    'order' => 'Event.id DESC'
	));
	
	$this->set('all_events',$all_events);
	$this->render('events_list');
	
	}


	public function event_detail() {

	$id = base64_decode($_GET['eid']);
	$event_detail = $this->Event->find('first', array(
	    'conditions' => array(
		'Event.id'=>$id
	    ),
	    'order' => 'Event.id DESC'
	));
	//pr($event_detail); die();
	
	$this->loadModel('Place');
	$place = $this->Place->find('first', array('conditions'=>array('Place.id'=>$event_detail['Event']['event_venue'])));
	$this->set('place', $place);
	//pr($place); die();
	
	$this->set('event_detail',$event_detail);

	$tags = explode(',',$event_detail['Event']['event_tags']);
	
	$tag_counts =  count($tags);
	$event_tags = array();
	for($i=0; $i <= $tag_counts-1; $i++){
		
		$this->loadModel("Taglike");		
		$result = $this->Taglike->find('count',array('conditions'=> array('Taglike.tag_name'=> $tags[$i], 'Taglike.status'=>'1')));		
		$tag_count = $result;
		array_push($event_tags, $tag_count);
		}
		
	$this->set('event_tags',$event_tags);
	$this->set('title_for_layout','Event Detail | Worldstache');

	$event_owner_id = $event_detail['Event']['user_id'];

	$event_cat_id = $event_detail['Event']['cat_id'];
	$upcoming_events = $this->Event->find('all', array('fields' => array('Event.id','Event.event_title', 'Event.event_image')));
	$this->set('upcoming_events', $upcoming_events);

	$user_id = $this->Auth->user('id');
	$user_data = $this->Auth->user('profile_image');
	$this->set('user_data',$user_data);
	$this->loadModel("Taglike");
		//$user_id = $this->Auth->user('id');
	
	
	$like= $this->Taglike->find('list', array('fields'=>array('tag_name'),'conditions' => array('user_id'=> $user_id, 'status'=>'1')));
	$this->set('likestatus',$like);
	$this->loadModel('Comment');
	$all_comments = $this->Comment->find('all', array('conditions' => array('type_id'=> $id, 'comment_type'=>'event')));
	$this->set('all_comments',$all_comments);

	if($user_id != "" && $user_id != null){
		$this->set('user_id', $user_id);
	} else{
		$user_id = "";
		$this->set('user_id', $user_id);
	}
	if($this->request->is('post')){
		$action = "https://www.sandbox.paypal.com/cgi-bin/webscr";
		$business = "piyushrana609@gmail.com";
		$return = "http://dev.staplelogic.in/worldstashdev/events/success";
		
		if(empty($this->request->data['amount']) || $this->request->data['amount'] == ""){
			$this->request->data['amount'] = 20;
		}
		$this->request->data['user_id'] = $user_id;

		$this->request->data['event_owner'] = $event_owner_id;
		
		$this->request->data['order_number'] = "EOD".uniqid();

		$this->request->data['mode_of_payment'] = "Paypal";

		  
		$customFields = $this->request->data['order_number'].'-'.$this->request->data['item_name'].'-'.$user_id.'-'.$this->request->data['event_id'].'-'.$this->request->data['total_tickets'].'-'.$this->request->data['total_type'];

		$this->loadModel('Payment');
		if($this->Payment->save($this->request->data)){
		$this->redirect($action.'?cmd='.$this->request->data['cmd'].'&business='.$business.
                      '&currency_code='.$this->request->data['currency_code'].'&item_name='.$this->request->data['item_name'].'&amount='.$this->request->data['amount'].'&return='.$return.'&custom='.$customFields);
		} else {

			$this->Session->setFlash("Cannot process transaction at this time. Try Again Later",'default', array('class'=>'btn-success success_msg'));
		}
	}

	$this->render('event_detail');
	
	}

	public function success() {

	//pr($_REQUEST);
	$this->request->data['Payment']['transaction_id'] = $_REQUEST['tx'];
	//$this->request->data['Payment']['item_description'] = $_REQUEST['tx'];
	$this->request->data['Payment']['transaction_status'] = $_REQUEST['st'];



	$custom = explode('-',$_REQUEST['cm']);
	$total_tickets = $custom['4'];
	$total_type = $custom['5'];
	$event_id = $custom['3'];
	$total_amount = $_REQUEST['amt'];

	if($_REQUEST['st'] == "Completed"){
	$event_mod = $this->Event->find('first', array(
		'conditions'=>array(
			'id'=>$custom['3']
			)
		));
	$tickets_sold = $event_mod['Event']['tickets_sold'];
	$revenue = $event_mod['Event']['total_revenue'];

	$total_tickets_sold = $tickets_sold + $total_tickets;
	$total_revenue = $total_amount + $revenue; 

	$this->request->data['Event']['tickets_sold'] = $total_tickets_sold;
	$this->request->data['Event']['total_revenue'] = $total_revenue;

	//pr($this->request->data); die();

	$this->Event->id = $event_id;
	$this->Event->save($this->request->data);
	}
	$this->loadModel('Payment');
	$payment = $this->Payment->find('first', array(
		'conditions'=>array(
			'user_id'=>$custom['2'],'event_id'=>$event_id,'order_number'=>$custom['0']
			)
		));
	//pr($payment);
	
	$id = $payment['Payment']['id'];
	$this->Payment->id = $id;
	$this->Payment->save($this->request->data);
	//$this->Session->setFlash("Successfull Payment",'default', array('class'=>'btn-success success_msg'));
	$this->set('payment',$payment);
	$this->set('data', $this->request->data);
	$this->layout = 'dashboard_layout';
	$this->render('success');

	
	}

	public function view_event() {

	if($this->Auth->user('user_type') == 3) {
	$this->set('title_for_layout','View Event');
	$id = base64_decode($_GET['eid']);
	$event_detail = $this->Event->find('first', array(
	    'conditions' => array(
		'Event.id'=>$id
	    ),
	    'order' => 'Event.id DESC'
	));
	$this->set('event_detail',$event_detail);

	$event_owner_id = $event_detail['Event']['user_id'];

	$event_cat_id = $event_detail['Event']['cat_id'];
	$upcoming_events = $this->Event->find('all', array('fields' => array('Event.id','Event.event_title', 'Event.event_image'),'conditions'=>array('cat_id'=>$event_cat_id)));
	$this->set('upcoming_events', $upcoming_events);

	$user_id = $this->Auth->user('id');
	$user_data = $this->Auth->user('profile_image');
	$this->set('user_data',$user_data);

	$this->loadModel('Comment');
	$all_comments = $this->Comment->find('all', array('conditions' => array('type_id'=> $id, 'comment_type'=>'event')));
	$this->set('all_comments',$all_comments);

	 if($user_id != "" && $user_id != null){
		$this->set('user_id', $user_id);
	} else{
		$user_id = "";
		$this->set('user_id', $user_id);
	}
	$this->render('view_event');
	
		} else {
		 $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
		}
	}

	//**********Seach Result Filter********//
	public function searcheventfilter() {
		$startdate = new DateTime(date('m/d/Y'));
		$sdate = $startdate->format('m/d/Y');
		$sdatetimestamp = strtotime($sdate);
		$conditions = array();
		$resultData['locations'] = array();
		$data = $this->request->data;
		$resultData['Events'] = array();
		$resultData['Subcategory'] = array();
		if( (isset($data['start_price'])) && (isset($data['end_price'])) ){
			$event_ids = $this->getPriceRangeEventIds($data['start_price'], $data['end_price'], $sdatetimestamp);
			$conditions[] = array('Event.id' => $event_ids);
		}
		$cat_ids = array();
		if(isset($data['cat_id']) && !empty($data['cat_id'])){
			$cat_ids = explode(',',$this->request->data['cat_id']);
			$conditions[] = array('Event.cat_id' => $cat_ids);	
		}

		if(isset($data['subcat_id']) && !empty($data['subcat_id'])){
			$subcat_ids = explode(',',$this->request->data['subcat_id']);
			$conditions[] = array('Event.sub_cat_id' => $subcat_ids);	
		}
		$conditions[] = array('Event.event_status'=>'1', 'Event.event_end_timestamp >=' => $sdatetimestamp);
		$this->Event->bindModel(array('belongsTo' => array(
									        'Place' => array(
									            'className' => 'Place',
									            'foreignKey' => 'event_venue',
									            'fields' => array('Place.id', 'Place.name')
									        )
								        )
        					));
		$resultData['Events'] = $this->Event->find('all', array('conditions' => $conditions));
		foreach($resultData['Events'] as $location)
        {
			$resultData['locations'][] = array(
				'infocity' => $location['Event']['event_city'],
				'latitude' => $location['Event']['event_city_lat'], 
				'longitude' => $location['Event']['event_city_long']
			);
			
	    }
		$this->loadModel('Subcategory');
	    $resultData['Subcategory'] = $this->Subcategory->find('list', array('fields'=>array('Subcategory.id','Subcategory.subcategory_name'),'conditions' => array('Subcategory.cat_id'=> $cat_ids)));
	    echo json_encode($resultData);
		$this->layout = false;
		$this->render(false);
		exit;

	}

	public function searchresultfilter() {
			
		$startdate = new DateTime(date('m/d/Y'));
		$sdate = $startdate->format('m/d/Y');
		$sdatetimestamp = strtotime($sdate);		
		
	 $c=$this->request->data['catname_n'];
     $sc=$this->request->data['subcat_name'];
     
		if(isset($this->request->data['evenids'])){
			$str= ltrim ($this->request->data['evenids'], ',');	
			$strArry =explode(',',$str);
		}
		
		if(isset($this->request->data['cat_name'])){
			$locations=array();
			$resultData['locations'] = array();
			if($this->request->data['cat_name'] != ""){ 
           $cat_name = explode(',',$this->request->data['cat_name']);
           $resultData['Events'] = $this->Event->find('all', array('conditions' => array('Event.cat_id'=>$cat_name, 'Event.event_status'=> '1', 'Event.event_end_timestamp >=' => $sdatetimestamp)));
           $this->loadModel('Subcategory');
           $resultData['Subcategory'] = $this->Subcategory->find('list', array('fields'=>array('Subcategory.id','Subcategory.subcategory_name'),'conditions' => array('Subcategory.cat_id'=>$cat_name)));
      		
      	   $number = 1;
           foreach($resultData['Events'] as $location)
           {
			$resultData['locations'][] = array(
			'count'=>$number, 
			'infocity'=>$location['Event']['event_city'],
			'latitude'=>$location['Event']['event_city_lat'], 
			'longitude'=>$location['Event']['event_city_long']
			);
			$number++;
	      }
	     		   
		   } else {
		   		$resultData['Events'] = $this->Event->find('all', array('conditions' => array('Event.event_status'=> '1', 'Event.event_end_timestamp >=' => $sdatetimestamp)));
     			$this->loadModel('Subcategory');
           		$resultData['Subcategory'] = $this->Subcategory->find('list', array('fields'=>array('Subcategory.id','Subcategory.subcategory_name')));

		   }
			
		}else if(!empty($sc) && (!empty($c))){ 
			 $cat_name = explode(',',$this->request->data['catname_n']);
			 $sub_cat_name = explode(',',$this->request->data['subcat_name']);
			$resultData = $this->Event->find('all', array('conditions' => array('Event.cat_id'=>$cat_name,'Event.sub_cat_id'=>$sub_cat_name,'Event.event_status'=>'1', 'Event.event_end_timestamp >=' => $sdatetimestamp)));
			
			
		}
		else if(!empty($c)) { 
			 $cat_name = explode(',',$this->request->data['catname_n']);
			
			 $sub_cat_name = explode(',',$this->request->data['subcat_name']);
			$resultData = $this->Event->find('all', array('conditions' => array('Event.cat_id'=>$cat_name,'Event.event_status'=>'1', 'Event.event_end_timestamp >=' => $sdatetimestamp)));
			
			
		}
		  else if(isset($this->request->data['all'])){
			$resultData = $this->Event->find('all', array('conditions' => array('Event.event_status'=>'1')));	
		}

		else if(isset($this->request->data['age_1']) && (!empty($this->request->data['catname']))){				
			$cat_name = explode(',',$this->request->data['catname']);
			
			$resultData['Events'] = $this->Event->find('all', array('conditions' => array('Event.event_status'=> '1','Event.cat_id'=>$cat_name,'Event.event_start_age >='=>$this->request->data['age_1'],'Event.event_start_age <='=>$this->request->data['end_rang_age'], 'Event.event_end_timestamp >=' => $sdatetimestamp)));
			
		   }
		   else if(empty($this->request->data['catname'])){				
			
			$price=array();
			$event=array();
			$allevent = $this->Event->find('list', array('fields'=>array('id','event_ticket_price'),'conditions'=>array('Event.event_status'=>'1', 'Event.event_end_timestamp >=' => $sdatetimestamp)));	
		//	pr($allevent); die();
			foreach($allevent as $key=> $val)
			{
				$ticket_amount=unserialize($val);
				$price[$key]=$ticket_amount;
					
		    }
		    $start_price= $this->request->data['age_1'];
		    $end_price= $this->request->data['end_rang_age'];
		    $number = range($start_price,$end_price);		    		    
		    foreach($price as $k=>$val)
		    {  foreach($val as $nm){			 
				if(in_array($nm,$number))
				{
			      $event[$k]=$k;			   
			    }
			   }			
			}		    
			$resultData['Events'] = $this->Event->find('all', array('conditions' => array('Event.id'=> $event)));
			//$resultData['Events'] = $this->Event->find('all', array('conditions' => array('Event.event_status'=> '1','Event.event_start_age >='=>$this->request->data['age_1'],'Event.event_start_age <='=>$this->request->data['end_rang_age'], 'Event.event_end_timestamp >=' => $sdatetimestamp)));
			
		   } 		   
		    echo json_encode($resultData);
		    $this->layout = false;
		    $this->render(false);
		
		}



		protected function getPriceRangeEventIds($start_price = 0, $end_price = 1000, $sdatetimestamp){
			$price=array();
			$event=array();
			$allevent = $this->Event->find('list', array('fields'=>array('id','event_ticket_price'),'conditions'=>array('Event.event_status'=>'1', 'Event.event_end_timestamp  >=' => $sdatetimestamp)));	
		//	pr($allevent); die();
			foreach($allevent as $key=> $val)
			{
				$ticket_amount=unserialize($val);
				$price[$key]=$ticket_amount;
					
		    }
		    //$start_price= $this->request->data['age_1'];
		    //$end_price= $this->request->data['end_rang_age'];
		    $number = range($start_price,$end_price);		    		    
		    foreach($price as $k=>$val)
		    {  foreach($val as $nm){			 
				if(in_array($nm,$number))
				{
			      $event[$k]=$k;			   
			    }
			   }			
			}		
			return $event;    
		}
		
		//***************End***************//

		public function typehead() {
			
		    $this->layout = false;
		    $this->render("typehead");
		
		}

		public function stripepay() {

			$securityKey = "sk_test_BpK89BEtfVaQiZRAo2O4Swsq";
			$this->set('securityKey',$securityKey);
			$publishKey = "pk_test_VBIK1um1lZ8McYFyE4tZeumb";
			$this->set('publishKey',$publishKey);

			App::import("Vendor", "stripepayment/index");
			$this->layout = false;
		    $this->render(false);
		}
public function charge(){
	
	try {
     $securityKey = "sk_test_KpTtASJWp5HxkJfXSuVBY6Du";
	App::import("Vendor", "Stripe/lib/Stripe");
	Stripe::setApiKey($securityKey);
	
	$data = array(
			'email'=> 'atish@staplelogic.in'
			);
		$token = $this->Stripe->createCustomer($data);
		print_r($token); die();
		$customerId = $token['response']['id'];

	
	$charge = Stripe_Charge::create(array(
  "amount" => 1500,
  "currency" => "usd",
  "card" => $_POST['stripeToken'],
  "description" => ""
));
	//send the file, this line will be reached if no error was thrown above
	$msg= "<h1>Your payment has been completed. We will send you the Facebook Login code in a minute.</h1>";
echo $msg;
die();
  //you can send the file to this email:
  echo $_POST['stripeEmail'];
}

catch(Stripe_CardError $e) {
	
}

 catch (Stripe_InvalidRequestError $e) {


} catch (Stripe_AuthenticationError $e) { 

} catch (Stripe_ApiConnectionError $e) {
  // Network communication with Stripe failed
} catch (Stripe_Error $e) {

  // Display a very generic error to the user, and maybe send
  // yourself an email
} catch (Exception $e) {

  // Something else happened, completely unrelated to Stripe
}
}
		public function paybystripecomp(){
			$this->layout='';
		
		
     //   App::import("Vendor", "stripepayment/index");
		$id = base64_decode($_GET['ids']);
		$event_detail = $this->Event->find('first', array(
		    'conditions' => array(
			'Event.id'=>$id
		    ),
		    'order' => 'Event.id DESC'
		));
		$this->set('event_detail',$event_detail);
		
		$ticket_type = $_REQUEST['type'];
		$this->set('ticket_type',$ticket_type);
		
		$ticket_num = $_REQUEST['num'];
		$this->set('ticket_num',$ticket_num);

		if($this->request->is('post')){

		$data = array(
			'email'=> $this->request->data['Event']['email']
			);
		$token = $this->Stripe->createCustomer($data);
		$customerId = $token['response']['id'];

		$cards = array(
			'number' => $this->request->data['Event']['number'],
			'exp_month' => $this->request->data['Event']['exp_month'],
			'exp_year' => $this->request->data['Event']['exp_year'],
			'cvc' => $this->request->data['Event']['cvc']
			);
		$crdata = $this->Stripe->createCard($customerId,$cards);

		$this->request->data['Payment']['order_number'] = "EOD".uniqid();
		$this->request->data['Payment']['user_id'] = $this->Auth->user('id');
		$this->request->data['Payment']['event_id'] = $this->request->data['Event']['event_id'];
		$this->request->data['Payment']['event_owner_id'] = $this->request->data['Event']['event_owner_id'];
		$this->request->data['Payment']['mode_of_payment'] = "Stripes";
		//pr($this->request->data['Payment']);
		//die();
		echo $this->request->data['Payment']['order_number']." ===== > "; 
		//$this->loadModel('Payment');
		//$this->Payment->save($this->request->data);
		if(!empty($crdata)) {
			$cardData = array(
				"amount" => 1500,
	  			"currency" => "usd",
	  			"card" => $crdata['response']['id'],
	  			"description" => "Charge for event ticket",
	  			"metadata" => array('order'=>$this->request->data['Payment']['order_number'])
	  			);
			$response = $this->Stripe->charge($cardData,$customerId);
			//pr($response); die();
			if(!empty($response)){

				$order = $response['response']['metadata']['order'];
				echo $order;
			}
		}
		}
		$this->layout = false;
		$this->render('paybystripecomp');
	}


	public function cashondelievery(){

		$id = base64_decode($_GET['ids']);
		//echo $id; die();
		$event_detail = $this->Event->find('first', array(
		    'conditions' => array(
			'Event.id'=>$id
		    ),
		    'order' => 'Event.id DESC'
		));
		 
		// pr($event_detail);
		 //die();
		//pr(array_value(unserialize($event_detail['Event']['event_ticket_price']))); die();
		
		
		$this->set('event_detail',$event_detail);

		$ticket_type = $_REQUEST['type'];
		$this->set('ticket_type',$ticket_type);
		
		$ticket_num = $_REQUEST['num'];
		$this->set('ticket_num',$ticket_num);
		
		if($event_detail['Event']['notification_email'] == ""){
		$event_owner = $this->User->find('first', array(
		    'conditions' => array(
			'User.id'=>$this->request->data['Event']['event_owner_id']
		    )
			));
			
		$event_owner_email = $event_owner['User']['email_id'];
		
		} else {
			
			$event_owner_email = $event_detail['Event']['notification_email'];
			
			}
		
		if($this->request->is('post')) {
	  $ticket_amount=unserialize($event_detail['Event']['event_ticket_price']);
		$Total =$ticket_amount[0] * $this->request->data['Event']['total_tickets'];
		$this->request->data['Payment']['order_number'] = "EOD".uniqid();
		$this->request->data['Payment']['user_id'] = $this->Auth->user('id');
		$this->request->data['Payment']['event_id'] = $this->request->data['Event']['event_id'];
		$this->request->data['Payment']['event_owner'] = $this->request->data['Event']['event_owner_id'];
		$this->request->data['Payment']['total_type'] = $this->request->data['Event']['total_type'];
		$this->request->data['Payment']['total_tickets'] = $this->request->data['Event']['total_tickets'];
		$this->request->data['Payment']['mode_of_payment'] = "Cashondelievery";
		
		//pr($this->request->data); die();
		$this->loadModel('Payment');
		if($this->Payment->save($this->request->data)){
			$status = 1;
			$this->set('status',$status);

			$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array('phpdevelopernk@gmail.com' => 'Worldstache'))
						->to($this->request->data['Event']['email_address'])
						->subject('Order Confirmation')
						->send('Your Order is confirmed. Please visit your Worldstache account more more information.');
						
						
					/******************Email To Event Owner******************/
					$Email = new CakeEmail('gmail');
					$Email->emailFormat('text')
						->from(array('phpdevelopernk@gmail.com' => 'Worldstache'))
						->to($event_owner_email)
						->subject(' New Order Confirmation From :' .$this->request->data['Event']['email_address'])
						->send('Number Of Tickets :' .$this->request->data['Event']['total_tickets'] .'  Ticket Type :' . $this->request->data['Event']['total_type'] .'   Total Amount : ' .$Total);
					/*************************End******************************/	

			$this->Session->setFlash("Order places succesfully.",'default', array('class'=>'btn-success success_msg'));
			
			
		} else {
			$this->Session->setFlash("Cannot process transaction at this time. Try Again Later",'default', array('class'=>'btn-success success_msg'));
			$status = 2;
			$this->set('status',$status);
		}
	}
		$this->layout = false;
		$this->render('cashondelievery');
	}

	public function showeventdetail() {
			
		$event_id = $this->request->data['id'];

		$result = $this->Event->find('all', array('conditions'=>array('Event.id'=>$event_id)));
		//pr($result); die();
		echo json_encode($result);          
		$this->layout = false;
		$this->render(false);
	}

	public function getpricedescription() {
			
		$event_id = $this->request->data['id'];
		$ticket_type = $this->request->data['type'];

		$result = $this->Event->find('first', array('conditions'=>array('Event.id'=>$event_id)));

		$data['types'] = unserialize($result['Event']['event_ticket']);
		$key = array_search($ticket_type, $data['types']);

		$data['price'] = unserialize($result['Event']['event_ticket_price']);
		$ticket['price'] = $data['price'][$key];

		$data['description'] = unserialize($result['Event']['event_ticket_description']);
		$ticket['description'] = $data['description'][$key];

		$data['seats'] = unserialize($result['Event']['event_ticket_seats']);
		$ticket['seats'] = $data['seats'][$key];

		echo json_encode($ticket);          
		$this->layout = false;
		$this->render(false);
	}
	
	public function liketag() {
		
		$this->loadModel("Taglike");
		$user_id = $this->Auth->user('id');
		$tag_name = $this->request->data['tag_name'];
		$this->request->data['user_id'] = $user_id;
		$this->request->data['status'] = 1;		
		$result = $this->Taglike->find('first', array('conditions'=>array('Taglike.tag_name'=>$tag_name, 'Taglike.user_id'=> $user_id)));	
		if(!empty($result)){	  $id = $result['Taglike']['id'];	
			   $st='1';
		if($result['Taglike']['status']=='1')
		{
				$st='0';
		}elseif($result['Taglike']['status']=='0')
		{
				$st='1';
		}		
	   $this->Taglike->id = $id;
	   $this->Taglike->save(array('status' =>  $st));
			} else {
			$this->Taglike->save($this->request->data);
				
		}
	$result['count'] = $this->Taglike->find('count', array('conditions'=>array('Taglike.tag_name'=>$tag_name, 'Taglike.status'=> '1')));
	$result['like']= $this->Taglike->find('first', array('fields'=>array('status'),'conditions' => array('user_id'=> $user_id,'tag_name'=>$tag_name)));    	
	$result['like'] =$result['like']['Taglike']['status'];
	//pr($result['like']['status']); die();
	echo  json_encode($result);        
	$this->layout = false;
	$this->render(false);
	}

}  


?>
