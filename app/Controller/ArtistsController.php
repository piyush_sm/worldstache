<?php 
	
/* 

 This controller uses Artist Model

*/

 Class ArtistsController extends AppController {

	public $components = array('Session','Cookie');
	public $helpers = array('Form','Html','Js','Time');



	public function add_artist() {

	$this->set('title_for_layout','Add Artist');
	if($this->Auth->user('user_type') == 3) {
		
	$all_artist = $this->Artist->find('all', array(
	    'conditions' => array(
		'artist_status' => '1'
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('all_artist',$all_artist);
	
	$pending_artists = $this->Artist->find('all', array(
	    'conditions' => array(
		'artist_status' => '0'
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('pending_artists',$pending_artists);

	if($this->request->is('post')){

		$this->request->data['Artist']['user_id'] = $this->Auth->user('id');

		if($this->Auth->user('user_type') == 3){
		$this->request->data['Artist']['artist_status'] = 1;
		}
		/* Process logo image here */
		$filename = $this->request->data['Artist']['artist_logo']['name'];
		// Move the image
		move_uploaded_file( $this->request->data['Artist']['artist_logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/artist/'.$filename;
		
		/* Process cover image here */
		$filename = $this->request->data['Artist']['artist_cover_image']['name'];
		/* Move the image */
		move_uploaded_file( $this->request->data['Artist']['artist_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		/* Set the image path in the database */
		$this->request->data['Artist']['artist_cover_image'] = '/images/artist/'.$filename;
		
		
		if($this->Artist->save($this->request->data)){

		$this->Session->setFlash("Artist is succesfully saved",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'artists', 'action'=>'add_artist'));

		} else {

       		$this->Session->setFlash("Opps!!... Information Not saved Try Again later");
		}
	
     }
    	$this->layout = 'admin_layout';
		$this->render('add_artist');
 } else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
}


	public function edit_artist($id=null) {
	
	$this->set('title_for_layout','Edit Artist');
	if($this->Auth->user('user_type') == 3) {
		
	$id = base64_decode($_GET['aid']);

	$artist = $this->Artist->findById($id);
	$this->set('artist', $artist);

	$all_artist = $this->Artist->find('all', array(
	    'conditions' => array(
		'artist_status' => '1'
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('all_artist',$all_artist);
	
	$pending_artists = $this->Artist->find('all', array(
	    'conditions' => array(
		'artist_status' => '0'
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('pending_artists',$pending_artists);

 	if($this->request->is('post','put')){
	
	/* Process logo image here */
		
		$filename = $this->request->data['Artist']['artist_logo']['name'];
		if($filename == ""){
			$this->request->data['Artist']['artist_logo'] = $artist['Artist']['artist_logo'];
			} else {
		// Move the image
		move_uploaded_file( $this->request->data['Artist']['artist_logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/artist/'.$filename;
		}
		/* Process cover image here */
		$filename = $this->request->data['Artist']['artist_cover_image']['name'];
		if($filename == ""){
			$this->request->data['Artist']['artist_cover_image'] = $artist['Artist']['artist_cover_image'];
			} else {
		/* Move the image */
		move_uploaded_file( $this->request->data['Artist']['artist_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		/* Set the image path in the database */
		$this->request->data['Artist']['artist_cover_image'] = '/images/artist/'.$filename;
	}
	$this->Artist->id = $id;
	if($this->Artist->save($this->request->data)){

	$this->Session->setFlash("Artist has been updated successfully");
	$this->redirect(array('controller'=>'artists', 'action'=>'add_artist'));

	} else {
	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 
	
	}
	$this->layout = 'admin_layout';
	$this->render('edit_artist');
	} else {
     	$this->Session->setFlash("You cannot access this page");
     	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

}
	public function delete_artist ($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['aid']);

	$this->Artist->id = $id;
        if($this->Artist->delete()){
	
	$this->Session->setFlash("You have successfully deleted artist the artist");
	$this->redirect(array('controller'=>'artists','action'=>'add_artist'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'artists','action'=>'add_artist'));
	}
	} else {
	$this->Session->setFlash("You cannot access this page");
	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function add_artist_p() {

	
	if($this->Auth->user('user_type') == 1) {
		
	$all_artist = $this->Artist->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('all_artist',$all_artist);

	if($this->request->is('post')){

		$this->request->data['Artist']['user_id'] = $this->Auth->user('id');
		/* Process logo image here */
		$filename = $this->request->data['Artist']['artist_logo']['name'];
		// Move the image
		move_uploaded_file( $this->request->data['Artist']['artist_logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/artist/'.$filename;
		
		/* Process cover image here */
		$filename = $this->request->data['Artist']['artist_cover_image']['name'];
		/* Move the image */
		move_uploaded_file( $this->request->data['Artist']['artist_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		/* Set the image path in the database */
		$this->request->data['Artist']['artist_cover_image'] = '/images/artist/'.$filename;
		
		if($this->Artist->save($this->request->data)){

		$this->Session->setFlash("Artist is succesfully saved",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'artists', 'action'=>'add_artist_p'));

		} else {

       		$this->Session->setFlash("Opps!!... Information Not saved Try Again later");
		}
	
     }
    	$this->layout = 'dashboard_layout';
	$this->render('add_artist_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}
	
	
	public function edit_artist_p() {

	if($this->Auth->user('user_type') == 1) {	
	$id = base64_decode($_GET['aid']);
	
	$artist = $this->Artist->findById($id);
	$this->set('artist', $artist);

 	if($this->request->is('post','put')){
	
	/* Process logo image here */
		
		$filename = $this->request->data['Artist']['artist_logo']['name'];
		if($filename == ""){
			$this->request->data['Artist']['artist_logo'] = $artist['Artist']['artist_logo'];
			} else {
		// Move the image
		move_uploaded_file( $this->request->data['Artist']['artist_logo']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/artist/'.$filename;
		}
		/* Process cover image here */
		$filename = $this->request->data['Artist']['artist_cover_image']['name'];
		if($filename == ""){
			$this->request->data['Artist']['artist_cover_image'] = $artist['Artist']['artist_cover_image'];
			} else {
		/* Move the image */
		move_uploaded_file( $this->request->data['Artist']['artist_cover_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/artist/'. basename($filename));
		/* Set the image path in the database */
		$this->request->data['Artist']['artist_cover_image'] = '/images/artist/'.$filename;
	}
	$this->Artist->id = $id;
	if($this->Artist->save($this->request->data)){

	$this->Session->setFlash("Artist has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'artists', 'action'=>'add_artist_p'));

	} else {
	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 
	
	}
	$this->layout = 'dashboard_layout';
	$this->render('edit_artist_p');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}


	}
	
	public function activate_artist() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['aid']);
	
	
	$this->Artist->id = $id;
	
	if($this->Artist->saveField('artist_status', 1)){

	$this->Session->setFlash("Artist has been activated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'artists', 'action'=>'add_artist'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_artist');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}
	
	public function deactivate_artist() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['aid']);
	
	
	$this->Artist->id = $id;
	
	if($this->Artist->saveField('artist_status', 0)){

	$this->Session->setFlash("Artist has been deactivated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'artists', 'action'=>'add_artist'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_artist');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}

	public function view_artist() {

	if($this->Auth->user('user_type') == 3) {

	$this->set('title_for_layout','View Artist');
	$id = base64_decode($_GET['aid']);
	$artist_detail = $this->Artist->find('first', array(
	    'conditions' => array(
		'Artist.id'=>$id
	    ),
	    'order' => 'Artist.id DESC'
	));
	$this->set('artist_detail',$artist_detail);
	$startdate = new DateTime(date('m/d/Y'));
	$sdate = $startdate->format('m/d/Y');
	$sdatetimestamp = strtotime($sdate);

	$this->loadModel('Event');
	$upcoming_events = $this->Event->find('all', array('fields' => array('Event.id','Event.event_title', 'Event.event_image'),'conditions'=>array('Event.event_end_timestamp >='=>$sdatetimestamp,'Event.event_artist'=>$id)));
	$this->set('upcoming_events', $upcoming_events);
	//pr($upcoming_events);

	$user_id = $this->Auth->user('id');
	$user_data = $this->Auth->user('profile_image');
	$this->set('user_data',$user_data);

	$this->loadModel('Comment');
	$all_comments = $this->Comment->find('all', array('conditions' => array('type_id'=> $id, 'comment_type'=>'artist')));
	$this->set('all_comments',$all_comments);

	if($user_id != "" && $user_id != null){
		$this->set('user_id', $user_id);
	} else{
		$user_id = "";
		$this->set('user_id', $user_id);
	}
	$this->render('view_artist');
	
	} else {
		$this->Session->setFlash("You cannot access this page");
	    $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	} 

	}
}
?>
