<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $helpers = array('Form','Html','Js','Time');
	public $components = array('Session','Cookie','RequestHandler',
			'Stripe' => array('logFile' => 'stripe','logType' => 'error'),
        	'Auth' => array('authenticate' => array('Form' => array('fields' => array('username' => 'email'))),
		    'logoutRedirect' => array('controller' => 'pages', 'action' => 'display','home'),
		    'authError' => 'You can not access this page',
		    'authorize' => array('Controller')
        )
    );
	 public $myGlobalVar; 
	public function isAuthorized($user) {
        return true;
    }

    public function beforeFilter() {
	     $this->myGlobalVar = "test2";
        $this->Auth->allow('register', 'verify', 'emailconfirmation','searchresult','paybystripecomp','typehead','searchresultfilter','searcheventfilter','privacy','team','terms_of_use','fblogin','fbregister','getSubcategories','subcategories_list','categories_list','success','events_list','events_list_s','blog_list','search','event_detail','about_us','contact_us','blog_detail','forgotpassword','display','home','googlelogin', 'liketag');
        $this->set('logged_in', $this->Auth->loggedIn());
        $this->set('current_user', $this->Auth->user());

             App::import('Vendor', 'google-outh');	
			$google_client_id = '371737360541-uq9nmeqhp1uoisdh2oth3p753gg2p6d9.apps.googleusercontent.com';
			$google_client_secret = 'rP9tJ3vvYuYVTUn6mYn25Itf';
			$google_redirect_url = 'http://dev.staplelogic.in/worldstashdev/';
			$google_developer_key = 'AIzaSyDyWnFnADDJoIw13NgBJHqnyqp9GaQZ0-U';
			App::import("Vendor", "google-outh/src/Google_Client");
			App::import("Vendor", "google-outh/src/contrib/Google_Oauth2Service");
			$gClient = new Google_Client();
			$gClient->setApplicationName('Login to Google');
			$gClient->setClientId($google_client_id);
			$gClient->setClientSecret($google_client_secret);
			$gClient->setRedirectUri($google_redirect_url);
			$gClient->setDeveloperKey($google_developer_key);
            $google_oauthV2 = new Google_Oauth2Service($gClient);
			if (isset($_REQUEST['reset']))
			{
			$this->set('msg', 'Logout');

			$this->Session->delete('token');
			$gClient->revokeToken();
			header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
			}
			if (isset($_REQUEST['code']))
			{
			$gClient->authenticate($_REQUEST['code']);
			$this->Session->write('token', $gClient->getAccessToken());
			$this->redirect(filter_var($google_redirect_url, FILTER_SANITIZE_URL), null, false);
			//header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
			return;
			}

			if ($this->Session->read('token'))
			{
			$gClient->setAccessToken($this->Session->read('token'));
			}
			if ($gClient->getAccessToken())
			{		
			$user = $google_oauthV2->userinfo->get();
			$user_id = $user['id'];
			$user_name = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
			$first_name = filter_var($user['given_name'], FILTER_SANITIZE_SPECIAL_CHARS);
			$last_name = filter_var($user['family_name'], FILTER_SANITIZE_SPECIAL_CHARS);
			$email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			//$profile_url = filter_var($user['link'], FILTER_VALIDATE_URL);
			$profile_image_url = filter_var($user['picture'], FILTER_VALIDATE_URL);
			$personMarkup = "$email<div><img src='$profile_image_url?sz=50'></div>";
			$this->Session->write('token', $gClient->getAccessToken());
			$this->Session->destroy('token');
			}
			else
			{

			$authUrl = $gClient->createAuthUrl();
			}

			if(isset($authUrl)) 
			{
			$this->set('authUrl', $authUrl);
			}
					else // user logged in
					{ $this->loadModel('User');
					$result = $this->User->find('count', array('conditions' => array('email' => $email)));
					if($result > 0)
					{	pr($result); die();
						/*$this->request->data['User']['first_name'] = $_POST['first_name'];
						$this->request->data['User']['last_name'] = $_POST['last_name'];
						$this->request->data['User']['email'] = $_POST['email'];
						$this->request->data['User']['status'] = 1;
						$this->Session->write('Auth.User',$data);
						$msg = 'Welcome back '.$user_name.'!<br />';
						$msg .= '<br />';
						$msg .= '<img src="'.$profile_image_url.'" width="100" align="left" hspace="10" vspace="10" />';
						$msg .= '<br />';
						$msg .= '&nbsp;Name: '.$user_name.'<br />';
						$msg .= '&nbsp;Email: '.$email.'<br />';
						$msg .= '<br />';
						$this->set('msg', $msg);*/
					}
					else
					{
					//pr($user); die('here');

					/*$msg1 = 'Hi '.$user_name.', Thanks for Registering!';
					$msg1 .= '<br />';
					$msg1 .= '<img src="'.$profile_image_url.'" width="100" align="left" hspace="10" vspace="10" />';
					$msg1 .= '<br />';
					$msg1 .= '&nbsp;Name: '.$user_name.'<br />';
					$msg1 .= '&nbsp;Email: '.$email.'<br />';
					$msg1 .= '<br />';
					$this->set('msg', $msg1);*/
					$this->User->query("INSERT INTO users (first_name, last_name, email) VALUES ('$first_name','$last_name','$email')");
					$this->Session->write('Auth.User', $data);
					$this->redirect("/");
			}
}
    }


    public function beforeRender() {
		
		$this->loadModel('Setting');

		$images = $this->Setting->find('first', array('order'=> array('id'=>'DESC')));
		$this->set('images',$images);

		$frontend_footer = $images['Setting']['show_footer'];
		$this->set('frontend_footer',$frontend_footer);

		$slider_data = unserialize($images['Setting']['slider_images']);
		$this->set('slider_data',$slider_data);
			
		$footer_data = unserialize($images['Setting']['footer_images']);
		$this->set('footer_data',$footer_data);

		$banner_data = unserialize($images['Setting']['banner_images']);
		$this->set('banner_data', $banner_data);		

		$this->loadModel('Category');
		$list_categories = $this->Category->find('list',array('fields'=>array('Category.id','Category.category_name'), 
    	'order' => array('Category.category_name' => 'ASC')));
		$this->set('list_categories',$list_categories);
	
	$this->loadModel('Category');
		$list_categoriess = $this->Category->find('list',array('fields'=>array('Category.id','Category.category_name'), 
    	'order' => array('Category.category_name' => 'ASC'),'conditions' =>array('NOT'=>array('Category.id'=> array('111')))  
    	));
		$this->set('list_categoriess',$list_categoriess); 
	
	
		$this->loadModel('Subcategory');
		$list_subcategories = $this->Subcategory->find('list',array('fields'=>array('Subcategory.id','Subcategory.subcategory_name')));
		$this->set('list_subcategories',$list_subcategories);
		
		$this->loadModel('Artist');
		$list_artists = $this->Artist->find('list',array('fields'=>array('Artist.id','Artist.artist_name'),'conditions'=>array('artist_status'=> '1')));
		$this->set('list_artists',$list_artists);

		$this->loadModel('Place');
		$list_places = $this->Place->find('list',array('fields'=>array('Place.id','Place.name'),'conditions'=>array('place_status'=> '1')));
		$this->set('list_places',$list_places);

		$user_type = $this->Auth->user('user_type');
		$this->set('user_type', $user_type);

		$user_detail = $this->Auth->user();
		$this->set('user_detail', $user_detail);
		
		$securityKey = "sk_test_BpK89BEtfVaQiZRAo2O4Swsq";
		$this->set('securityKey',$securityKey);
		$publishKey = "pk_test_VBIK1um1lZ8McYFyE4tZeumb";
		$this->set('publishKey',$publishKey);
		
	}

	
}
