<?php 
	
/* 

 This controller uses Place Model

*/

 Class PlacesController extends AppController {

	public $components = array('Session','Cookie');
	public $helpers = array('Form','Html','Js','Time');



	public function add_place() {
	$this->set('title_for_layout','Add Place');
	if($this->Auth->user('user_type') == 3) {
		
	$all_place = $this->Place->find('all', array(
	    'conditions' => array(
		'place_status' => '1'
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('all_place',$all_place);
	
	$pending_place = $this->Place->find('all', array(
	    'conditions' => array(
		'place_status' => '0'
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('pending_place',$pending_place);

	if($this->request->is('post')){
		
		$this->request->data['Place']['user_id'] = $this->Auth->user('id');

		if($this->Auth->user('user_type') == 3){
			$this->request->data['Place']['place_status'] = 1;
		}
		/* Process logo image here */
		$filename = $this->request->data['Place']['place_image']['name'];
		// Move the image
		move_uploaded_file( $this->request->data['Place']['place_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/place/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Place']['place_image'] = '/images/place/'.$filename;
		
		
		if($this->Place->save($this->request->data)){

		$this->Session->setFlash("Place has been saved successfully",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'places', 'action'=>'add_place'));

		} else {

       		$this->Session->setFlash("Opps!!... Information Not saved Try Again later");
		}
	
     }
    	$this->layout = 'admin_layout';
	$this->render('add_place');
 } else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
}


	public function edit_place($id=null) {
	$this->set('title_for_layout','Edit Place');
	if($this->Auth->user('user_type') == 3) {
		
	$id = base64_decode($_GET['aid']);

	$place = $this->Place->findById($id);
	$this->set('place', $place);

	$all_place = $this->Place->find('all', array(
	    'conditions' => array(
		'place_status' => '1'
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('all_place',$all_place);
	
	$pending_place = $this->Place->find('all', array(
	    'conditions' => array(
		'place_status' => '0'
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('pending_place',$pending_place);

 	if($this->request->is('post','put')){
	
	/* Process logo image here */
		
		$filename = $this->request->data['Place']['place_image']['name'];
		if($filename == ""){
			$this->request->data['Place']['place_image'] = $place['Place']['place_image'];
			} else {
		// Move the image
		move_uploaded_file( $this->request->data['Place']['place_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/place/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/place/'.$filename;
		}
		
	$this->Place->id = $id;
	if($this->Place->save($this->request->data)){

	$this->Session->setFlash("Place has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places', 'action'=>'add_place'));

	} else {
	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 
	
	}
	$this->layout = 'admin_layout';
	$this->render('edit_place');
	} else {
     	$this->Session->setFlash("You cannot access this page");
     	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

}
	public function delete_place ($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['aid']);

	$this->Place->id = $id;
        if($this->Place->delete()){
	
	$this->Session->setFlash("You have successfully deleted the place",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places','action'=>'add_place'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'artists','action'=>'add_artist'));
	}
	} else {
	$this->Session->setFlash("You cannot access this page");
	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function activate_place() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['aid']);
	
	
	$this->Place->id = $id;
	
	if($this->Place->saveField('place_status', 1)){

	$this->Session->setFlash("Place has been activated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places', 'action'=>'add_place'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_place');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	
	}
	
	
	public function deactivate_place() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['aid']);
	
	
	$this->Place->id = $id;
	
	if($this->Place->saveField('place_status', 0)){

	$this->Session->setFlash("Place has been deactivated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places', 'action'=>'add_place'));

	} else {

	$this->Session->setFlash("Opps!!.. There is some Problem . Please try again");
	} 

	$this->layout = 'admin_layout';
	$this->render('add_place');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	
	}
	
	
	public function add_place_p() {

	if($this->Auth->user('user_type') == 1) {
		
	$all_place = $this->Place->find('all', array(
	    'conditions' => array(
		'user_id' => $this->Auth->user('id'),
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('all_place',$all_place);

	if($this->request->is('post')){
		
		$this->request->data['Place']['user_id'] = $this->Auth->user('id');
		/* Process logo image here */
		$filename = $this->request->data['Place']['place_image']['name'];
		// Move the image
		move_uploaded_file( $this->request->data['Place']['place_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/place/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Place']['place_image'] = '/images/place/'.$filename;
		
		
		if($this->Place->save($this->request->data)){

		$this->Session->setFlash("Place has been saved successfully",'default', array('class'=>'btn-success success_msg'));
		$this->redirect(array('controller'=>'places', 'action'=>'add_place_p'));

		} else {

       		$this->Session->setFlash("Opps!!... Information Not saved Try Again later");
		}
	
     }
    	$this->layout = 'dashboard_layout';
		$this->render('add_place_p');
 } else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
}


	public function edit_place_p($id=null) {

	if($this->Auth->user('user_type') == 1) {
		
	$id = base64_decode($_GET['aid']);

	$place = $this->Place->findById($id);
	$this->set('place', $place);

	$all_place = $this->Place->find('all');
	
	$this->set('all_place',$all_place);

 	if($this->request->is('post','put')){
	
	/* Process logo image here */
		
		$filename = $this->request->data['Place']['place_image']['name'];
		if($filename == ""){
			$this->request->data['Place']['place_image'] = $place['Place']['place_image'];
			} else {
		// Move the image
		move_uploaded_file( $this->request->data['Place']['place_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/place/'. basename($filename));
		// Set the image path in the database
		$this->request->data['Artist']['artist_logo'] = '/images/place/'.$filename;
		}
		
	$this->Place->id = $id;
	if($this->Place->save($this->request->data)){

	$this->Session->setFlash("Place has been updated successfully",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places', 'action'=>'add_place_p'));

	} else {
	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 
	
	}
	$this->layout = 'dashboard_layout';
	$this->render('edit_place_p');
	} else {
     	$this->Session->setFlash("You cannot access this page");
     	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}

}

	public function delete_place_p ($id=null) {

	if($this->Auth->user('user_type') == 1) {
	$id = base64_decode($_GET['aid']);

	$this->Place->id = $id;
    if($this->Place->delete()){
	
	$this->Session->setFlash("You have successfully deleted the place",'default', array('class'=>'btn-success success_msg'));
	$this->redirect(array('controller'=>'places','action'=>'add_place_p'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'places','action'=>'add_place_p'));
	}
	} else {
	$this->Session->setFlash("You cannot access this page");
	$this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}

	public function view_place() {

	$this->set('title_for_layout','View Place');
	$id = base64_decode($_GET['pid']);
	$place_detail = $this->Place->find('first', array(
	    'conditions' => array(
		'Place.id'=>$id
	    ),
	    'order' => 'Place.id DESC'
	));
	$this->set('place_detail',$place_detail);

	$startdate = new DateTime(date('m/d/Y'));
	$sdate = $startdate->format('m/d/Y');
	$sdatetimestamp = strtotime($sdate);

	$city = $place_detail['Place']['city'];
	
	$this->loadModel('Event');

	$upcoming_events = $this->Event->find('all', array('fields' => array('Event.id','Event.event_title', 'Event.event_image'),'conditions'=>array('Event.event_end_timestamp >='=>$sdatetimestamp,"MATCH(Event.event_city) AGAINST('".$city."' IN BOOLEAN MODE)")));
	$this->set('upcoming_events', $upcoming_events);

	$user_id = $this->Auth->user('id');
	$user_data = $this->Auth->user('profile_image');
	$this->set('user_data',$user_data);

	$this->loadModel('Comment');
	$all_comments = $this->Comment->find('all', array('conditions' => array('type_id'=> $id, 'comment_type'=>'place')));
	$this->set('all_comments',$all_comments);

	if($user_id != "" && $user_id != null){
		$this->set('user_id', $user_id);
	} else{
		$user_id = "";
		$this->set('user_id', $user_id);
	}
	
	$this->render('view_place');
	
	}

}
?>
