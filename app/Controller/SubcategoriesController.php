<?php 
	
/* 

 This controller uses Subcategory Model

*/

Class SubcategoriesController extends AppController {

	public $components = array('Session','Cookie');
	public $helpers = array('Form','Html','Js','Time');


    public function add_subcategory() {
	
	if($this->Auth->user('user_type') == 3){
	$all_subcategories = $this->Subcategory->find('all',array('order'=>array('Subcategory.cat_id'=>'ASC')));
	$this->set('all_subcategories',$all_subcategories);
	
	$this->loadModel('Category');
	$all_categories = $this->Category->find('all');
	$this->set('all_categories',$all_categories);
	
	if($this->request->is('post')) {

	$filename = $this->request->data['Subcategory']['subcategory_image']['name'];

	// Move the image
	
	move_uploaded_file( $this->request->data['Subcategory']['subcategory_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/subcategory/'. basename($filename));

	// Set the image path in the database

	$this->request->data['Subcategory']['subcategory_image'] = '/images/subcategory/'.$filename;

	
	 	if($this->Subcategory->save($this->request->data)){

		$this->Session->setFlash('Sub-Category has been saved successfully');
		$this->redirect(array('controller'=>'subcategories', 'action'=>'add_subcategory'));

		} else {

		$this->Session->setFlash('Opps!!.. Not saved. Try again later');
	
		}
	
   	}
	$this->layout = 'admin_layout';
	$this->render('add_subcategory');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


   public function edit_subcategory() {

	if($this->Auth->user('user_type') == 3) {	
	$id = base64_decode($_GET['scid']);

	$subcategory = $this->Subcategory->findById($id);
	$this->set('subcategory', $subcategory);
	
	$all_subcategories = $this->Subcategory->find('all');	
	$this->set('all_subcategories',$all_subcategories);
	
	$this->loadModel('Category');
	$all_categories = $this->Category->find('all');
	$this->set('all_categories',$all_categories);

	if($this->request->is('post','put')) {
	
	$filename = $this->request->data['Subcategory']['subcategory_image']['name'];
	if($filename == ""){
		$this->request->data['Subcategory']['subcategory_image'] = $subcategory['Subcategory']['subcategory_image'];
		} else {
	// Move the image
	move_uploaded_file( $this->request->data['Subcategory']['subcategory_image']['tmp_name'], $_SERVER['DOCUMENT_ROOT']. '/worldstashdev/app/webroot/images/subcategory/'. basename($filename));
	// Set the image path in the database
	$this->request->data['Subcategory']['subcategory_image'] = '/images/subcategory/'.$filename;
	}
	$this->Subcategory->id = $id;	
	if($this->Subcategory->save($this->request->data)){

	$this->Session->setFlash("Sub-Category has been updated successfully");
	$this->redirect(array('controller'=>'subcategories', 'action'=>'add_subcategory'));

	} else {

	$this->Session->setFlash("Opps!!.. Problem updating. Please try again");
	} 

		}
	$this->layout = 'admin_layout';
	$this->render('edit_subcategory');
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	}


	public function delete_subcategory ($id=null) {

	if($this->Auth->user('user_type') == 3) {
	$id = base64_decode($_GET['scid']);

	$this->Subcategory->id = $id;
        if($this->Subcategory->delete()) {
	
	$this->Session->setFlash("Sub-Category has been deleted succesfully");
	$this->redirect(array('controller'=>'subcategories','action'=>'add_subcategory'));

	} else {

	$this->Session->setFlash("Opps!!.. Some problem while deleting. Please try again later");
	$this->redirect(array('controller'=>'subcategories','action'=>'add_subcategory'));
	}
	} else {
	     $this->Session->setFlash("You cannot access this page");
	     $this->redirect(array('controller'=>'pages', 'action'=>'display'));
	}
	

	}
	
	public function subcategories_list() {
		
		$id = base64_decode($_GET['scid']);
		$this->set('cid',$id);
		
		$title = $this->Subcategory->find('first',array(
		'conditions'=> array('Subcategory.cat_id'=>$id)));
		if($title == null || $title == ""){

			$this->loadModel('Category');
			$title = $this->Category->find('first',array(
				'conditions'=> array('Category.id'=>$id)));

		$cat_name = "No Subcategory available for this category";
		$this->set('cat_name',$cat_name);
		$cat_image = "all.png";
		$this->set('cat_image',$cat_image);
		} else {
		$cat_name = $title['Category']['category_name'];
		$this->set('cat_name',$cat_name);
		$this->set('title_for_layout',$cat_name.' | Worldstache');
		$cat_image = $title['Category']['category_image'];
		$this->set('cat_image',$cat_image);
		}

		//pr($cat_image);die();
		$all_subcategories = $this->Subcategory->find('all', array(
		'conditions'=> array('Subcategory.cat_id'=>$id),
	    'order' => 'Subcategory.id DESC'
	));

		$all_subcat_images = $this->Subcategory->query('SELECT `id`,`subcategory_image` FROM subcategories WHERE cat_id ='.$id);
		//pr($all_subcat_images); die();
		$this->set('all_subcategories',$all_subcategories);
		$this->set('all_subcat_images',$all_subcat_images);
		$this->render('subcategories_list');
	}
	
	public function getSubcategories() {
		
		$id = $_POST['id'];
		if($this->request->is('post')){
			
		$sub_cat_list = $this->Subcategory->find('list', array(
			'fields'=>array('Subcategory.subcategory_name'),
			'conditions'=> array('Subcategory.cat_id'=> $id),
			'order'=>array('Subcategory.subcategory_name'=>'ASC')
			));
			echo json_encode($sub_cat_list);
		}
		$this->layout = false;
		$this->render(false);
		}

	/* public function getAllSubcategories() {
		
		$id = $_POST['id'];
		if($this->request->is('post')){
			
		$sub_cat_list = $this->Subcategory->find('all', array(
			'conditions'=> array('Subcategory.cat_id'=> $id),
			'order'=>array('Subcategory.subcategory_name'=>'ASC')
			));
			echo json_encode($sub_cat_list);
		}
		$this->layout = false;
		$this->render(false);
		} */

}  


?>
