<?php

/* 

 Subcategory model to manage subcategoty table in database

*/

Class Subcategory extends AppModel {

    var $name = 'Subcategory';
    var $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'cat_id',
        )
    );
}


?>
