<?php

/* 

 Event model to manage events table in datasource

*/

Class Event extends AppModel {
	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

	function getEventsBySearch($data){
	if($data['event_date'] != ""){
	$data['event_date'] = strtotime($data['event_date']);
	//echo $data['event_date']; die();
	if(!empty($data['event_date']) && $data['event_date'] != "" && $data['cat_id'] != "111" && $data['sub_cat_id'] != "All"){
	$conditions = array(
    		'Event.event_city LIKE' => '%'.$data['where'].'%',
    		'AND' => array(
    			'Event.cat_id' => $data['cat_id'], 
    			'Event.sub_cat_id' => $data['sub_cat_id'],
    			'Event.event_start_timestamp <='=> $data['event_date'],
    			'Event.event_end_timestamp >='=> $data['event_date'],
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
    			),
    		'Event.event_status'=> '1',
        );
	}
	if($data['cat_id'] == "111" && !empty($data['event_date']) && $data['event_date'] != ""){
		$conditions = array(
			'Event.event_city LIKE' => '%'.$data['where'].'%',
			'AND' => array(
				'Event.event_start_timestamp <='=> $data['event_date'],
				'Event.event_end_timestamp >='=> $data['event_date'],
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
				),
			'Event.event_status'=> '1',
			);
	}
	if($data['cat_id'] != "111" && $data['sub_cat_id'] == "All" && !empty($data['event_date']) && $data['event_date'] != ""){
		$conditions = array(
			'Event.event_city LIKE' => '%'.$data['where'].'%',
			'AND' => array(
				'Event.cat_id' => $data['cat_id'],
				'Event.event_start_timestamp <='=> $data['event_date'],
				'Event.event_end_timestamp >='=> $data['event_date'],
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
				),
			'Event.event_status'=> '1',
			);
	}
	} else {
		
		switch ($data['date_option']) {
		    case "0":
		        $startdate = new DateTime(date('m/d/Y'));
		        $sdate = $startdate->format('m/d/Y');
		        $enddate = new DateTime(date('m/d/Y'));
		        $enddate->add(new DateInterval('P365D'));
		        $edate = $enddate->format('m/d/Y');
		        $sdatetimestamp = strtotime($sdate);
		        $edatetimestamp = strtotime($edate);
				//$date = array('Event.event_start_timestamp >='=>$sdatetimestamp,'Event.event_end_timestamp <='=>$edatetimestamp);
				$date = array('Event.event_end_timestamp >='=>$sdatetimestamp);
				//pr($date); 
		        break;
		    case "1":
		        $startdate = new DateTime(date('m/d/Y'));
		        $sdate = $startdate->format('m/d/Y');
		        $enddate = new DateTime(date('m/d/Y'));
		        $edate = $enddate->format('m/d/Y');
		        $sdatetimestamp = strtotime($sdate);
				$edatetimestamp = strtotime($edate);
				//echo $sdate;
				//echo "Start => ".$sdatetimestamp." ---- End => ".$edatetimestamp; die();
		        $date = array('Event.event_start_timestamp <='=>$sdatetimestamp,'Event.event_end_timestamp >='=>$edatetimestamp);
		        //pr($date);
		        break;
		    case "2":
		        $startdate = new DateTime(date('m/d/Y'));
		        $startdate->add(new DateInterval('P1D'));
		        $sdate = $startdate->format('m/d/Y');
		        $enddate = new DateTime(date('m/d/Y'));
		        $enddate->add(new DateInterval('P1D'));
		        $edate = $enddate->format('m/d/Y');
		        $sdatetimestamp = strtotime($sdate);
				$edatetimestamp = strtotime($edate);
				$date = array('Event.event_start_timestamp <='=>$sdatetimestamp,'Event.event_end_timestamp >='=>$edatetimestamp);
		        break;
		    case "3":
		        $sdatetimestamp = strtotime('saturday');
		        $edatetimestamp = strtotime('sunday');
		        $date = array('Event.event_start_timestamp <='=>$sdatetimestamp,'Event.event_end_timestamp >='=>$edatetimestamp);
		        break;
		    case "4":
		        $startdate = new DateTime(date('m/d/Y'));
		        $sdate = $startdate->format('m/d/Y');
		        $sdatetimestamp = strtotime($sdate);
				$edatetimestamp = strtotime('sunday');
				//echo "Start => ".$sdatetimestamp." ---- End => ".$edatetimestamp; die();
				$date = array('Event.event_start_timestamp <='=>$sdatetimestamp,'Event.event_end_timestamp >='=>$edatetimestamp);
		        break;
		    case "5":
		        $sdatetimestamp = strtotime('monday');
		        $edatetimestamp = strtotime('+1 sunday');
		        $date = array('Event.event_start_timestamp <='=>$sdatetimestamp,'Event.event_end_timestamp >='=>$edatetimestamp);
		        break;
		    default:
		        $startdate = new DateTime(date('m/d/Y'));
		        $sdate = $startdate->format('m/d/Y');
		        $enddate = new DateTime(date('m/d/Y'));
		        $edate = $enddate->format('m/d/Y');
		}
		//$sdatetimestamp = strtotime($sdate);
		//$edatetimestamp = strtotime($edate);
		//echo "Start => ".$sdatetimestamp." ---- End => ".$edatetimestamp; die();
		if($sdatetimestamp != "" && $edatetimestamp != "" && $data['cat_id'] != "111" && $data['sub_cat_id'] != "All"){
	$conditions = array(
    		'Event.event_city LIKE' => '%'.$data['where'].'%',
    		'AND' => array(
    			'Event.cat_id' => $data['cat_id'], 
    			'Event.sub_cat_id' => $data['sub_cat_id'],
    			$date,
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
    			),
    		'Event.event_status'=> '1',
        );
	}
	if($data['cat_id'] == "111"  && $sdatetimestamp != "" && $edatetimestamp != ""){
		$conditions = array(
			'Event.event_city LIKE' => '%'.$data['where'].'%',
			'AND' => array(
				//'Event.event_start_timestamp <='=> $sdatetimestamp,
    			//'Event.event_end_timestamp >='=> $edatetimestamp,
    			$date,
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
				),
			'Event.event_status'=> '1',
			);
	}
	if($data['cat_id'] != "111" && $data['sub_cat_id'] == "All" && $sdatetimestamp != "" && $edatetimestamp != ""){
		$conditions = array(
			'Event.event_city LIKE' => '%'.$data['where'].'%',
			'AND' => array(
				'Event.cat_id' => $data['cat_id'],
				//'Event.event_start_timestamp <='=> $sdatetimestamp,
    			//'Event.event_end_timestamp >='=> $edatetimestamp,
    			$date,
    			'Event.event_start_age between ? and ?' => array($data['start_age'], $data['end_age'])
				),
			'Event.event_status'=> '1',
			);
	}

	}

	$searched_events = $this->find('all', array('conditions'=>$conditions));
	return $searched_events;

	//pr($searched_events);
	//pr($data); die();

	}

	}

?>
