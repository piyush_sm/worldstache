<?php

/* 

 Comment model to manage comments database changes

*/

Class Comment extends AppModel {
	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
}

?>
