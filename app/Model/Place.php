<?php

/* 

 Place model to manage places database changes

*/

Class Place extends AppModel {
	public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );
}

?>
