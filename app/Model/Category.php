<?php

/* 

 Category model to manage categoty table in database

*/

Class Category extends AppModel {

	var $name = 'Category';
    var $hasMany = array(
        'Subcategory' => array(
            'className' => 'Subcategory',
            'foreignKey' => 'cat_id',
        ),
        'Event' => array(
            'className' => 'Event',
            'foreignKey' => 'cat_id',
        )
    );

}


?>
