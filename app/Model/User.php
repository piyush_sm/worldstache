<?php

/* 
 User model to manage users in datasource
*/

App::uses('AppModel', 'Model');

Class User extends AppModel {
	
	public $hasOne = array(
		'Userdetail' => array(
			'className'=>'Userdetail',
			'foreignKey'=>'user_id',
			'dependent' => true 
		)		
	  );

	public $validate = array(
        'first_name' => array(
            'Plese enter your First name' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your First name.'
            )
        ),
        'last_name' => array(
            'Plese enter your name' => array(
                'rule' => 'notEmpty',
                'message' => 'Plese enter your Last name.'
            ),
        ),
        'email' => array(
            'required' => array(
                'rule' => array('email', true),   
                'message' => 'Please provide a valid email address.'   
            ),
             'unique' => array(
                'rule'    => array('isUniqueEmail'),
                'message' => 'This email is already in use',
            ),
            'between' => array(
                'rule' => array('between', 6, 60),
                'message' => 'Usernames must be between 6 to 60 characters'
            )
        ),
        'password' => array(
            'Not empty' => array(
                'rule' => 'notEmpty',
                'message' => 'Plese enter your password'
            )
        )
        
    );

    /* Encrypting password using BlowfishPassword Hasher */

    /*public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $passwordHasher = new BlowfishPasswordHasher();
        $this->data[$this->alias]['password'] = $passwordHasher->hash(
            $this->data[$this->alias]['password']
        );
    }
    return true;
	} */

      public function beforeSave($options = array()) {
        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }
        return true;
    }
   
     /**
     * Before isUniqueEmail
     * @param array $options
     * @return boolean
     */
    public function isUniqueEmail($check) {
 	
        $email = $this->find('first',array(
                'fields' => array('User.id'),
                'conditions' => array('User.email' => $check['email']
                )
            )
        );

        if(!empty($email)) {
            return false;
        }else{
            return true;
        }
    }

	public function checkEmail($check) {
 	
        $email = $this->find('first',array(
                'fields' => array('User.id'),
                'conditions' => array('User.email' => $check
                )
            )
        );

        if(!empty($email)) {
            return $email;
        }
    }


}

?>
